import keras
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten


# input: probability matrix

def create_model():
    # create model
    model = Sequential()
    # add model layers
    model.add(Conv2D(8 * 8 * 20, kernel_size=(3, 3), activation='relu', input_shape=(10, 10, 1)))
    model.add(Conv2D(6 * 6 * 20, kernel_size=(3, 3), activation='relu'))
    model.add(Conv2D(4 * 4 * 20, kernel_size=(3, 3), activation='relu'))
    model.add(Conv2D(2 * 2 * 20, kernel_size=(3, 3), activation='relu'))
    model.add(Flatten())
    model.add(Dense(10, activation='softmax'))
    # keras.optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=1e-8)
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    print(model.summary())
    return model


def train_model(model, X_train, y_train, X_validate, y_validate):
    # train the model
    # compile model using accuracy to measure model performance
    model.fit(X_train, y_train, epochs=15)  # validation_data=(X_validate, y_validate), epochs=10)
    return model
