import copy

import numpy as np
import scipy.stats
from .utility import \
    f_divergence, lognormal

from .load_group_data import \
    representation2representation


# sef.b_set, self.clusters
class MultiCentroid:
    def __init__(self, b_set, clusters, closeness,
                 individuals_relative, valid_positions,
                 is_log='log', is_gauss='gauss'):
        self.b_set = b_set
        self.clusters = clusters
        self.closeness = closeness
        self.individuals_relative = individuals_relative
        self.valid_positions = valid_positions
        self.is_log = is_log
        self.is_gauss = is_gauss
        self.centroid_b_in_group_clusters = []
        self.clusters_rep = []

    def get_diver_b_cent_b2(self, b2_centroids):
        return f_divergence(
            self.individuals_relative[self.b_set],
            b2_centroids,
            self.valid_positions,
            is_same=False,
            is_smooth=False
        )

    def get_diver_b_cent_b2_smooth(self, b2_centroids):
        return f_divergence(
            self.individuals_relative[self.b_set],
            b2_centroids,
            self.valid_positions,
            is_same=False,
        )

    def get_diver_x_set_cent_y(self, x_set, y_centroids):
        return f_divergence(
            self.individuals_relative[x_set],
            y_centroids,
            self.valid_positions,
            is_same=False,
            is_smooth=False
        )

    def get_diver_x_set_cent_y_smooth(self, x_set, y_centroids):
        return f_divergence(
            self.individuals_relative[x_set],
            y_centroids,
            self.valid_positions,
            is_same=False
        )

    def get_diver_b_to_b2_centroids(self, b2_centroids,
                                    b2_centroids_index_as_cluster=None):
        if b2_centroids_index_as_cluster is None:
            b2_centroids_index_as_cluster = []

        # self.set_clusters_rep()  # self.clusters_rep
        self.set_centroid_b_in_group_clusters()
        diver_b_cent_b2 = self.get_diver_b_cent_b2_smooth(b2_centroids) # attention!
        # # alter the diag cluster divergence
        # if len(b2_centroids_index_as_cluster) > 0:
        #     diver_b_cent_b2 = \
        #         self.get_altered_diag_divergence(diver_b_cent_b2, b2_centroids_index_as_cluster)
        estimated_params = self.b_set_to_b2_centroids_moments(diver_b_cent_b2)
        return diver_b_cent_b2, estimated_params

    def x_set_to_b2_centroids(self, x_set, b2_centroids,
                              map_centroid, estimated_params,
                              diver_x_set_cent_b2=None,
                              remove_diag=False,
                              is_testing=True
                              ):
        if diver_x_set_cent_b2 is not None:
            len_x_set = len(diver_x_set_cent_b2)

        if is_testing:
            diver_x_set_cent_b2 = self.get_diver_x_set_cent_y_smooth(x_set, b2_centroids) #!
            len_x_set = len(x_set)
        elif diver_x_set_cent_b2 is None:
            raise Exception('diver_x_set_cent_b2 must not be None in training mode!')

        if self.is_gauss == 'gauss':
            cluster_to_b2_centroids_mean, cluster_to_b2_centroids_std = \
                estimated_params
        elif self.is_gauss == 'lognormal':
            cluster_to_b2_centroids_mu, cluster_to_b2_centroids_sigma = \
                estimated_params

        sum_log_fgl_b = np.zeros((len_x_set, len(self.clusters)))
        fgl_b_matrix = np.zeros((len_x_set, len(self.clusters), len(b2_centroids)))

        for cluster_index in range(len(self.clusters)):
            # log_p_g = np.log(len(self.clusters[cluster_index]) / full_size)

            for subject_index in range(len_x_set):
                if self.is_log == 'log' or self.is_log == 'sum':
                    cluster_fixed_sum_log_fgl_b = 0
                else:
                    cluster_fixed_sum_log_fgl_b = 1
                for centroid_index in range(len(b2_centroids)):
                    x_line = diver_x_set_cent_b2[subject_index, centroid_index]
                    is_diag = False

                    # if m latent groups in this mst leads to n b2_centroids
                    # map_centroid[centroid_index] = latent group match
                    # no match : map_centroid[centroid_index] = np.nan

                    if cluster_index == map_centroid[centroid_index]:
                        is_diag = True

                    if self.is_gauss == 'gauss':
                        norm_distribution = \
                            scipy.stats.norm(cluster_to_b2_centroids_mean[cluster_index][centroid_index],
                                             cluster_to_b2_centroids_std[cluster_index][centroid_index])

                        fgl_b = norm_distribution.pdf(x_line)

                    elif self.is_gauss == 'lognormal':
                        mu = cluster_to_b2_centroids_mu[cluster_index][centroid_index]
                        sigma = cluster_to_b2_centroids_sigma[cluster_index][centroid_index]
                        fgl_b = lognormal(x_line, mu, sigma)

                    elif self.is_gauss == 'norm':
                        fgl_b = scipy.stats.norm.pdf(x_line,
                                                     *estimated_params[cluster_index][centroid_index])

                    elif self.is_gauss == 'skewnorm':
                        fgl_b = scipy.stats.skewnorm.pdf(x_line,
                                                         *estimated_params[cluster_index][centroid_index])
                    elif self.is_gauss == 'expon':
                        fgl_b = scipy.stats.expon.pdf(x_line,
                                                      *estimated_params[cluster_index][centroid_index])
                    elif self.is_gauss == 'gaussian_kde':
                        kernel = estimated_params[cluster_index][centroid_index]
                        fgl_b = kernel.pdf(x_line)

                    if remove_diag:
                        if is_diag:
                            if self.is_log == 'log' or self.is_log == 'sum':
                                fgl_b = 0
                            else:
                                fgl_b = 1

                    fgl_b_matrix[subject_index, cluster_index, centroid_index] = fgl_b

                    if self.is_log == 'log':
                        log_fgl_b = \
                            np.log(fgl_b)
                        cluster_fixed_sum_log_fgl_b += log_fgl_b
                    elif self.is_log == 'sum':
                        cluster_fixed_sum_log_fgl_b += fgl_b
                    elif self.is_log == 'multi':
                        cluster_fixed_sum_log_fgl_b *= fgl_b

                sum_log_fgl_b[subject_index][cluster_index] = \
                    cluster_fixed_sum_log_fgl_b

        return sum_log_fgl_b, fgl_b_matrix

    def get_altered_diag_divergence(self, diver_b_cent_b2, b2_centroids_index_as_cluster):
        # if b2_centroids represents a cluster that is the same as the one in this MST.
        # b2_centroids_index -> cluster_index
        # for example: 5 clusters in this MST.
        # 0 th cluster has no match in b2_centroids
        # 1 st cluster is matched with 4th b2 centroids
        # ...
        # b2_centroids_index_as_cluster = [nan, 4, 3, nan, nan]
        for cluster_index in range(len(self.clusters)):
            if ~np.isnan(b2_centroids_index_as_cluster[cluster_index]):
                diver_b_cent_b2[list(self.clusters[cluster_index]),
                                b2_centroids_index_as_cluster[cluster_index]] = \
                    self.get_cluster_to_centroid_diag_replace(cluster_index)  # cluster is a set
        return diver_b_cent_b2

    def get_cluster_to_centroid_diag_replace(self, cluster_index):
        # clusters_rep = self.clusters_rep
        centroid_b_in_cluster = self.centroid_b_in_group_clusters

        return self.cluster_to_b_centroids(cluster_index,
                                           centroid_b_in_cluster[cluster_index])
    #

    def set_clusters_rep(self):
        group_clusters_rep = []
        # print(np.shape(self.individuals_relative))
        for cluster in self.clusters:
            cluster_rep = []
            for idx in cluster:
                # print(self.b_set[idx])
                cluster_rep.append(
                    self.individuals_relative[self.b_set[idx]])
            group_clusters_rep.append(cluster_rep)
        self.clusters_rep = group_clusters_rep

    def set_centroid_b_in_group_clusters(self):
        centroid_b_in_group_clusters = []
        for cluster in self.clusters:
            b_in_this_cluster = []
            sum_cluster_closeness = np.sum(self.closeness[list(cluster)])
            for b_in_cluster in cluster:
                b_closeness = self.closeness[b_in_cluster]
                centroid = np.zeros((200, 200))
                for j in cluster:
                    if j != b_in_cluster:
                        centroid += (self.closeness[j] / (sum_cluster_closeness - b_closeness)) * \
                                    self.individuals_relative[self.b_set[j]]
                b_in_this_cluster.append(centroid)
            centroid_b_in_group_clusters.append(b_in_this_cluster)
        self.centroid_b_in_group_clusters = centroid_b_in_group_clusters

    def cluster_to_b_centroids(self, cluster_index, b_centroids):

        this_cluster_to_centroids = []
        b_idx = 0
        for idx_in_b in self.clusters[cluster_index]:
            this_cluster_to_centroids.append(
                float(self.get_diver_x_set_cent_y(
                    [self.b_set[idx_in_b]], [b_centroids[b_idx]]))
            )
            b_idx += 1

        return this_cluster_to_centroids

    def b_set_to_b2_centroids_moments(self, diver_b_cent_b2):
        if self.is_gauss == 'gauss':
            cluster_to_b2_centroids_mean = []
            cluster_to_b2_centroids_std = []
            for cluster in self.clusters:
                # cluster is a set
                diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
                cluster_to_b2_centroids_mean.append(np.mean(diver_g_b2_centroids, axis=0))
                cluster_to_b2_centroids_std.append(np.std(diver_g_b2_centroids, axis=0))
            cluster_to_b2_centroids_mean = np.array(cluster_to_b2_centroids_mean)
            cluster_to_b2_centroids_std = np.array(cluster_to_b2_centroids_std)
            return cluster_to_b2_centroids_mean, cluster_to_b2_centroids_std
        elif self.is_gauss == 'lognormal':
            # log-norm
            cluster_to_b2_centroids_mu = []
            cluster_to_b2_centroids_sigma = []
            for cluster in self.clusters:
                # cluster is a set
                diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
                cluster_to_b2_centroids_mu.append(
                    np.mean(np.log(diver_g_b2_centroids), axis=0)
                )
                cluster_to_b2_centroids_sigma.append(
                    np.var(np.log(diver_g_b2_centroids), axis=0)
                )
            cluster_to_b2_centroids_mu = np.array(cluster_to_b2_centroids_mu)  # size: cluster_size * b2_centroids_size
            cluster_to_b2_centroids_sigma = np.array(cluster_to_b2_centroids_sigma)
            return cluster_to_b2_centroids_mu, cluster_to_b2_centroids_sigma
        elif self.is_gauss == 'skewnorm':
            cluster_to_b2_centroids_fitting_params = []
            for cluster in self.clusters:
                # cluster is a set
                diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
                params_list = [scipy.stats.skewnorm.fit(diver_g_b2_centroids[:, centroid_idx])
                     for centroid_idx in range(np.shape(diver_g_b2_centroids)[1])]
                cluster_to_b2_centroids_fitting_params.append(
                    params_list
                )  # fit for each centroid

                # for centroid_idx in range(np.shape(diver_g_b2_centroids)[1]):
                #     print(f"centroid_idx:{centroid_idx}")
                #     print(scipy.stats.skewnorm.pdf(
                #         diver_g_b2_centroids[:, centroid_idx], *params_list[centroid_idx]))

            return cluster_to_b2_centroids_fitting_params
        elif self.is_gauss == 'norm':
            cluster_to_b2_centroids_fitting_params = []
            for cluster in self.clusters:
                # cluster is a set
                diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
                cluster_to_b2_centroids_fitting_params.append(
                    [scipy.stats.norm.fit(diver_g_b2_centroids[:, centroid_idx])
                     for centroid_idx in range(np.shape(diver_g_b2_centroids)[1])]
                )  # fit for each centroid
            return cluster_to_b2_centroids_fitting_params
        elif self.is_gauss == 'expon':
            cluster_to_b2_centroids_fitting_params = []
            for cluster in self.clusters:
                # cluster is a set
                diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
                cluster_to_b2_centroids_fitting_params.append(
                    [scipy.stats.expon.fit(diver_g_b2_centroids[:, centroid_idx])
                     for centroid_idx in range(np.shape(diver_g_b2_centroids)[1])]
                )
            return cluster_to_b2_centroids_fitting_params
        elif self.is_gauss == 'gaussian_kde':
            cluster_to_b2_centroids_fitting_params = []
            for cluster in self.clusters:
                # cluster is a set
                diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
                cluster_to_b2_centroids_fitting_params.append(
                    [scipy.stats.gaussian_kde(diver_g_b2_centroids[:, centroid_idx])
                     for centroid_idx in range(np.shape(diver_g_b2_centroids)[1])]
                )
            return cluster_to_b2_centroids_fitting_params

    # TODO: replace the latent_group_algorithms

    def clustering_with_hubs(self, hubs):
        length_clusters = len(self.clusters)
        initial_clusters = [set() for _ in range(length_clusters)]
        centroids = self.individuals_relative[np.array(self.b_set)[hubs]]
        for idx_in_b in range(len(self.b_set)):
            # print(np.shape(self.individuals_relative[self.b_set[hubs]]))
            diver_indexes_in_b_hubs = \
                self.get_diver_x_set_cent_y_smooth( # smooth, attention!
                    [self.b_set[idx_in_b]],
                    centroids)
            # if idx_in_b in hubs:
            #     print(diver_indexes_in_b_hubs)
            m_index = np.argmin(diver_indexes_in_b_hubs)
            initial_clusters[int(m_index)].add(idx_in_b)
        #print(f"hubs:{hubs}")
        #print(f"initial clusters:{initial_clusters}")
        return initial_clusters, centroids

    def refine_clusters(self, hubs):
        clusters = copy.deepcopy(self.clusters)
        iter_cnt = 0
        refined_clusters = self.repartition(clusters, hubs)

        print(f"input_clusters:{self.clusters}")
        print(f"1st refined_clusters:{refined_clusters}")

        while not iter_cnt == 5 and not np.all(list(map(lambda x: x[0] == x[1], zip(refined_clusters, clusters)))):
            clusters = refined_clusters.copy()  # C<-R
            refined_clusters = self.repartition(clusters, hubs)
            iter_cnt += 1

            print(f"iter{iter_cnt} refined_clusters:{clusters}")

        print(f"summary: \n"
              f"iter_cnt:{iter_cnt}, \n clusters:{self.clusters} \n"
              f"refined clusters:{refined_clusters}")
        centroids = self.f_centroid(refined_clusters)
            #self.f_centroid_average(refined_clusters)
        return refined_clusters, centroids

    def repartition(self, clusters, hubs):
        length_clusters = len(clusters)
        refined_clusters = [set() for _ in range(length_clusters)]
        for idx in range(len(hubs)):
            refined_clusters[idx].add(hubs[idx])

        centroids = self.f_centroid(clusters)
            # self.f_centroid_average(clusters)

        diver_indexes_in_b_cent = np.zeros((len(self.b_set), len(clusters)))
        for cluster_idx in range(len(clusters)):
            sum_cluster_closeness = np.sum(self.closeness[list(clusters[cluster_idx])])

            for idx_in_b in clusters[cluster_idx]:
                if idx_in_b not in hubs:

                    # print(f"idx_in_b:{idx_in_b}")

                    diver_indexes_in_b_cent[idx_in_b, :] = \
                        self.get_diver_x_set_cent_y([self.b_set[idx_in_b]], centroids)

                    b_closeness = self.closeness[idx_in_b]
                    this_centroid = np.zeros((200, 200))
                    for j in clusters[cluster_idx]:
                        if j != idx_in_b:
                            this_centroid += (self.closeness[j] / (sum_cluster_closeness - b_closeness)) * \
                                             self.individuals_relative[self.b_set[j]]

                    diver_indexes_in_b_cent[idx_in_b, cluster_idx] = \
                        self.get_diver_x_set_cent_y([self.b_set[idx_in_b]], [this_centroid])

                    m_index = np.argmin(diver_indexes_in_b_cent[idx_in_b, :])
                    # print(f"diver_indexes_in_b_cent:{diver_indexes_in_b_cent[idx_in_b, :]},\n"
                    #      f"argmin:{int(m_index)}")
                    # print(f"m_index:{m_index}")
                    # print(refined_clusters[int(m_index)])
                    refined_clusters[int(m_index)].add(idx_in_b)
        return refined_clusters

    def f_centroid(self, clusters):
        # TODO: let 200,200 be variables
        centroids = np.zeros((len(clusters), 200, 200))

        # weighted sum
        for i in range(len(clusters)):
            sum_weight = 0
            for j in clusters[i]:
                sum_weight += self.closeness[j]
            for j in clusters[i]:
                centroids[i] += (self.closeness[j] / sum_weight) * \
                                self.individuals_relative[self.b_set[j]]
        return centroids

    def f_centroid_average(self, clusters):
        centroids = np.zeros((len(clusters), 200, 200))
        for i in range(len(clusters)):
            individuals_cnt = 1
            for j in clusters[i]:
                centroids[i] += self.individuals_relative[self.b_set[j]]
                individuals_cnt += 1
            centroids[i] = centroids[i] / individuals_cnt
        return centroids

