from .debug_leave_one_utility import LeaveOne


def get_prob_matrices(is_log='sum', is_gauss='skewnorm', is_testing=False):
    print(f"is testing:{is_testing}")

    experiment_path = 'Samsung_T5/Experiment2019/K-fold_NYU/'
    leave_none_test = LeaveOne(experiment_path)
    a1a3_to_g1cent_pg_n_fgl_b_matrix, a1a3_to_g2cent_pg_n_fgl_b_matrix, \
        typical_to_g1cent_pg_n_fgl_b_matrix, typical_to_g2cent_pg_n_fgl_b_matrix, \
        a1a3_clusters, typical_clusters, \
        a1a3_g1cent_params, a1a3_g2cent_params, typical_g1cent_params, typical_g2cent_params, \
        diver_x_set_g1cent, \
        diver_x_set_g2cent = \
        leave_none_test.leave_none_test(is_log=is_log, is_gauss=is_gauss, is_testing=is_testing)

    # a1a3_clusters = leave_none_test.leave_none['a1a3_latent_groups']
    # typical_clusters = leave_none_test.leave_none['typical_latent_groups']
    len_a1a3_clusters = len(a1a3_clusters)
    len_typical_clusters = len(typical_clusters)

    return a1a3_to_g1cent_pg_n_fgl_b_matrix, a1a3_to_g2cent_pg_n_fgl_b_matrix, \
        typical_to_g1cent_pg_n_fgl_b_matrix, typical_to_g2cent_pg_n_fgl_b_matrix,\
        a1a3_clusters, typical_clusters, len_a1a3_clusters, len_typical_clusters, \
        a1a3_g1cent_params, a1a3_g2cent_params, typical_g1cent_params, typical_g2cent_params, \
        diver_x_set_g1cent, \
        diver_x_set_g2cent


