import numpy as np
import copy

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
from tqdm import tqdm

from .orbitual_param import get_prob, get_prob_2dim

from .load_group_data import \
    load_data, load_representation, \
    resolve_valid_positions, representation2representation
from .latent_group_utility import MultiCentroid
from .utility import \
    load_leave_one_mst_n_hubs, \
    load_leave_one_local_centroids_n_latent_groups, \
    get_leave_ij_set, \
    load_leave_one_closeness, \
    load_global_centroid_n_cluster

'''
Four scenarios:

outer leave one out : in adhd
inner leave one out : in adhd

outer leave one out : in adhd
inner leave one out : in typical

outer leave one out : in typical
inner leave one out : in adhd


outer leave one out : in typical
inner leave one out : in typical
'''


class LeaveOne:
    def __init__(self, experiment_path):
        # experiment_path = 'Samsung_T5/Experiment2019/K-fold_NYU/'
        self.experiment_path = experiment_path
        self.data_dict = load_data()
        self.a1a3_group = self.data_dict['a1a3_group']
        self.typical_group = self.data_dict['typical_group']
        self.ta1a3_group = self.data_dict['ta1a3_group']
        self.adhd3_end = self.data_dict['adhd3_end']
        self.length_universal_set = len(self.ta1a3_group)

        representation_dict = load_representation(self.data_dict)
        input_representations = representation_dict['input_representations']
        self.individuals_relative = representation_dict['individuals_relative']
        valid_position_dict = resolve_valid_positions(input_representations, self.ta1a3_group)
        self.valid_positions = valid_position_dict['valid_positions']

        self.leave_one_dict = {}
        self.leave_none = {}
        self.leave_one = {}

    def load_leave_one(self):
        leave_one_typical_mst, leave_one_typical_hubs = \
            load_leave_one_mst_n_hubs('Typical', self.experiment_path)

        leave_one_a1a3_mst, leave_one_a1a3_hubs = \
            load_leave_one_mst_n_hubs('ADHD', self.experiment_path)

        leave_one_typical_local_centroids, leave_one_typical_latent_groups = \
            load_leave_one_local_centroids_n_latent_groups('Typical', self.experiment_path)

        leave_one_a1a3_local_centroids, leave_one_a1a3_latent_groups = \
            load_leave_one_local_centroids_n_latent_groups('ADHD', self.experiment_path)

        leave_one_typical_closeness = load_leave_one_closeness('Typical', self.experiment_path)
        leave_one_a1a3_closeness = load_leave_one_closeness('ADHD', self.experiment_path)

        global_typical_centroid, global_typical_group = load_global_centroid_n_cluster(
            'Typical', self.experiment_path)
        global_a1a3_centroid, global_a1a3_group = load_global_centroid_n_cluster(
            'ADHD', self.experiment_path)

        self.leave_one_dict = {
            'typical_mst': leave_one_typical_mst,
            'typical_hubs': leave_one_typical_hubs,
            'a1a3_mst': leave_one_a1a3_mst,
            'a1a3_hubs': leave_one_a1a3_hubs,
            'typical_local_centroids': leave_one_typical_local_centroids,
            'typical_latent_groups': leave_one_typical_latent_groups,
            'a1a3_local_centroids': leave_one_a1a3_local_centroids,
            'a1a3_latent_groups': leave_one_a1a3_latent_groups,
            'typical_closeness': leave_one_typical_closeness,
            'a1a3_closeness': leave_one_a1a3_closeness,
            'typical_global_centroid': global_typical_centroid,
            'a1a3_global_centroid': global_a1a3_centroid,
            'typical_global_group': global_typical_group,
            'a1a3_global_group': global_a1a3_group
        }

    def set_leave_none(self):
        self.leave_none = {
            'typical_mst':
                self.leave_one_dict['typical_mst'][-1],
            'typical_hubs':
                self.leave_one_dict['typical_hubs'][-1],
            'a1a3_mst':
                self.leave_one_dict['a1a3_mst'][-1],
            'a1a3_hubs':
                self.leave_one_dict['a1a3_hubs'][-1],
            'typical_local_centroids':
                self.leave_one_dict['typical_local_centroids'][-1],
            'typical_latent_groups':
                self.leave_one_dict['typical_latent_groups'][-1],
            'a1a3_local_centroids':
                self.leave_one_dict['a1a3_local_centroids'][-1],
            'a1a3_latent_groups':
                self.leave_one_dict['a1a3_latent_groups'][-1],
            'typical_closeness':
                self.leave_one_dict['typical_closeness'][-1],
            'a1a3_closeness':
                self.leave_one_dict['a1a3_closeness'][-1],
            'typical_global_centroid':
                self.leave_one_dict['typical_global_centroid'][-1],
            'a1a3_global_centroid':
                self.leave_one_dict['a1a3_global_centroid'][-1],
            'typical_global_group':
                self.leave_one_dict['typical_global_group'][-1],
            'a1a3_global_group':
                self.leave_one_dict['a1a3_global_group'][-1]
        }

    def clusters_to_centroids_leave_none(self):
        self.load_leave_one()
        self.set_leave_none()

        a1a3_clusters = self.leave_none['a1a3_latent_groups']
        typical_clusters = self.leave_none['typical_latent_groups']

        a1a3_local_centroids = self.leave_none['a1a3_local_centroids']
        typical_local_centroids = self.leave_none['typical_local_centroids']

        a1a3_hubs = self.leave_none['a1a3_hubs']
        typical_hubs = self.leave_none['typical_hubs']

        a1a3_clusters_rep, _ = \
            self.get_cluster_n_hub_reps(self.a1a3_group, a1a3_hubs, a1a3_clusters)

        typical_clusters_rep, _ = \
            self.get_cluster_n_hub_reps(self.typical_group, typical_hubs, typical_clusters)

        universal_clusters_rep = np.concatenate([a1a3_clusters_rep, typical_clusters_rep])
        universal_centroids_rep = np.concatenate([a1a3_local_centroids , typical_local_centroids])

        universal_clusters_to_centroids = self.clusters_to_hubs(
            universal_clusters_rep, universal_centroids_rep)

        return universal_clusters_to_centroids

    def clusters_to_hubs_leave_none(self):
        self.load_leave_one()
        self.set_leave_none()

        a1a3_clusters = self.leave_none['a1a3_latent_groups']
        typical_clusters = self.leave_none['typical_latent_groups']

        a1a3_hubs = self.leave_none['a1a3_hubs']
        typical_hubs = self.leave_none['typical_hubs']

        a1a3_clusters_rep, a1a3_hubs_rep = \
            self.get_cluster_n_hub_reps(self.a1a3_group, a1a3_hubs, a1a3_clusters)

        typical_clusters_rep, typical_hubs_rep = \
            self.get_cluster_n_hub_reps(self.typical_group, typical_hubs, typical_clusters)

        universal_clusters_rep = np.concatenate([a1a3_clusters_rep, typical_clusters_rep])
        universal_hubs_rep = np.concatenate([a1a3_hubs_rep, typical_hubs_rep])

        universal_clusters_to_hubs = self.clusters_to_hubs(
            universal_clusters_rep, universal_hubs_rep)

        typical_clusters_to_hubs = self.clusters_to_hubs(
            typical_clusters_rep, typical_hubs_rep)

        a1a3_clusters_to_hubs = self.clusters_to_hubs(
            a1a3_clusters_rep, a1a3_hubs_rep)

        return a1a3_clusters_to_hubs, \
               typical_clusters_to_hubs, \
               universal_clusters_to_hubs

    def get_cluster_n_hub_reps(self, group_indexes, hubs, clusters):
        hubs_rep = []
        for idx in hubs:
            hubs_rep.append(
                self.individuals_relative[group_indexes[idx]])

        group_clusters_rep = []
        for cluster in clusters:
            cluster_rep = []
            for idx in cluster:
                cluster_rep.append(
                    self.individuals_relative[group_indexes[idx]])
            group_clusters_rep.append(cluster_rep)

        return np.array(group_clusters_rep), np.array(hubs_rep)

    def clusters_to_hubs(self, clusters_rep, hubs_rep):
        this_clusters_to_hubs = []

        for cluster_idx in range(len(clusters_rep)):
            this_cluster_to_hubs = []
            for hub_rep in hubs_rep:
                individuals_to_this_hub = []
                for individual_rep in clusters_rep[cluster_idx]:
                    individuals_to_this_hub.append(
                        representation2representation(
                            individual_rep, hub_rep, self.valid_positions)
                    )
                this_cluster_to_hubs.append(individuals_to_this_hub)
            this_clusters_to_hubs.append(this_cluster_to_hubs)

        return this_clusters_to_hubs

    def centroid_to_centroid_leave_none(self):
        self.load_leave_one()
        self.set_leave_none()

        a1a3_local_centriods = self.leave_none['a1a3_local_centroids']
        typical_local_centriods = self.leave_none['typical_local_centroids']
        universal_local_centroids = np.concatenate([a1a3_local_centriods, typical_local_centriods])
        a1a3_centroid2centroid = self.centroid_to_centroid(a1a3_local_centriods)
        typical_centroid2centroid = self.centroid_to_centroid(typical_local_centriods)
        universal_centroid2centroid = self.centroid_to_centroid(universal_local_centroids)
        return a1a3_centroid2centroid, typical_centroid2centroid, universal_centroid2centroid

    def centroid_to_centroid(self, centroids):
        centroid_to_centroid = np.zeros((len(centroids), len(centroids)))
        for row in range(len(centroids)):
            for col in range(len(centroids)):
                centroid_to_centroid[row, col] = \
                    representation2representation(
                        centroids[row],
                        centroids[col],
                        self.valid_positions)
        return centroid_to_centroid

    def set_fixed_latent_groups(self):
        fixed_a1a3_set = self.a1a3_group
        fixed_a1a3_clusters = self.leave_none['a1a3_latent_groups']
        fixed_a1a3_centroids = self.leave_none['a1a3_local_centroids']

        fixed_typical_set = self.typical_group
        fixed_typical_clusters = self.leave_none['typical_latent_groups']
        fixed_typical_centroids = self.leave_none['typical_local_centroids']

        return fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
               fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids

    def set_fixed_global_group(self):
        fixed_a1a3_set = self.a1a3_group
        fixed_a1a3_clusters = self.leave_none['a1a3_global_group']
        fixed_a1a3_centroids = self.leave_none['a1a3_global_centroid']

        fixed_typical_set = self.typical_group
        fixed_typical_clusters = self.leave_none['typical_global_group']
        fixed_typical_centroids = self.leave_none['typical_global_centroid']

        return fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
               fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids

    def leave_one_test(self, is_global=False, is_log='log', is_gauss='gauss', remove_diag=False):
        prob_take_one_in_a1a3 = []
        prob_take_one_in_typical = []

        self.load_leave_one()
        self.set_leave_none()

        if is_global:
            fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
            fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids = \
                self.set_fixed_global_group()
        else:
            fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
            fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids = \
                self.set_fixed_latent_groups()

        fixed_a1a3_latent_multi = MultiCentroid(
            fixed_a1a3_set,
            fixed_a1a3_clusters,
            self.individuals_relative, self.valid_positions,
            is_log, is_gauss
        )

        fixed_typical_latent_multi = MultiCentroid(
            fixed_typical_set,
            fixed_typical_clusters,
            self.individuals_relative, self.valid_positions,
            is_log, is_gauss
        )

        for take_one in tqdm(range(len(self.ta1a3_group))):

            adapted_typical_latent_multi = copy.deepcopy(fixed_typical_latent_multi)
            adapted_typical_clusters = copy.deepcopy(fixed_typical_clusters)
            adapted_typical_centroids = copy.deepcopy(fixed_typical_centroids)

            adapted_a1a3_latent_multi = copy.deepcopy(fixed_a1a3_latent_multi)
            adapted_a1a3_clusters = copy.deepcopy(fixed_a1a3_clusters)
            adapted_a1a3_centroids = copy.deepcopy(fixed_a1a3_centroids)

            if take_one < self.adhd3_end:
                adapted_a1a3_set = get_leave_ij_set(fixed_a1a3_set, take_one, -1)
                if is_global:
                    adapted_a1a3_clusters = \
                        self.leave_one_dict['a1a3_global_group'][take_one]
                    adapted_a1a3_centroids = \
                        self.leave_one_dict['a1a3_global_centroid'][take_one]
                else:
                    adapted_a1a3_clusters = \
                        self.leave_one_dict['a1a3_latent_groups'][take_one]
                    adapted_a1a3_centroids = \
                        self.leave_one_dict['a1a3_local_centroids'][take_one]
                adapted_a1a3_latent_multi = MultiCentroid(
                    adapted_a1a3_set,
                    adapted_a1a3_clusters,
                    self.individuals_relative, self.valid_positions,
                    is_log, is_gauss
                )

            elif take_one >= self.adhd3_end:
                typical_take_one = take_one - self.adhd3_end
                adapted_typical_set = get_leave_ij_set(fixed_typical_set, typical_take_one, -1)
                if is_global:
                    adapted_typical_clusters = \
                        self.leave_one_dict['typical_global_group'][typical_take_one]
                    adapted_typical_centroids = \
                        self.leave_one_dict['typical_global_centroid'][typical_take_one]
                else:
                    adapted_typical_clusters = \
                        self.leave_one_dict['typical_latent_groups'][typical_take_one]
                    adapted_typical_centroids = \
                        self.leave_one_dict['typical_local_centroids'][typical_take_one]
                adapted_typical_latent_multi = MultiCentroid(
                    adapted_typical_set,
                    adapted_typical_clusters,
                    self.individuals_relative, self.valid_positions,
                    is_log, is_gauss
                )

            prob_in_a1a3, prob_in_typical = \
                get_prob_2dim(np.array([self.ta1a3_group[take_one]]),
                              adapted_a1a3_centroids,
                              adapted_typical_centroids,
                              adapted_a1a3_clusters,
                              adapted_typical_clusters,
                              self.length_universal_set,
                              adapted_a1a3_latent_multi, adapted_typical_latent_multi,
                              is_log, remove_diag
                              )

            prob_take_one_in_a1a3.append(prob_in_a1a3)
            prob_take_one_in_typical.append(prob_in_typical)

        return prob_take_one_in_a1a3, prob_take_one_in_typical

        # to_a1a3_sort, to_typical_sort,  = self.get_sort_leave_one_prob(
        #     prob_take_one_in_a1a3, prob_take_one_in_typical
        # )
        #
        # to_a1a3_max, to_typical_max, = self.get_max_leave_one_prob(
        #     prob_take_one_in_a1a3, prob_take_one_in_typical
        # )
        # return to_typical_max, to_a1a3_max, to_typical_sort, to_a1a3_sort

    def given_leave_one_test(self, take_one):
        prob_take_one_in_a1a3 = []
        prob_take_one_in_typical = []
        self.load_leave_one()
        self.set_leave_none()

        fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
        fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids = \
            self.set_fixed_latent_groups()

        fixed_a1a3_latent_multi = MultiCentroid(
            fixed_a1a3_set,
            fixed_a1a3_clusters,
            self.individuals_relative, self.valid_positions)

        fixed_typical_latent_multi = MultiCentroid(
            fixed_typical_set,
            fixed_typical_clusters,
            self.individuals_relative, self.valid_positions)

        adapted_typical_set = self.typical_group
        adapted_typical_latent_multi = copy.deepcopy(fixed_typical_latent_multi)
        adapted_typical_clusters = copy.deepcopy(fixed_typical_clusters)
        adapted_typical_centroids = copy.deepcopy(fixed_typical_centroids)

        adapted_a1a3_set = self.a1a3_group
        adapted_a1a3_latent_multi = copy.deepcopy(fixed_a1a3_latent_multi)
        adapted_a1a3_clusters = copy.deepcopy(fixed_a1a3_clusters)
        adapted_a1a3_centroids = copy.deepcopy(fixed_a1a3_centroids)

        if take_one < self.adhd3_end:
            adapted_a1a3_set = get_leave_ij_set(fixed_a1a3_set, take_one, -1)
            adapted_a1a3_clusters = self.leave_one_dict['a1a3_latent_groups'][take_one]
            adapted_a1a3_centroids = self.leave_one_dict['a1a3_local_centroids'][take_one]
            adapted_a1a3_latent_multi = MultiCentroid(
                adapted_a1a3_set,
                adapted_a1a3_clusters,
                self.individuals_relative, self.valid_positions)

        elif take_one >= self.adhd3_end:
            typical_take_one = take_one - self.adhd3_end
            adapted_typical_set = get_leave_ij_set(fixed_typical_set, typical_take_one, -1)
            adapted_typical_clusters = \
                self.leave_one_dict['typical_latent_groups'][typical_take_one]
            adapted_typical_centroids = \
                self.leave_one_dict['typical_local_centroids'][typical_take_one]
            adapted_typical_latent_multi = MultiCentroid(
                adapted_typical_set,
                adapted_typical_clusters,
                self.individuals_relative, self.valid_positions)

        prob_a1a3_in_a1a3, prob_a1a3_in_typical, \
        prob_typical_in_a1a3, prob_typical_in_typical = \
            get_prob(adapted_a1a3_set, adapted_typical_set,
                     adapted_a1a3_centroids, adapted_typical_centroids,
                     adapted_a1a3_clusters, adapted_typical_clusters,
                     self.length_universal_set,
                     adapted_a1a3_latent_multi, adapted_typical_latent_multi
                     )

        to_typical_sort, to_a1a3_sort = get_sort_prob(
            prob_a1a3_in_a1a3, prob_a1a3_in_typical,
            prob_typical_in_a1a3, prob_typical_in_typical
        )

        to_typical_max, to_a1a3_max = get_max_prob(
            prob_a1a3_in_a1a3, prob_a1a3_in_typical,
            prob_typical_in_a1a3, prob_typical_in_typical
        )

        return to_typical_max, to_a1a3_max, to_typical_sort, to_a1a3_sort

    def leave_none_global_test(self):
        self.load_leave_one()
        self.set_leave_none()

        prob_take_one_in_a1a3 = []
        prob_take_one_in_typical = []

        fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
        fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids = \
            self.set_fixed_global_group()

        fixed_a1a3_latent_multi = MultiCentroid(
            fixed_a1a3_set,
            fixed_a1a3_clusters,
            self.individuals_relative, self.valid_positions)

        fixed_typical_latent_multi = MultiCentroid(
            fixed_typical_set,
            fixed_typical_clusters,
            self.individuals_relative, self.valid_positions)

        # prob_in_a1a3, prob_in_typical = \
        #     get_prob_2dim(np.array([self.ta1a3_group]),
        #                   fixed_a1a3_centroids,
        #                   fixed_typical_centroids,
        #                   fixed_a1a3_clusters,
        #                   fixed_typical_clusters,
        #                   self.length_universal_set,
        #                   fixed_a1a3_latent_multi, fixed_typical_latent_multi
        #                   )
        #
        # prob_take_one_in_a1a3.append(prob_in_a1a3)
        # prob_take_one_in_typical.append(prob_in_typical)
        #
        # return prob_take_one_in_a1a3, prob_take_one_in_typical

        prob_a1a3_in_a1a3, prob_a1a3_in_typical, \
        prob_typical_in_a1a3, prob_typical_in_typical = \
            get_prob(fixed_a1a3_set, fixed_typical_set,
                     fixed_a1a3_centroids, fixed_typical_centroids,
                     fixed_a1a3_clusters, fixed_typical_clusters,
                     self.length_universal_set,
                     fixed_a1a3_latent_multi, fixed_typical_latent_multi
                     )

        to_typical_sort, to_a1a3_sort = get_sort_prob(
            prob_a1a3_in_a1a3, prob_a1a3_in_typical,
            prob_typical_in_a1a3, prob_typical_in_typical
        )

        to_typical_max, to_a1a3_max = get_max_prob(
            prob_a1a3_in_a1a3, prob_a1a3_in_typical,
            prob_typical_in_a1a3, prob_typical_in_typical
        )

        return to_typical_max, to_a1a3_max, to_typical_sort, to_a1a3_sort

    def leave_none_test(self, is_log='log', is_gauss='gauss', remove_diag=False):
        self.load_leave_one()
        self.set_leave_none()

        # prob_universal_in_a1a3 = []
        # prob_universal_in_typical = []

        fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
            fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids = \
            self.set_fixed_latent_groups()

        fixed_a1a3_latent_multi = MultiCentroid(
            fixed_a1a3_set,
            fixed_a1a3_clusters,
            self.individuals_relative, self.valid_positions,
            is_log, is_gauss
        )

        fixed_typical_latent_multi = MultiCentroid(
            fixed_typical_set,
            fixed_typical_clusters,
            self.individuals_relative, self.valid_positions,
            is_log, is_gauss
        )

        prob_a1a3_in_a1a3, prob_a1a3_in_typical, \
        prob_typical_in_a1a3, prob_typical_in_typical = \
            get_prob(fixed_a1a3_set, fixed_typical_set,
                     fixed_a1a3_centroids, fixed_typical_centroids,
                     fixed_a1a3_clusters, fixed_typical_clusters,
                     self.length_universal_set,
                     fixed_a1a3_latent_multi, fixed_typical_latent_multi,
                     is_log, remove_diag
                     )
        # prob_in_a1a3, prob_in_typical = \
        #     get_prob_2dim(self.ta1a3_group,
        #                   fixed_a1a3_centroids,
        #                   fixed_typical_centroids,
        #                   fixed_a1a3_clusters,
        #                   fixed_typical_clusters,
        #                   self.length_universal_set,
        #                   fixed_a1a3_latent_multi, fixed_typical_latent_multi
        #                   )
        #
        # prob_universal_in_a1a3.append(prob_in_a1a3)
        # prob_universal_in_typical.append(prob_in_typical)
        #
        # return prob_universal_in_a1a3, prob_universal_in_typical
        return prob_a1a3_in_a1a3, prob_a1a3_in_typical, \
               prob_typical_in_a1a3, prob_typical_in_typical

        # to_typical_sort, to_a1a3_sort = get_sort_prob(
        #     prob_a1a3_in_a1a3, prob_a1a3_in_typical,
        #     prob_typical_in_a1a3, prob_typical_in_typical
        # )
        #
        # to_typical_max, to_a1a3_max = get_max_prob(
        #     prob_a1a3_in_a1a3, prob_a1a3_in_typical,
        #     prob_typical_in_a1a3, prob_typical_in_typical
        # )
        #
        # return to_typical_max, to_a1a3_max, to_typical_sort, to_a1a3_sort

    def get_sort_leave_one_prob(self, prob_take_one_in_a1a3, prob_take_one_in_typical):
        to_a1a3_sort = [-np.sort(
            -prob_take_one_in_a1a3[i]
            , axis=1)[:, :3] for i in range(len(self.ta1a3_group))]
        to_typical_sort = [-np.sort(
            -prob_take_one_in_typical[i]
            , axis=1)[:, :3] for i in range(len(self.ta1a3_group))]
        return to_a1a3_sort, to_typical_sort

    def get_max_leave_one_prob(self, prob_take_one_in_a1a3, prob_take_one_in_typical):
        to_a1a3_max = [np.max(
            prob_take_one_in_a1a3[i]
            , axis=1) for i in range(len(self.ta1a3_group))]
        to_typical_max = [np.max(
            prob_take_one_in_typical[i]
            , axis=1) for i in range(len(self.ta1a3_group))]
        return to_a1a3_max, to_typical_max

    # TODO merge regression
    def leave_one_regression_max(self, to_typical, to_a1a3, train_type="none"):
        y = np.concatenate([np.zeros(len(self.a1a3_group)),
                            np.ones((len(self.typical_group)))]
                           )
        predict_y = np.zeros(len(self.ta1a3_group))
        score_list = np.zeros(len(self.ta1a3_group))

        if train_type == "none":
            x = np.vstack((to_typical, to_a1a3)).T
        if train_type == "leave one":
            x = np.concatenate([np.array(to_typical).T, np.array(to_a1a3).T]).T
        x[np.where(x == -np.inf)] = -1000

        for take_one in tqdm(range(len(self.ta1a3_group))):
            train_x = np.concatenate(
                [x[0:take_one],
                 x[take_one + 1:]])

            train_y = np.concatenate(
                [y[0:take_one],
                 y[take_one + 1:]])

            clf = LogisticRegression(random_state=0, solver='lbfgs',
                                     multi_class='ovr').fit(train_x, train_y)
            clf.score(train_x, train_y)
            score_list[take_one] = clf.score(train_x, train_y)
            predict_y[take_one] = clf.predict([x[take_one]])

        result = {
            'mean_score': np.mean(score_list),
            'accurate_count': np.count_nonzero(predict_y == y),
            'adhd_accurate_count':
                np.count_nonzero(predict_y[:self.adhd3_end] == y[:self.adhd3_end]),
            'typical_accurate_count':
                np.count_nonzero(predict_y[self.adhd3_end + 1:] == y[self.adhd3_end + 1:]),
            'f1_score': f1_score(y, predict_y),
            'predict_y': predict_y
        }
        return result

    def leave_one_regression_sort(self, to_typical_sort, to_a1a3_sort, train_type="none"):
        y = np.concatenate([np.zeros(len(self.a1a3_group)),
                            np.ones((len(self.typical_group)))]
                           )
        predict_y = np.zeros(len(self.ta1a3_group))
        score_list = np.zeros(len(self.ta1a3_group))

        if train_type == "none":
            x = np.concatenate([to_typical_sort[:, :3].T, to_a1a3_sort[:, :3].T]).T
        if train_type == "leave one":
            x = np.concatenate([np.array(to_typical_sort).reshape(-1, 3).T,
                                np.array(to_a1a3_sort).reshape(-1, 3).T]).T
        x[np.where(x == -np.inf)] = -1000

        for take_one in tqdm(range(len(self.ta1a3_group))):
            train_x = np.concatenate(
                [x[0:take_one],
                 x[take_one + 1:]])

            train_y = np.concatenate(
                [y[0:take_one],
                 y[take_one + 1:]])

            clf = LogisticRegression(random_state=0, solver='lbfgs',
                                     multi_class='ovr').fit(train_x, train_y)
            clf.score(train_x, train_y)
            score_list[take_one] = clf.score(train_x, train_y)
            predict_y[take_one] = clf.predict([x[take_one]])

        result = {
            'mean_score': np.mean(score_list),
            'accurate_count': np.count_nonzero(predict_y == y),
            'adhd_accurate_count':
                np.count_nonzero(predict_y[:self.adhd3_end] == y[:self.adhd3_end]),
            'typical_accurate_count':
                np.count_nonzero(predict_y[self.adhd3_end+1:] == y[self.adhd3_end+1:]),
            'f1_score': f1_score(y, predict_y),
            'predict_y': predict_y
        }
        return result

    def get_max_prob(self, prob_a1a3_in_a1a3, prob_a1a3_in_typical,
                     prob_typical_in_a1a3, prob_typical_in_typical):
        a1a3_x = np.max(prob_a1a3_in_typical, axis=1)
        a1a3_y = np.max(prob_a1a3_in_a1a3, axis=1)
        typical_x = np.max(prob_typical_in_typical, axis=1)
        typical_y = np.max(prob_typical_in_a1a3, axis=1)
        to_typical = np.concatenate([a1a3_x, typical_x])
        to_a1a3 = np.concatenate([a1a3_y, typical_y])

        return to_typical, to_a1a3

    def get_sort_prob(self, prob_a1a3_in_a1a3, prob_a1a3_in_typical,
                      prob_typical_in_a1a3, prob_typical_in_typical):
        a1a3_x = -np.sort(-prob_a1a3_in_typical, axis=1)
        a1a3_y = -np.sort(-prob_a1a3_in_a1a3, axis=1)
        typical_x = -np.sort(-prob_typical_in_typical, axis=1)
        typical_y = -np.sort(-prob_typical_in_a1a3, axis=1)
        to_typical_sort = np.concatenate([a1a3_x, typical_x])
        to_a1a3_sort = np.concatenate([a1a3_y, typical_y])

        return to_typical_sort, to_a1a3_sort
