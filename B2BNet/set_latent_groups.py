import pickle
import numpy as np
from tqdm import tqdm
from .load_group_data import \
    path_prefix, \
    kl_matrix_dict
from .utility import load_closeness, load_divergence, load_mst_n_hubs, \
    load_leave_one_closeness, load_leave_one_mst_n_hubs, get_leave_ij_set
from .latent_group_algorithms import LatentGroup


def write_local_centroids_n_latent_groups(
        b_set,
        group_name,
        experiment_path,
        individuals_relative,
        valid_positions,
        hub_thres=5
):
    leave_twice_local_clusters = []

    b_diver = load_divergence(group_name, experiment_path)
    group_mst, group_hubs = load_mst_n_hubs(group_name, experiment_path)
    group_closeness = load_closeness(group_name, experiment_path)

    for outer_i in tqdm(range(len(b_set))):
        leave_one_local_centroids = []
        leave_one_local_clusters = []
        for inner_j in range(len(b_set) - 1):
            group_latent_group = LatentGroup(
                b_diver,
                group_mst,
                group_hubs,
                group_closeness,
                group_name,
                b_set,
                outer_i,
                inner_j,
                experiment_path,
                individuals_relative,
                valid_positions,
                hub_thres
            )
            local_centroids, local_clusters = \
                group_latent_group.f_local_centroids_n_latent_groups()
            leave_one_local_centroids.append(local_centroids)
            leave_one_local_clusters.append(local_clusters)

        leave_twice_local_clusters.append(leave_one_local_clusters)
        with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
                  f"_{outer_i}_local_centroids.txt", "wb") as fp:
            pickle.dump(leave_one_local_centroids, fp)

    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              f"_latent_groups.txt", "wb") as fp:
        pickle.dump(leave_twice_local_clusters, fp)


def write_leave_one_local_centroids_n_latent_groups(
        b_set,
        group_name,
        experiment_path,
        individuals_relative,
        valid_positions,
        hub_thres=5
):

    b_diver = load_divergence(group_name, experiment_path)
    group_mst, group_hubs = load_leave_one_mst_n_hubs(group_name, experiment_path)
    group_closeness = load_leave_one_closeness(group_name, experiment_path)

    leave_one_local_centroids = []
    leave_one_local_clusters = []
    for outer_i in tqdm(range(len(b_set) + 1)):  # last element is without leave one
        converted_outer_i = outer_i
        inner_j = -1
        if outer_i == len(b_set):
            converted_outer_i = -1
        group_latent_group = LatentGroup(
            b_diver,
            group_mst,
            group_hubs,
            group_closeness,
            group_name,
            b_set,
            converted_outer_i,
            inner_j,
            experiment_path,
            individuals_relative,
            valid_positions,
            hub_thres
        )
        local_centroids, local_clusters = \
            group_latent_group.f_local_centroids_n_latent_groups()
        leave_one_local_centroids.append(local_centroids)
        leave_one_local_clusters.append(local_clusters)

    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              f"_leave_one_local_centroids.txt", "wb") as fp:
        pickle.dump(leave_one_local_centroids, fp)

    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              f"_leave_one_latent_groups.txt", "wb") as fp:
        pickle.dump(leave_one_local_clusters, fp)


def write_global_centroid(
        b_set,
        group_name,
        experiment_path,
        individuals_relative,
        valid_positions):

    b_diver = load_divergence(group_name, experiment_path)
    group_mst, group_hubs = load_leave_one_mst_n_hubs(group_name, experiment_path)
    group_closeness = load_leave_one_closeness(group_name, experiment_path)

    leave_one_local_centroids = []
    leave_one_local_clusters = []
    for outer_i in tqdm(range(len(b_set) + 1)):  # last element is without leave one
        converted_outer_i = outer_i
        inner_j = -1
        if outer_i == len(b_set):
            converted_outer_i = -1
        group_latent_group = LatentGroup(
            b_diver,
            group_mst,
            group_hubs,
            group_closeness,
            group_name,
            b_set,
            converted_outer_i,
            inner_j,
            experiment_path,
            individuals_relative,
            valid_positions,
        )

        if outer_i == len(b_set):
            cluster_set = np.arange(len(b_set))
            local_clusters = [set(cluster_set)]
        else:
            cluster_set = np.arange(len(b_set) - 1)
            local_clusters = [set(cluster_set)]

        local_centroids = group_latent_group.f_centroid(local_clusters)

        leave_one_local_centroids.append(local_centroids)
        leave_one_local_clusters.append(local_clusters)

    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              f"_global_centroid_leave_one.txt", "wb") as fp:
        pickle.dump(leave_one_local_centroids, fp)

    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              f"_global_cluster.txt", "wb") as fp:
        pickle.dump(leave_one_local_clusters, fp)
