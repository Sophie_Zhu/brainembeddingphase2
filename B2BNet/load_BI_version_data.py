from tqdm import tqdm
import glob
import numpy as np
import os
import pandas as pd

ubuntu_prefix = '/media/sophie/'
mac_prefix = '/Volumes/'
path_prefix = ubuntu_prefix

path = path_prefix + 'Samsung_T5/Data/ADHD200/rois_3000/rois_3000_nyu/rois/'
file_list = glob.glob(path + '*run1.mat')
# get
key_path = path_prefix + 'Samsung_T5/Data/ADHD200/rois_3000/rois_3000_nyu/NYU_phenotypic.csv'
df = pd.read_csv(key_path)


def load_label() -> [list, list]:
    DX_label = np.zeros(len(file_list))  # Diagnose label
    QC_Rest1 = np.zeros(len(file_list))  # Quality check label
    item = 0
    for item_path in tqdm(file_list):
        f = os.path.splitext(os.path.basename(item_path))
        df_id = np.where(df["ScanDir ID"] == int(f[0][15:-5]))
        DX_label[item] = df.loc[df_id[0]]['DX']
        QC_Rest1[item] = df.loc[df_id[0]]['QC_Rest_1']
        item += 1
    DX_label = DX_label.astype(int)
    return DX_label, QC_Rest1


def resolve_valid_indexes(idx_arr: list, QC_Rest1: list) -> list:
    ql = np.where(QC_Rest1 == 0)
    al = [0, 34, 47, 96, 72, 83, 65, 6, 126, 109, 118, 190, 122, 141, 131]
    idx_arr = [idx_arr[i] for i in range(len(idx_arr)) if i not in al and i not in ql[0].tolist()]
    return idx_arr


def resolve_quality_pass_indexes(idx_arr: list, QC_Rest1: list) -> list:
    ql = np.where(QC_Rest1 == 0)
    idx_arr = [i for i in idx_arr if i not in ql[0].tolist()]
    return idx_arr


def load_subject_ids():
    item = 0
    subject_ids = np.zeros(len(file_list))
    for item_path in tqdm(file_list):
        f = os.path.splitext(os.path.basename(item_path))
        df_id = np.where(df["ScanDir ID"] == int(f[0][15:-5]))
        subject_ids[item] = df.loc[df_id[0]]['ScanDir ID']
        item += 1
    return subject_ids


def get_subject_ids(indexes):
    subject_ids = load_subject_ids()
    return subject_ids[indexes]













