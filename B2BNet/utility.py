import numpy as np
from tqdm import tqdm
import pickle
from .Kruskal_algorithm import *
from .load_group_data import \
    representation2representation, \
    get_hubs, \
    generate_directed_graph, \
    generate_graph_for_Kruskal, \
    path_prefix, \
    kl_matrix_dict


def lognormal(x, mu, sigma):
    return (np.exp(-(np.log(x) - mu) ** 2 / (2 * sigma ** 2)) / (x * sigma * np.sqrt(2 * np.pi)))


def f_divergence(representation_set1, representation_set2, valid_positions,
                 is_same=True, is_smooth=True):

    row_len = len(representation_set1)
    col_len = len(representation_set2)

    if is_same:
        col_len = row_len
        representation_set2 = representation_set1

    diver = np.zeros((row_len, col_len))
    for i in range(row_len):
        for j in range(col_len):
            kl_ij = representation2representation(
                representation_set1[i],
                representation_set2[j],
                valid_positions
            )
            if is_same or not is_smooth:
                diver[i][j] = kl_ij
            elif not is_same:
                kl_ji = representation2representation(
                    representation_set2[j],
                    representation_set1[i],
                    valid_positions
                )
                diver[i][j] = (kl_ij + kl_ji) / 2
    return diver


def f_hub(b_len, mst, hub_thres):
    hubs = get_hubs(b_len, mst, hub_thres)
    # print('hub:', hubs)
    return hubs


def f_net(b_len, diver_b_set):
    net = generate_graph_for_Kruskal(b_len, diver_b_set)
    return net


def f_mst(net):
    return net.KruskalMST()


def f_closeness(b_set_len, diver_b_set):
    nx_g = generate_directed_graph(b_set_len, diver_b_set)
    nx_g_closeness = nx.closeness_centrality(nx_g, distance='weighted')
    node_closeness = []
    for v in nx_g.nodes():
        node_closeness.append(nx_g_closeness[v])
    closeness = np.array(node_closeness)
    return closeness


def load_divergence(group_name, experiment_path):
    file_path = path_prefix + experiment_path
    kl_matrix = np.load(file_path + kl_matrix_dict[group_name] + '.npy')
    return kl_matrix


def get_leave_ij_divergence(kl_matrix, outer_i, inner_j):
    leave_outer_i_matrix = get_leave_one_matrix(outer_i, kl_matrix)
    if outer_i == -1:
        return kl_matrix
    if inner_j == -1:
        return leave_outer_i_matrix
    else:
        leave_ij_matrix = get_leave_one_matrix(inner_j, leave_outer_i_matrix)
        return leave_ij_matrix


def get_leave_one_matrix(leave_one_index, kl_matrix):
    cut_matrix = np.delete(kl_matrix.copy(), leave_one_index, axis=1)
    cut_matrix = np.delete(cut_matrix, leave_one_index, axis=0)
    return cut_matrix


def write_closeness(group_name, group_len,  experiment_path):
    # leave twice out closeness
    print(f"writing closeness - {group_name} group ...")
    kl_matrix = load_divergence(group_name, experiment_path)
    group_leave_twice_closeness = np.zeros((group_len, group_len, group_len - 2))
    for i in tqdm(range(group_len)):
        leave_one_outer_matrix = get_leave_one_matrix(i, kl_matrix)
        #
        for j in range(group_len-1):
            leave_one_inner_matrix = get_leave_one_matrix(j, leave_one_outer_matrix)
            closeness = f_closeness(group_len - 2, leave_one_inner_matrix)
            group_leave_twice_closeness[i][j] = closeness

    np.save(path_prefix + experiment_path + kl_matrix_dict[group_name] + "_leave_twice_closeness",
            group_leave_twice_closeness)


def write_leave_one_closeness(group_name, group_len, experiment_path):
    # Note that the last element is the group closeness (no leave one)
    print(f"writing leave one closeness - {group_name} group ...")
    kl_matrix = load_divergence(group_name, experiment_path)
    group_leave_one_closeness = []
    for i in tqdm(range(group_len)):
        leave_one_outer_matrix = get_leave_one_matrix(i, kl_matrix)
        leave_one_closeness = f_closeness(group_len - 1, leave_one_outer_matrix)
        group_leave_one_closeness.append(leave_one_closeness)

    group_closeness = f_closeness(group_len, kl_matrix)
    group_leave_one_closeness.append(group_closeness)
    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              "_leave_one_closeness.txt", "wb") as fp:
        pickle.dump(group_leave_one_closeness, fp)


def load_closeness(group_name, experiment_path):
    group_leave_twice_closeness =\
        np.load(path_prefix + experiment_path + kl_matrix_dict[group_name] +
                "_leave_twice_closeness.npy")
    return group_leave_twice_closeness


def load_leave_one_closeness(group_name, experiment_path):
    infile = open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
                  "_leave_one_closeness.txt", 'rb')
    group_leave_one_closeness = pickle.load(infile)
    infile.close()
    return group_leave_one_closeness


def get_leave_ij_closeness(
        group_leave_closeness, outer_i, inner_j):
    if inner_j == -1:
        leave_ij_closeness = group_leave_closeness[outer_i]
    else:
        leave_ij_closeness = group_leave_closeness[outer_i][inner_j]

    return leave_ij_closeness


def write_mst_n_hubs(group_name, group_len, experiment_path, hub_thres=5):
    print(f"writing mst_n_hubs - {group_name} group ...")
    kl_matrix = load_divergence(group_name, experiment_path)
    # group_leave_twice_mst: (group_len, group_len-1, group_len - 2)
    # group_leave_twice_hubs: (group_len, group_len - 1, group_len - 2)
    group_leave_twice_mst = []
    group_leave_twice_hubs = []
    for i in tqdm(range(group_len)):
        leave_one_outer_matrix = get_leave_one_matrix(i, kl_matrix)
        #
        inner_mst = []
        inner_hubs = []
        for j in range(group_len - 1):
            leave_one_inner_matrix = get_leave_one_matrix(j, leave_one_outer_matrix)

            mst = f_mst(f_net(group_len-2, leave_one_inner_matrix))
            hubs = f_hub(group_len-2, mst, hub_thres)
            inner_mst.append(mst)
            inner_hubs.append(hubs)

        group_leave_twice_mst.append(inner_mst)
        group_leave_twice_hubs.append(inner_hubs)

    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              "_leave_twice_mst.txt", "wb") as fp:
        pickle.dump(group_leave_twice_mst, fp)

    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              "_leave_twice_hubs.txt", "wb") as fp:
        pickle.dump(group_leave_twice_hubs, fp)


def write_leave_one_mst_n_hubs(group_name, group_len, experiment_path, hub_thres=5):
    print(f"writing leave one mst_n_hubs  - {group_name} group ...")
    kl_matrix = load_divergence(group_name, experiment_path)
    group_leave_one_mst = []
    group_leave_one_hubs = []
    for i in tqdm(range(group_len)):
        leave_one_outer_matrix = get_leave_one_matrix(i, kl_matrix)
        leave_one_mst = f_mst(f_net(group_len - 1, leave_one_outer_matrix))
        leave_one_hubs = f_hub(group_len - 1, leave_one_mst, hub_thres)
        group_leave_one_mst.append(leave_one_mst)
        group_leave_one_hubs.append(leave_one_hubs)

    group_mst = f_mst(f_net(group_len, kl_matrix))
    group_hubs = f_hub(group_len, group_mst, hub_thres)

    group_leave_one_mst.append(group_mst)
    group_leave_one_hubs.append(group_hubs)
    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              "_leave_one_mst.txt", "wb") as fp:
        pickle.dump(group_leave_one_mst, fp)

    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              "_leave_one_hubs.txt", "wb") as fp:
        pickle.dump(group_leave_one_hubs, fp)


def load_mst_n_hubs(group_name, experiment_path):
    infile = open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
                  "_leave_twice_hubs.txt", 'rb')
    group_leave_twice_hubs = pickle.load(infile)
    infile.close()

    infile = open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
                  "_leave_twice_mst.txt", 'rb')
    group_leave_twice_mst = pickle.load(infile)
    infile.close()
    return group_leave_twice_mst, group_leave_twice_hubs


def load_leave_one_mst_n_hubs(group_name, experiment_path):
    infile = open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
                  "_leave_one_hubs.txt", 'rb')
    group_leave_one_hubs = pickle.load(infile)
    infile.close()

    infile = open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
                  "_leave_one_mst.txt", 'rb')
    group_leave_one_mst = pickle.load(infile)
    infile.close()
    return group_leave_one_mst, group_leave_one_hubs


def get_leave_ij_mst_n_hubs(
        group_leave_mst,
        group_leave_hubs,
        outer_i, inner_j):

    if inner_j == -1:
        leave_ij_hubs = group_leave_hubs[outer_i]
        leave_ij_mst = group_leave_mst[outer_i]
    else:
        leave_ij_hubs = group_leave_hubs[outer_i][inner_j]
        leave_ij_mst = group_leave_mst[outer_i][inner_j]

    return leave_ij_mst, leave_ij_hubs


def get_leave_ij_set(b_set, outer_i: int, inner_j: int):
    b_arr = b_set.copy()
    if (outer_i < len(b_set)) and (outer_i > -1):
        b_leave_one_arr = np.delete(b_arr, outer_i)
    else:
        return b_arr

    if inner_j == -1:
        return b_leave_one_arr
    else:
        b_leave_twice_arr = np.delete(b_leave_one_arr, inner_j)
        return b_leave_twice_arr


def load_leave_one_local_centroids_n_latent_groups(
        group_name,
        experiment_path,
):
    infile = open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
                  f"_leave_one_local_centroids.txt", 'rb')
    group_leave_one_local_centroids = pickle.load(infile)
    infile.close()

    infile = open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
                  f"_leave_one_latent_groups.txt", 'rb')
    group_leave_one_latent_groups = pickle.load(infile)
    infile.close()

    return group_leave_one_local_centroids, group_leave_one_latent_groups


def load_global_centroid_n_cluster(
        group_name,
        experiment_path,
):
    infile = open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
                  f"_global_centroid_leave_one.txt", 'rb')
    global_centroid_leave_one = pickle.load(infile)
    infile.close()

    infile = open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
                  f"_global_cluster.txt", 'rb')
    global_cluster_leave_one = pickle.load(infile)
    infile.close()

    return global_centroid_leave_one, global_cluster_leave_one


