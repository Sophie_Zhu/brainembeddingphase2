import numpy as np


def get_log_p_g(a1a3_mst_clusters, typical_mst_clusters,
                length_universal_set, is_log='log'):
    # %%
    #
    log_a1a3_p_g = get_log_group_x_p_g(a1a3_mst_clusters, length_universal_set,
                                       is_log)
    log_typical_p_g = get_log_group_x_p_g(typical_mst_clusters, length_universal_set,
                                          is_log)
    return log_a1a3_p_g, log_typical_p_g


def get_log_group_x_p_g(group_clusters, length_universal_set,
                        is_log='log'):
    log_group_x_p_g = np.zeros((1, len(group_clusters)))
    i = 0
    if is_log == 'log':
        for cluster in group_clusters:
            p_g = len(cluster) / length_universal_set
            log_group_x_p_g[:, i] = np.log(p_g)
            i += 1
    else:
        for cluster in group_clusters:
            p_g = len(cluster) / length_universal_set
            log_group_x_p_g[:, i] = p_g
            i += 1

    return log_group_x_p_g


def get_prob_x_in_y(log_y_p_g, adapted_y_latent_group,
                    x_group, g1_centroids, g2_centroids,
                    map_2g1_centroids, map_2g2_centroids,
                    is_log='log', remove_diag=False):

    term1 = adapted_y_latent_group.x_set_to_b2_centroids(
                                x_group, g1_centroids, map_2g1_centroids, remove_diag)[0]
    term2 = adapted_y_latent_group.x_set_to_b2_centroids(
                                x_group, g2_centroids, map_2g2_centroids, remove_diag)[0]

    if is_log == 'log':
        prob_x_in_y = log_y_p_g + term1 + term2

    elif is_log == 'multi':
        prob_x_in_y = log_y_p_g * term1 * term2

    elif is_log == 'sum':
        prob_x_in_y = log_y_p_g * (term1 + term2)
    # print(prob_x_in_y)
    return prob_x_in_y


def get_prob(a1a3_group, typical_group,
             a1a3_centroids, typical_centroids,
             a1a3_mst_clusters, typical_mst_clusters,
             length_universal_set,
             adapted_a1a3_latent_group, adapted_typical_latent_group,
             is_log='log',
             remove_diag=False
             ):

    g1_centroids = a1a3_centroids.copy()
    g2_centroids = typical_centroids.copy()

    map_a2a_centroids, map_a2t_centroids, \
        map_t2t_centroids, map_t2a_centroids = \
        map_g1_n_g2_centroids(len(g1_centroids),
                              len(g2_centroids))

    for_a1a3_map_2g1_centroids = map_a2a_centroids.copy()
    for_a1a3_map_2g2_centroids = map_a2t_centroids.copy()
    for_typical_map_2g1_centroids = map_t2a_centroids.copy()
    for_typical_map_2g2_centroids = map_t2t_centroids.copy()

    log_a1a3_p_g, log_typical_p_g = get_log_p_g(
        a1a3_mst_clusters,
        typical_mst_clusters,
        length_universal_set,
        is_log
    )

    prob_a1a3_in_a1a3 = get_prob_x_in_y(
        log_a1a3_p_g, adapted_a1a3_latent_group,
        a1a3_group, g1_centroids, g2_centroids,
        for_a1a3_map_2g1_centroids, for_a1a3_map_2g2_centroids,
        is_log, remove_diag
    )

    prob_a1a3_in_typical = get_prob_x_in_y(
        log_typical_p_g, adapted_typical_latent_group,
        a1a3_group, g1_centroids, g2_centroids,
        for_typical_map_2g1_centroids, for_typical_map_2g2_centroids,
        is_log, remove_diag
    )

    prob_typical_in_a1a3 = get_prob_x_in_y(
        log_a1a3_p_g, adapted_a1a3_latent_group,
        typical_group, g1_centroids, g2_centroids,
        for_a1a3_map_2g1_centroids, for_a1a3_map_2g2_centroids,
        is_log, remove_diag
    )

    prob_typical_in_typical = get_prob_x_in_y(
        log_typical_p_g, adapted_typical_latent_group,
        typical_group, g1_centroids, g2_centroids,
        for_typical_map_2g1_centroids, for_typical_map_2g2_centroids,
        is_log, remove_diag
    )

    return prob_a1a3_in_a1a3, prob_a1a3_in_typical, \
        prob_typical_in_a1a3, prob_typical_in_typical


def get_prob_2dim(x_set,
                  adapted_a1a3_centroids, adapted_typical_centroids,
                  adapted_a1a3_mst_clusters, adapted_typical_mst_clusters,
                  length_universal_set,
                  adapted_a1a3_latent_group, adapted_typical_latent_group,
                  is_log='log',
                  remove_diag=False
                  ):

    g1_centroids = adapted_a1a3_centroids.copy()
    g2_centroids = adapted_typical_centroids.copy()

    map_a2a_centroids, map_a2t_centroids, \
        map_t2t_centroids, map_t2a_centroids = \
        map_g1_n_g2_centroids(len(adapted_a1a3_centroids),
                              len(adapted_typical_centroids))

    for_a1a3_map_2g1_centroids = map_a2a_centroids.copy()
    for_a1a3_map_2g2_centroids = map_a2t_centroids.copy()
    for_typical_map_2g1_centroids = map_t2a_centroids.copy()
    for_typical_map_2g2_centroids = map_t2t_centroids.copy()

    log_a1a3_p_g, log_typical_p_g = get_log_p_g(
        adapted_a1a3_mst_clusters,
        adapted_typical_mst_clusters,
        length_universal_set,
        is_log
    )

    prob_in_a1a3 = get_prob_x_in_y(
        log_a1a3_p_g, adapted_a1a3_latent_group,
        x_set, g1_centroids, g2_centroids,
        for_a1a3_map_2g1_centroids, for_a1a3_map_2g2_centroids,
        is_log, remove_diag
    )

    prob_in_typical = get_prob_x_in_y(
        log_typical_p_g, adapted_typical_latent_group,
        x_set, g1_centroids, g2_centroids,
        for_typical_map_2g1_centroids, for_typical_map_2g2_centroids,
        is_log, remove_diag
    )
    return prob_in_a1a3, prob_in_typical


def map_g1_n_g2_centroids(len_adapted_a1a3_centroids,
                          len_adapted_typical_centroids):
    # g1 -> a1a3
    map_a2a_centroids = np.arange(len_adapted_a1a3_centroids)
    map_a2t_centroids = np.zeros(len_adapted_typical_centroids)
    map_a2t_centroids.fill(np.nan)
    # g2 ->typical
    map_t2t_centroids = np.arange(len_adapted_typical_centroids)
    map_t2a_centroids = np.zeros(len_adapted_a1a3_centroids)
    map_t2a_centroids.fill(np.nan)
    return map_a2a_centroids, map_a2t_centroids, map_t2t_centroids, map_t2a_centroids
