import scipy.stats
from .utility import \
    f_divergence,\
    get_leave_ij_divergence, \
    get_leave_ij_set, \
    get_leave_ij_mst_n_hubs, \
    get_leave_ij_closeness
from .load_group_data import *


class LatentGroup:
    def __init__(self,
                 group_diver,
                 group_mst,
                 group_hubs,
                 group_closeness,
                 group_name,
                 group_set,
                 outer_i,
                 inner_j,
                 experiment_path,
                 individuals_relative,
                 valid_positions,
                 hub_thres=5):

        self.experiment_path = experiment_path
        self.outer_i = outer_i
        self.inner_j = inner_j
        self.group_name = group_name
        self.b_set = get_leave_ij_set(group_set, outer_i, inner_j)
        self.individuals_relative = individuals_relative
        self.valid_positions = valid_positions
        self.hub_thres = hub_thres

        # saved variables that should be loaded
        self.mst, self.hubs = get_leave_ij_mst_n_hubs(
            group_mst, group_hubs, outer_i, inner_j)
        self.diver_b_set = get_leave_ij_divergence(group_diver, outer_i, inner_j)
        self.closeness = get_leave_ij_closeness(group_closeness, outer_i, inner_j)
        # empty variables
        self.nearest_b_cent_b = []
        self.centroids = []
        self.clusters = []

        # Algorithm CentroidOfLatentGroup(C, HUB) { //   C = T, A, T\b, or A\b, ect.
    #         Taking HUB’s distributions as initialized CENTROID;
    #         CLUSTER = f_cluster(C, CENTROID);
    #         CENTROID = f_centroid(CLUSTER, CLOSENESS_C);
    #         CLUSTER_New = f_cluster(C, CENTROID);
    #         While (CLUSTER_New <> CLUSTER) {
    #                CLUSTER = CLUSTER_New;
    #                CENTROID = f_centroid(CLUSTER, CLOSENESS_C);
    #                CLUSTER_New = f_cluster(C, CENTROID);
    #         }
    #         return CENTROID;
    # }

    def f_local_centroids_n_latent_groups(self):
        # initialization
        centroids = self.initialize_centroids()
        diver_b_cent_b = self.get_diver_b_cent_b_smooth(centroids)
        clusters = self.f_cluster(centroids, diver_b_cent_b)
        # print('Round1 clusters:', clusters)

        centroids = self.f_centroid(clusters)
        diver_b_cent_b = self.get_diver_b_cent_b(centroids)
        clusters_new = self.f_cluster(centroids, diver_b_cent_b)

        # print('Round2 clusters:', clusters_new)

        iter_cnt = 2

        while not np.all(list(map(lambda x: x[0] == x[1], zip(clusters_new, clusters)))):
            clusters = clusters_new
            centroids = self.f_centroid(clusters)
            diver_b_cent_b = self.get_diver_b_cent_b(centroids)
            clusters_new = self.f_cluster(centroids, diver_b_cent_b)
            iter_cnt += 1
            # print(f"Round{iter_cnt} clusters:", clusters_new)

        self.centroids = centroids
        self.clusters = clusters_new
        return centroids, clusters_new

    def get_nearest_b_cent_x(self, cent_b2_set):
        diver_b_cent_b2 = f_divergence(
            self.individuals_relative[self.b_set],
            cent_b2_set,
            self.valid_positions,
            is_same=False,
            is_smooth=False
        )
        return np.min(diver_b_cent_b2, axis=1)

    def f_cluster(self, centroids, diver):
        # diver can be diver_b_set, diver_b_cent_b
        self.nearest_b_cent_b = np.min(diver, axis=1)
        shortest_kl_res = np.argmin(diver, axis=1)

        clusters = []
        for i in range(len(centroids)):
            clusters.append(set(np.where(shortest_kl_res == i)[0]))
        return clusters

    def f_centroid(self, clusters):
        # TODO: let 200,200 be variables
        centroids = np.zeros((len(clusters), 200, 200))

        # weighted sum
        for i in range(len(clusters)):
            sum_weight = 0
            for j in clusters[i]:
                sum_weight += self.closeness[j]
            for j in clusters[i]:
                centroids[i] += (self.closeness[j] / sum_weight) * \
                                self.individuals_relative[self.b_set[j]]
        return centroids

    def get_diver_b_cent_b_smooth(self, cent_b):
        return f_divergence(
            self.individuals_relative[self.b_set],
            cent_b,
            self.valid_positions,
            is_same=False
        )

    def get_diver_b_cent_b(self, cent_b):
        return f_divergence(
            self.individuals_relative[self.b_set],
            cent_b,
            self.valid_positions,
            is_same=False,
            is_smooth=False
        )

    def get_diver_b2_cent_b(self, b2_set):
        return f_divergence(
            self.individuals_relative[b2_set],
            self.centroids,
            self.valid_positions,
            is_same=False,
            is_smooth=False
        )

    def get_diver_b_cent_b2(self, b2_centroids):
        return f_divergence(
            self.individuals_relative[self.b_set],
            b2_centroids,
            self.valid_positions,
            is_same=False,
            is_smooth=False
        )

    def get_diver_x_set_cent_y(self, x_set, y_centroids):
        return f_divergence(
            self.individuals_relative[x_set],
            y_centroids,
            self.valid_positions,
            is_same=False,
            is_smooth=False
        )

    def get_diver_x_cent_b(self, x_individual: int):
        individual_representation = self.individuals_relative[x_individual]
        individual_representation = individual_representation[np.newaxis, :, :]

        # print('individual_representation', np.shape(individual_representation))
        # print('centroids', np.shape(self.centroids))

        diver_x_cent_b = \
            f_divergence(
                individual_representation,  # convert to shape(1,200,200)
                self.valid_positions,
                self.centroids,
                is_same=False,
                is_smooth=False
            )
        return diver_x_cent_b

    def x_individual_nearest_centroids(self, x_individual):
        diver_x_cent_b = self.get_diver_x_cent_b(x_individual)
        return np.min(diver_x_cent_b, axis=1)

    # 组的轨道半径=组内KL平均值。 KL值/轨道半径 = 相对KL值。 最后用相对KL值的最小值 取代 KL最小值
    def b2_relative_nearest_centroids(self, b2_set):
        diver_b2_cent_b = \
            self.get_diver_b2_cent_b(b2_set)
        relative_kl = self.get_relative_diver_b2_cent_b(diver_b2_cent_b)

        return np.min(relative_kl, axis=1)

    def clusters_to_centroids_mean(self):
        clusters_mean_inner_diver = np.zeros((1, len(self.clusters)))
        for i in range(len(self.clusters)):
            clusters_to_centroids =\
                f_divergence(
                    self.individuals_relative[
                        np.array(list(self.clusters[i]))
                    ],
                    self.centroids[i][np.newaxis, :, :],
                    self.valid_positions,
                    is_same=False,
                    is_smooth=False
                )
            clusters_mean_inner_diver[0, i] = np.mean(clusters_to_centroids)
        return clusters_mean_inner_diver

    def get_relative_diver_b2_cent_b(self, diver_b2_cent_b):
        mean_inner_diver = self.clusters_to_centroids_mean()
        return diver_b2_cent_b / mean_inner_diver

    def initialize_centroids(self):
        centroids = []
        for hub in self.hubs:
            centroids.append(self.individuals_relative[self.b_set[hub]])
        centroids = np.array(centroids)
        return centroids

    def x_set_to_b2_centroids(self, x_set, b2_centroids, is_log=True):
        diver_b_cent_b2 = self.get_diver_b_cent_b2(b2_centroids)
        diver_x_set_cent_b2 = self.get_diver_x_set_cent_y(x_set, b2_centroids)

        cluster_to_b2_centroids_mean, cluster_to_b2_centroids_std =\
            self.b_set_to_b2_centroids_moments(diver_b_cent_b2)

        sum_log_fgl_b = np.zeros((len(x_set), len(self.clusters)))

        for cluster_index in range(len(self.clusters)):
            # log_p_g = np.log(len(self.clusters[cluster_index]) / full_size)

            for subject_index in range(len(x_set)):
                cluster_fixed_sum_log_fgl_b = 0
                for centroid_index in range(len(b2_centroids)):
                    norm_distribution = \
                        scipy.stats.norm(cluster_to_b2_centroids_mean[cluster_index][centroid_index],
                                         cluster_to_b2_centroids_std[cluster_index][centroid_index])

                    fgl_b = norm_distribution.pdf(diver_x_set_cent_b2[subject_index, centroid_index])

                    if is_log:
                        log_fgl_b = \
                            np.log(fgl_b)
                        cluster_fixed_sum_log_fgl_b += log_fgl_b
                    else:
                        cluster_fixed_sum_log_fgl_b *= fgl_b

                sum_log_fgl_b[subject_index][cluster_index] = \
                    cluster_fixed_sum_log_fgl_b

        return sum_log_fgl_b, cluster_to_b2_centroids_mean, cluster_to_b2_centroids_std

    def b_set_to_b2_centroids_moments(self, diver_b_cent_b2):
        cluster_to_b2_centroids_mean = []
        cluster_to_b2_centroids_std = []
        for cluster in self.clusters:
            # cluster is a set
            diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
            cluster_to_b2_centroids_mean.append(np.mean(diver_g_b2_centroids, axis=0))
            cluster_to_b2_centroids_std.append(np.std(diver_g_b2_centroids, axis=0))

        cluster_to_b2_centroids_mean = np.array(cluster_to_b2_centroids_mean)  # size: cluster_size * b2_centroids_size
        cluster_to_b2_centroids_std = np.array(cluster_to_b2_centroids_std)
        return cluster_to_b2_centroids_mean, cluster_to_b2_centroids_std
