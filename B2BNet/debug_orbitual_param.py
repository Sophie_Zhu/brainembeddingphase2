import numpy as np


def get_log_p_g(a1a3_mst_clusters, typical_mst_clusters,
                length_universal_set, is_log='log'):
    # %%
    #
    log_a1a3_p_g = get_log_group_x_p_g(a1a3_mst_clusters, length_universal_set,
                                       is_log)
    log_typical_p_g = get_log_group_x_p_g(typical_mst_clusters, length_universal_set,
                                          is_log)
    return log_a1a3_p_g, log_typical_p_g


def get_pg_multi_fglb(log_y_p_g, adapted_y_latent_group,
                      x_group, g1_centroids, g2_centroids,
                      map_2g1_centroids, map_2g2_centroids,
                      remove_diag=False):

    g1cent_fgl_b = adapted_y_latent_group.x_set_to_b2_centroids(
                                x_group, g1_centroids, map_2g1_centroids, remove_diag)[2]
    g2cent_fgl_b = adapted_y_latent_group.x_set_to_b2_centroids(
                                x_group, g2_centroids, map_2g2_centroids, remove_diag)[2]

    pg_multi_fglb_g1cent = log_y_p_g.T * g1cent_fgl_b
    pg_multi_fglb_g2cent2 = log_y_p_g.T * g2cent_fgl_b

    return pg_multi_fglb_g1cent, pg_multi_fglb_g2cent2


def get_pg_n_fglb(log_y_p_g, adapted_y_latent_group,
                  x_group, g1_centroids, g2_centroids,
                  map_2g1_centroids, map_2g2_centroids,
                  g1_centroids_index_as_cluster, g2_centroids_index_as_cluster,
                  remove_diag=False):

    g1cent_fgl_b, diver_b_g1cent = adapted_y_latent_group.x_set_to_b2_centroids(
                                x_group, g1_centroids, map_2g1_centroids,
                                remove_diag, g1_centroids_index_as_cluster)[2:]
    g2cent_fgl_b, diver_b_g2cent = adapted_y_latent_group.x_set_to_b2_centroids(
                                x_group, g2_centroids, map_2g2_centroids,
                                remove_diag, g2_centroids_index_as_cluster)[2:]

    tileg1_log_y_p_g = np.tile(log_y_p_g.T, (np.shape(g1cent_fgl_b)[0], 1, 1))
    # print(np.shape(g1cent_fgl_b), np.shape(tileg1_log_y_p_g))

    pg_n_fglb_g1cent = np.concatenate([g1cent_fgl_b, tileg1_log_y_p_g ], axis=2)
    # print(np.shape(pg_n_fglb_g1cent))

    tileg2_log_y_p_g = np.tile(log_y_p_g.T, (np.shape(g2cent_fgl_b)[0], 1, 1))
    pg_n_fglb_g2cent = np.concatenate([g2cent_fgl_b, tileg2_log_y_p_g], axis=2)

    # pg_n_fglb_g1cent = np.column_stack([np.squeeze(g1cent_fgl_b), log_y_p_g.T])
    # print(np.shape(pg_n_fglb_g1cent))
    # pg_n_fglb_g2cent2 = np.column_stack([np.squeeze(g2cent_fgl_b), log_y_p_g.T])

    return pg_n_fglb_g1cent, pg_n_fglb_g2cent, \
        diver_b_g1cent, diver_b_g2cent


def get_log_group_x_p_g(group_clusters, length_universal_set,
                        is_log='log'):
    log_group_x_p_g = np.zeros((1, len(group_clusters)))
    i = 0
    if is_log == 'log':
        for cluster in group_clusters:
            p_g = len(cluster) / length_universal_set
            log_group_x_p_g[:, i] = np.log(p_g)
            i += 1
    else:
        for cluster in group_clusters:
            p_g = len(cluster) / length_universal_set
            log_group_x_p_g[:, i] = p_g
            i += 1

    return log_group_x_p_g


def get_prob_x_in_y(log_y_p_g, adapted_y_latent_group,
                    x_group, g1_centroids, g2_centroids,
                    map_2g1_centroids, map_2g2_centroids,
                    g1_centroids_index_as_cluster, g2_centroids_index_as_cluster,
                    is_log='log', remove_diag=False):

    term1, _, g1cent_fgl_b, _ = adapted_y_latent_group.x_set_to_b2_centroids(
                                x_group, g1_centroids, map_2g1_centroids,
                                remove_diag, g1_centroids_index_as_cluster)
    term2, _, g2cent_fgl_b, _ = adapted_y_latent_group.x_set_to_b2_centroids(
                                x_group, g2_centroids, map_2g2_centroids,
                                remove_diag, g2_centroids_index_as_cluster)

    if is_log == 'log':
        prob_x_in_y = log_y_p_g + term1 + term2

    elif is_log == 'multi':
        prob_x_in_y = log_y_p_g * term1 * term2

    elif is_log == 'sum':
        prob_x_in_y = log_y_p_g * (term1 + term2)
    # print(prob_x_in_y)
    return prob_x_in_y, g1cent_fgl_b, g2cent_fgl_b


def get_prob(a1a3_group, typical_group,
             a1a3_centroids, typical_centroids,
             a1a3_mst_clusters, typical_mst_clusters,
             length_universal_set,
             adapted_a1a3_latent_group, adapted_typical_latent_group,
             is_log='log',
             remove_diag=False
             ):

    g1_centroids = a1a3_centroids.copy()
    g2_centroids = typical_centroids.copy()

    map_a2a_centroids, map_a2t_centroids, \
        map_t2t_centroids, map_t2a_centroids = \
        map_g1_n_g2_centroids(len(g1_centroids),
                              len(g2_centroids))

    for_a1a3_map_2g1_centroids = map_a2a_centroids.copy()
    for_a1a3_map_2g2_centroids = map_a2t_centroids.copy()
    for_typical_map_2g1_centroids = map_t2a_centroids.copy()
    for_typical_map_2g2_centroids = map_t2t_centroids.copy()

    g1_centroids_index_as_a1a3_cluster = np.arange(len(a1a3_mst_clusters))
    g2_centroids_index_as_a1a3_cluster = np.zeros(len(a1a3_mst_clusters))
    g2_centroids_index_as_a1a3_cluster.fill(np.nan)

    g1_centroids_index_as_typical_cluster = np.zeros(len(typical_mst_clusters))
    g1_centroids_index_as_typical_cluster.fill(np.nan)
    g2_centroids_index_as_typical_cluster = np.arange(len(typical_mst_clusters))

    log_a1a3_p_g, log_typical_p_g = get_log_p_g(
        a1a3_mst_clusters,
        typical_mst_clusters,
        length_universal_set,
        is_log
    )

    prob_a1a3_in_a1a3, aa_tog1_fgl_b, aa_tog2_fgl_b = get_prob_x_in_y(
        log_a1a3_p_g, adapted_a1a3_latent_group,
        a1a3_group, g1_centroids, g2_centroids,
        for_a1a3_map_2g1_centroids, for_a1a3_map_2g2_centroids,
        g1_centroids_index_as_a1a3_cluster, g2_centroids_index_as_a1a3_cluster,
        is_log, remove_diag
    )

    prob_a1a3_in_typical, at_tog1_fgl_b, at_tog2_fgl_b = get_prob_x_in_y(
        log_typical_p_g, adapted_typical_latent_group,
        a1a3_group, g1_centroids, g2_centroids,
        for_typical_map_2g1_centroids, for_typical_map_2g2_centroids,
        g1_centroids_index_as_typical_cluster, g2_centroids_index_as_typical_cluster,
        is_log, remove_diag
    )

    prob_typical_in_a1a3, ta_tog1_fgl_b, ta_tog2_fgl_b = get_prob_x_in_y(
        log_a1a3_p_g, adapted_a1a3_latent_group,
        typical_group, g1_centroids, g2_centroids,
        for_a1a3_map_2g1_centroids, for_a1a3_map_2g2_centroids,
        g1_centroids_index_as_a1a3_cluster, g2_centroids_index_as_a1a3_cluster,
        is_log, remove_diag
    )

    prob_typical_in_typical, tt_tog1_fgl_b, tt_tog2_fgl_b = get_prob_x_in_y(
        log_typical_p_g, adapted_typical_latent_group,
        typical_group, g1_centroids, g2_centroids,
        for_typical_map_2g1_centroids, for_typical_map_2g2_centroids,
        g1_centroids_index_as_typical_cluster, g2_centroids_index_as_typical_cluster,
        is_log, remove_diag
    )

    return prob_a1a3_in_a1a3, prob_a1a3_in_typical, \
        prob_typical_in_a1a3, prob_typical_in_typical, \
        aa_tog1_fgl_b, aa_tog2_fgl_b, \
        ta_tog1_fgl_b, ta_tog2_fgl_b, \
        at_tog1_fgl_b, at_tog2_fgl_b, \
        tt_tog1_fgl_b, tt_tog2_fgl_b


def get_prob_2dim(x_set,
                  adapted_a1a3_centroids, adapted_typical_centroids,
                  adapted_a1a3_mst_clusters, adapted_typical_mst_clusters,
                  length_universal_set,
                  adapted_a1a3_latent_group, adapted_typical_latent_group,
                  is_log='log',
                  remove_diag=False
                  ):

    g1_centroids = adapted_a1a3_centroids.copy()
    g2_centroids = adapted_typical_centroids.copy()

    map_a2a_centroids, map_a2t_centroids, \
        map_t2t_centroids, map_t2a_centroids = \
        map_g1_n_g2_centroids(len(adapted_a1a3_centroids),
                              len(adapted_typical_centroids))

    for_a1a3_map_2g1_centroids = map_a2a_centroids.copy()
    for_a1a3_map_2g2_centroids = map_a2t_centroids.copy()
    for_typical_map_2g1_centroids = map_t2a_centroids.copy()
    for_typical_map_2g2_centroids = map_t2t_centroids.copy()

    log_a1a3_p_g, log_typical_p_g = get_log_p_g(
        adapted_a1a3_mst_clusters,
        adapted_typical_mst_clusters,
        length_universal_set,
        is_log
    )

    prob_in_a1a3, x_in_a_tog1_fgl_b, x_in_a_tog2_fgl_b = get_prob_x_in_y(
        log_a1a3_p_g, adapted_a1a3_latent_group,
        x_set, g1_centroids, g2_centroids,
        for_a1a3_map_2g1_centroids, for_a1a3_map_2g2_centroids,
        is_log, remove_diag
    )

    prob_in_typical, x_in_t_tog1_fgl_b, x_in_t_tog2_fgl_b = get_prob_x_in_y(
        log_typical_p_g, adapted_typical_latent_group,
        x_set, g1_centroids, g2_centroids,
        for_typical_map_2g1_centroids, for_typical_map_2g2_centroids,
        is_log, remove_diag
    )
    return prob_in_a1a3, prob_in_typical, \
        x_in_a_tog1_fgl_b, x_in_a_tog2_fgl_b, \
        x_in_t_tog1_fgl_b, x_in_t_tog2_fgl_b
##############################################
##
##############################################


def get_params_by_category(
        adapted_y_latent_group,
        g1_centroids, g2_centroids,
        g1_centroids_index_as_cluster, g2_centroids_index_as_cluster,
     ):
    diver_b_g1cent, b_g1cent_params = \
        adapted_y_latent_group.get_diver_b_to_b2_centroids(
            g1_centroids, g1_centroids_index_as_cluster)
    diver_b_g2cent, b_g2cent_params = \
        adapted_y_latent_group.get_diver_b_to_b2_centroids(
            g2_centroids, g2_centroids_index_as_cluster)
    return diver_b_g1cent, b_g1cent_params, \
        diver_b_g2cent, b_g2cent_params


def get_probability_matrix_by_category(
        adapted_y_latent_group,
        x_set,
        g1_centroids,
        g2_centroids,
        diver_x_set_g1cent,
        diver_x_set_g2cent,
        y_g1cent_params,
        y_g2cent_params,
        map_g1_centroid,
        map_g2_centroid,
        log_y_p_g,
        remove_diag=False,
        is_testing=False
):
    y_to_g1cent_fgl_b_matrix = adapted_y_latent_group.x_set_to_b2_centroids(
        x_set, g1_centroids, map_g1_centroid, y_g1cent_params, diver_x_set_g1cent, remove_diag,
        is_testing)[1]

    y_to_g2cent_fgl_b_matrix = adapted_y_latent_group.x_set_to_b2_centroids(
        x_set, g2_centroids, map_g2_centroid, y_g2cent_params, diver_x_set_g2cent, remove_diag,
        is_testing)[1]

    tileg1_log_y_p_g = np.tile(log_y_p_g.T, (np.shape(y_to_g1cent_fgl_b_matrix)[0], 1, 1))
    pg_n_fglb_g1cent = np.concatenate([y_to_g1cent_fgl_b_matrix, tileg1_log_y_p_g], axis=2)
    # print(np.where(np.isnan(pg_n_fglb_g1cent)))

    tileg2_log_y_p_g = np.tile(log_y_p_g.T, (np.shape(y_to_g2cent_fgl_b_matrix)[0], 1, 1))
    pg_n_fglb_g2cent = np.concatenate([y_to_g2cent_fgl_b_matrix, tileg2_log_y_p_g], axis=2)
    # print(np.where(np.isnan(pg_n_fglb_g2cent)))

    return pg_n_fglb_g1cent, pg_n_fglb_g2cent
        #y_to_g1cent_fgl_b_matrix, y_to_g2cent_fgl_b_matrix


def get_probability_matrices(
        diver_x_set_g1cent,
        diver_x_set_g2cent,
        a1a3_g1cent_params,
        a1a3_g2cent_params,
        typical_g1cent_params,
        typical_g2cent_params,
        g1_centroids, g2_centroids,
        for_a1a3_map_2g1_centroids,
        for_a1a3_map_2g2_centroids,
        for_typical_map_2g1_centroids,
        for_typical_map_2g2_centroids,
        adapted_a1a3_latent_group, adapted_typical_latent_group,
        log_a1a3_p_g, log_typical_p_g,
        is_testing=False, x_set=None,
        remove_diag=False
):
    if is_testing and x_set is None:
        raise Exception('x_set must be provided in testing mode!')

    a1a3_to_g1cent_pg_n_fgl_b_matrix, a1a3_to_g2cent_pg_n_fgl_b_matrix =\
        get_probability_matrix_by_category(
            adapted_a1a3_latent_group,
            x_set,
            g1_centroids,
            g2_centroids,
            diver_x_set_g1cent,
            diver_x_set_g2cent,
            a1a3_g1cent_params,
            a1a3_g2cent_params,
            for_a1a3_map_2g1_centroids,
            for_a1a3_map_2g2_centroids,
            log_a1a3_p_g,
            remove_diag,
            is_testing
        )

    typical_to_g1cent_pg_n_fgl_b_matrix, typical_to_g2cent_pg_n_fgl_b_matrix =\
        get_probability_matrix_by_category(
            adapted_typical_latent_group,
            x_set,
            g1_centroids,
            g2_centroids,
            diver_x_set_g1cent,
            diver_x_set_g2cent,
            typical_g1cent_params,
            typical_g2cent_params,
            for_typical_map_2g1_centroids,
            for_typical_map_2g2_centroids,
            log_typical_p_g,
            remove_diag,
            is_testing
        )

    return a1a3_to_g1cent_pg_n_fgl_b_matrix, a1a3_to_g2cent_pg_n_fgl_b_matrix, \
        typical_to_g1cent_pg_n_fgl_b_matrix, typical_to_g2cent_pg_n_fgl_b_matrix


def get_params_by_fitting(
        adapted_a1a3_latent_group,
        adapted_typical_latent_group,
        adapted_a1a3_centroids, adapted_typical_centroids,
        adapted_a1a3_mst_clusters,
        adapted_typical_mst_clusters
):
    g1_centroids = adapted_a1a3_centroids.copy()
    g2_centroids = adapted_typical_centroids.copy()

    g1_centroids_index_as_a1a3_cluster = np.arange(len(adapted_a1a3_mst_clusters))
    g2_centroids_index_as_a1a3_cluster = np.zeros(len(adapted_a1a3_mst_clusters))
    g2_centroids_index_as_a1a3_cluster.fill(np.nan)

    g1_centroids_index_as_typical_cluster = np.zeros(len(adapted_typical_mst_clusters))
    g1_centroids_index_as_typical_cluster.fill(np.nan)
    g2_centroids_index_as_typical_cluster = np.arange(len(adapted_typical_mst_clusters))

    diver_a1a3_g1cent, a1a3_g1cent_params, \
        diver_a1a3_g2cent, a1a3_g2cent_params\
        = get_params_by_category(
            adapted_a1a3_latent_group,
            g1_centroids, g2_centroids,
            g1_centroids_index_as_a1a3_cluster,
            g2_centroids_index_as_a1a3_cluster)

    diver_typical_g1cent, typical_g1cent_params, \
        diver_typical_g2cent, typical_g2cent_params \
        = get_params_by_category(
                adapted_typical_latent_group,
                g1_centroids, g2_centroids,
                g1_centroids_index_as_typical_cluster,
                g2_centroids_index_as_typical_cluster)

    diver_x_set_g1cent = np.concatenate([diver_a1a3_g1cent, diver_typical_g1cent])
    diver_x_set_g2cent = np.concatenate([diver_a1a3_g2cent, diver_typical_g2cent])

    return diver_x_set_g1cent, \
        diver_x_set_g2cent, \
        a1a3_g1cent_params, \
        a1a3_g2cent_params, \
        typical_g1cent_params, \
        typical_g2cent_params


def get_pg_fglb_2dim(
        diver_x_set_g1cent,
        diver_x_set_g2cent,
        a1a3_g1cent_params,
        a1a3_g2cent_params,
        typical_g1cent_params,
        typical_g2cent_params,
        adapted_a1a3_centroids, adapted_typical_centroids,
        adapted_a1a3_mst_clusters, adapted_typical_mst_clusters,
        length_universal_set,
        adapted_a1a3_latent_group, adapted_typical_latent_group,
        is_log='log',
        remove_diag=False,
        is_testing=False,
        x_set=None
        ):
    g1_centroids = adapted_a1a3_centroids.copy()
    g2_centroids = adapted_typical_centroids.copy()

    map_a2a_centroids, map_a2t_centroids, \
        map_t2t_centroids, map_t2a_centroids = \
        map_g1_n_g2_centroids(len(adapted_a1a3_centroids),
                              len(adapted_typical_centroids))

    for_a1a3_map_2g1_centroids = map_a2a_centroids.copy()
    for_a1a3_map_2g2_centroids = map_a2t_centroids.copy()
    for_typical_map_2g1_centroids = map_t2a_centroids.copy()
    for_typical_map_2g2_centroids = map_t2t_centroids.copy()

    log_a1a3_p_g, log_typical_p_g = get_log_p_g(
        adapted_a1a3_mst_clusters,
        adapted_typical_mst_clusters,
        length_universal_set,
        is_log
    )

    a1a3_to_g1cent_pg_n_fgl_b_matrix, a1a3_to_g2cent_pg_n_fgl_b_matrix, \
        typical_to_g1cent_pg_n_fgl_b_matrix, typical_to_g2cent_pg_n_fgl_b_matrix = \
        get_probability_matrices(
            diver_x_set_g1cent,
            diver_x_set_g2cent,
            a1a3_g1cent_params,
            a1a3_g2cent_params,
            typical_g1cent_params,
            typical_g2cent_params,
            g1_centroids, g2_centroids,
            for_a1a3_map_2g1_centroids,
            for_a1a3_map_2g2_centroids,
            for_typical_map_2g1_centroids,
            for_typical_map_2g2_centroids,
            adapted_a1a3_latent_group, adapted_typical_latent_group,
            log_a1a3_p_g, log_typical_p_g,
            is_testing, x_set,
            remove_diag
        )
    return a1a3_to_g1cent_pg_n_fgl_b_matrix, a1a3_to_g2cent_pg_n_fgl_b_matrix, \
        typical_to_g1cent_pg_n_fgl_b_matrix, typical_to_g2cent_pg_n_fgl_b_matrix


def cluster_scoring():
    pass


def map_g1_n_g2_centroids(len_adapted_a1a3_centroids,
                          len_adapted_typical_centroids):
    # g1 -> a1a3
    map_a2a_centroids = np.arange(len_adapted_a1a3_centroids)
    map_a2t_centroids = np.zeros(len_adapted_typical_centroids)
    map_a2t_centroids.fill(np.nan)
    # g2 ->typical
    map_t2t_centroids = np.arange(len_adapted_typical_centroids)
    map_t2a_centroids = np.zeros(len_adapted_a1a3_centroids)
    map_t2a_centroids.fill(np.nan)
    return map_a2a_centroids, map_a2t_centroids, map_t2t_centroids, map_t2a_centroids
