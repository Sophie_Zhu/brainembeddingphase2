import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.metrics import f1_score

from tqdm import tqdm


def generate_prob_matrix_with_cluster_labels(
        a1a3_to_g1cent_pg_n_fgl_b_matrix,
        a1a3_to_g2cent_pg_n_fgl_b_matrix,
        typical_to_g1cent_pg_n_fgl_b_matrix,
        typical_to_g2cent_pg_n_fgl_b_matrix,
        a1a3_clusters,  typical_clusters,
        len_a1a3_clusters, len_typical_clusters,
        len_a1a3_group,
        is_pg=False
):
    len_samples = len(a1a3_to_g1cent_pg_n_fgl_b_matrix)
    matrix_for_scoring = []
    sample_weight = np.ones((len_samples, len_a1a3_clusters+len_typical_clusters))
    # balance_weight = 7
    # prefix = '/media/sophie/'
    # experiment_path = 'Samsung_T5/Experiment2019/K-fold_NYU/'
    # with pd.ExcelWriter(
    #         prefix + experiment_path + "norm_Probability_matrix+#cluster_label+#category_label0905.xlsx") as writer:
    for i in range(len_samples):
        if is_pg:
            df_x_in_a_tog1_fgl_b = pd.DataFrame(a1a3_to_g1cent_pg_n_fgl_b_matrix[i, :, :-1])
            df_x_in_a_tog2_fgl_b = pd.DataFrame(a1a3_to_g2cent_pg_n_fgl_b_matrix[i])
            df_x_in_t_tog1_fgl_b = pd.DataFrame(typical_to_g1cent_pg_n_fgl_b_matrix[i, :, :-1])
            df_x_in_t_tog2_fgl_b = pd.DataFrame(typical_to_g2cent_pg_n_fgl_b_matrix[i])
        else:
            df_x_in_a_tog1_fgl_b = pd.DataFrame(a1a3_to_g1cent_pg_n_fgl_b_matrix[i, :, :-1])
            df_x_in_a_tog2_fgl_b = pd.DataFrame(a1a3_to_g2cent_pg_n_fgl_b_matrix[i, :, :-1])
            df_x_in_t_tog1_fgl_b = pd.DataFrame(typical_to_g1cent_pg_n_fgl_b_matrix[i, :, :-1])
            df_x_in_t_tog2_fgl_b = pd.DataFrame(typical_to_g2cent_pg_n_fgl_b_matrix[i, :, :-1])

        if i < len_a1a3_group:
            y_a1a3 = np.ones(len_a1a3_clusters)
            y_typical = - np.ones(len_typical_clusters)
            # category label
            df_y = pd.concat([pd.DataFrame(y_a1a3), pd.DataFrame(y_typical)])
            df_y.columns = ['category_label']

            # cluster label
            cluster_a1a3_labels = -np.ones(len_a1a3_clusters)
            cluster_typical_labels = -np.ones(len_typical_clusters)

            aa = pd.concat([df_x_in_a_tog1_fgl_b, df_x_in_a_tog2_fgl_b], axis=1)
            at = pd.concat([df_x_in_t_tog1_fgl_b, df_x_in_t_tog2_fgl_b], axis=1)
            aa_at = pd.concat([aa, at])

            for cluster_idx in range(len_a1a3_clusters):
                # sample_weight[:, cluster_idx] = len(a1a3_clusters[cluster_idx])
                this_balance_weight = len_samples - len(a1a3_clusters[cluster_idx])
                if i in a1a3_clusters[cluster_idx]:
                    cluster_a1a3_labels[cluster_idx] = 1
                    sample_weight[i, cluster_idx] = this_balance_weight
                else:
                    sample_weight[i, cluster_idx] = len(a1a3_clusters[cluster_idx])
            for cluster_idx in range(len_typical_clusters):
                sample_weight[i, len_a1a3_clusters + cluster_idx] = len(typical_clusters[cluster_idx])


            df_a1a3_c = pd.DataFrame(cluster_a1a3_labels)
            df_typical_c = pd.DataFrame(cluster_typical_labels)
            df_c = pd.concat([df_a1a3_c, df_typical_c])
            df_c.columns = ['cluster_label']

            df_aa_at_y = pd.concat([aa_at, df_y], axis=1)
            df_aa_at_y_labels = pd.concat([df_aa_at_y, df_c], axis=1)
            # sheet_str = f"adhd_No{i}"
            # df_aa_at_y_labels.to_excel(writer, sheet_name=sheet_str)
            matrix_for_scoring.append(df_aa_at_y_labels.values)

        else:
            k = i - len_a1a3_group
            y_a1a3 = -np.ones(len_a1a3_clusters)
            y_typical = np.ones(len_typical_clusters)
            # category label
            df_y = pd.concat([pd.DataFrame(y_a1a3), pd.DataFrame(y_typical)])
            df_y.columns = ['category_label']

            # cluster label
            cluster_a1a3_labels = -np.ones(len_a1a3_clusters)
            cluster_typical_labels = -np.ones(len_typical_clusters)

            ta = pd.concat([df_x_in_a_tog1_fgl_b, df_x_in_a_tog2_fgl_b], axis=1)
            tt = pd.concat([df_x_in_t_tog1_fgl_b, df_x_in_t_tog2_fgl_b], axis=1)
            ta_tt = pd.concat([ta, tt])

            for cluster_idx in range(len_typical_clusters):
                # sample_weight[:, cluster_idx] = len(typical_clusters[cluster_idx])
                this_balance_weight = len_samples - len(typical_clusters[cluster_idx])
                if k in typical_clusters[cluster_idx]:
                    cluster_typical_labels[cluster_idx] = 1
                    sample_weight[i, len_a1a3_clusters+cluster_idx] = this_balance_weight
                else:
                    sample_weight[i, len_a1a3_clusters + cluster_idx] = len(typical_clusters[cluster_idx])
            for cluster_idx in range(len_a1a3_clusters):
                sample_weight[i, cluster_idx] = len(a1a3_clusters[cluster_idx])

            df_a1a3_c = pd.DataFrame(cluster_a1a3_labels)
            df_typical_c = pd.DataFrame(cluster_typical_labels)
            df_c = pd.concat([df_a1a3_c, df_typical_c])
            df_c.columns = ['cluster_label']

            df_ta_tt_y = pd.concat([ta_tt, df_y], axis=1)
            df_ta_tt_y_labels = pd.concat([df_ta_tt_y, df_c], axis=1)
            # sheet_str = f"typical_No{k}"
            # df_ta_tt_y_labels.to_excel(writer, sheet_name=sheet_str)
            matrix_for_scoring.append(df_ta_tt_y_labels.values)

    return np.array(matrix_for_scoring), sample_weight


def cluster_scoring_training(matrix_for_scoring, sample_weight):
    X = matrix_for_scoring[:, :, :-1]
    shape_X = np.shape(X)
    # X = X.reshape(shape_X[0]*shape_X[1], shape_X[2])`
    y = matrix_for_scoring[:, :, -1]
    shape_y = np.shape(y)
    # print(shape_y)
    # y = y.reshape(shape_y[0]*shape_y[1])

    reg_list = []
    reg_predict_list = []
    for i in range(shape_X[1]):
        this_X = X[:, i, :]
        this_X = this_X.reshape(shape_X[0], shape_X[2])
        # print(np.shape(this_X))
        this_y = y[:, i]
        this_sample_weight = sample_weight[:, i]
        # print(np.shape(this_y))
        this_reg = LinearRegression(normalize=True).fit(this_X, this_y, sample_weight=this_sample_weight)
        reg_list.append(this_reg)
        reg_predict_list.append(this_reg.predict(this_X))

    return reg_list, reg_predict_list


def cluster_scoring_testing(matrix_for_scoring, reg_list):
    X = matrix_for_scoring[:, :, :-1]
    shape_X = np.shape(X)
    reg_predict_list = []
    for i in range(shape_X[1]):
        this_X = X[:, i, :]
        this_X = this_X.reshape(shape_X[0], shape_X[2])
        this_reg = reg_list[i]
        reg_predict_list.append(this_reg.predict(this_X))
    return reg_predict_list


def get_sort_score(input_score_in_a1a3, input_score_in_typical, no_of_judges):
    no_of_subjects = np.shape(input_score_in_a1a3)[0]
    score_in_a1a3 = np.zeros((no_of_subjects, no_of_judges))
    score_in_typical = np.zeros((no_of_subjects, no_of_judges))
    for i in range(no_of_subjects):
        this_score_in_a1a3 = input_score_in_a1a3[i]
        score_in_a1a3[i, : len(this_score_in_a1a3)] = this_score_in_a1a3
        this_score_in_typical = input_score_in_typical[i]
        score_in_a1a3[i, : len(this_score_in_typical)] = this_score_in_typical

    to_a1a3_sort = [-np.sort(
        -score_in_a1a3[i]) for i in range(no_of_subjects)]
    to_typical_sort = [-np.sort(
        -score_in_typical[i]) for i in range(no_of_subjects)]

    return to_a1a3_sort, to_typical_sort


def leave_one_regression_sort(to_typical_sort, to_a1a3_sort,
                              len_a1a3_group, len_typical_group,
                              train_type="none"):
    y = np.concatenate([np.zeros(len_a1a3_group),
                        np.ones(len_typical_group)]
                       )
    predict_y = np.zeros(len_a1a3_group + len_typical_group)

    if train_type == "none":
        x = np.concatenate([to_typical_sort[:, :3].T, to_a1a3_sort[:, :3].T]).T
    if train_type == "leave one":
        x = np.concatenate([np.array(to_typical_sort).reshape(-1, 3).T,
                            np.array(to_a1a3_sort).reshape(-1, 3).T]).T
    # x[np.where(x == -np.inf)] = -1000

    for take_one in tqdm(range(len_a1a3_group + len_typical_group)):
        train_x = np.concatenate(
            [x[0:take_one],
             x[take_one + 1:]])

        train_y = np.concatenate(
            [y[0:take_one],
             y[take_one + 1:]])

        clf = LogisticRegression(random_state=0, solver='lbfgs',
                                 multi_class='ovr').fit(train_x, train_y)
        clf.score(train_x, train_y)
        predict_y[take_one] = clf.predict([x[take_one]])

    result = {
        'accurate_count': np.count_nonzero(predict_y == y),
        'adhd_accurate_count':
            np.count_nonzero(predict_y[:len_a1a3_group] == y[:len_a1a3_group]),
        'typical_accurate_count':
            np.count_nonzero(predict_y[len_a1a3_group:] == y[len_a1a3_group:]),
        'f1_score': f1_score(y, predict_y),
        'predict_y': predict_y
    }
    return result


def regression(to_typical, to_a1a3,
               len_a1a3_group, len_typical_group,
               train_type="none",
               is_training=True, trained_clf=None,
               ):
    y = np.concatenate([np.zeros(len_a1a3_group),
                        np.ones(len_typical_group)]
                       )

    if train_type == "none":
        x = np.concatenate([to_typical.T, to_a1a3.T]).T
    if train_type == "leave one":
        x = np.concatenate([np.array(to_typical).reshape(-1, 3).T,
                            np.array(to_a1a3).reshape(-1, 3).T]).T

    train_x = x.copy()
    train_y = y.copy()

    if is_training:
        clf = LogisticRegression(random_state=0, solver='lbfgs', fit_intercept=False,
                                 multi_class='ovr').fit(train_x, train_y)
        predict_y = clf.predict(train_x)
        result = {
            'trained_clf': clf,
            'accurate_count': np.count_nonzero(predict_y == y),
            'adhd_accurate_count':
                np.count_nonzero(predict_y[:len_a1a3_group] == y[:len_a1a3_group]),
            'typical_accurate_count':
                np.count_nonzero(predict_y[len_a1a3_group:] == y[len_a1a3_group:]),
            # 'f1_score': f1_score(y, predict_y),
            'predict_y': predict_y
        }

    else:
        if trained_clf is None:
            raise Exception("trained_clf must be provided in testing mode!")
        else:
            predict_y = trained_clf.predict(train_x)
            result = predict_y
    return result


def cluster_scoring_with_two_labels(
        matrix_for_scoring, sample_weight,
        is_training=True,
        trained_cate_reg_list=None,
        trained_cluster_reg_list=None
):
    cate_reg_list = []
    cate_reg_predict_list = []
    cluster_reg_list = []
    cluster_reg_predict_list =[]

    cate_X = matrix_for_scoring[:, :, :-2]
    shape_cate_X = np.shape(cate_X)
    # X = X.reshape(shape_X[0]*shape_X[1], shape_X[2])`
    cate_y = matrix_for_scoring[:, :, -2]
    shape_cate_y = np.shape(cate_y)

    for i in range(shape_cate_X[1]):
        this_X = cate_X[:, i, :]
        this_X = this_X.reshape(shape_cate_X[0], shape_cate_X[2])
        this_y = cate_y[:, i]
        if is_training:
            # print(f"i:{i},\n,{np.where(np.isnan(this_X))}")
            this_reg = LinearRegression(normalize=True).fit(this_X, this_y)
            cate_reg_list.append(this_reg)
        else:
            this_reg = trained_cate_reg_list[i]
        cate_reg_predict_list.append(this_reg.predict(this_X))

    cluster_X = matrix_for_scoring[:, :, :-2]
    shape_cluster_X = np.shape(cluster_X)
    # X = X.reshape(shape_X[0]*shape_X[1], shape_X[2])`
    cluster_y = matrix_for_scoring[:, :, -1]
    shape_cluster_y = np.shape(cluster_y)

    for i in range(shape_cluster_X[1]):
        this_X = cluster_X[:, i, :]
        this_X = this_X.reshape(shape_cluster_X[0], shape_cluster_X[2])
        # print(np.shape(this_X))
        this_y = cluster_y[:, i]
        this_sample_weight = sample_weight[:, i]
        # print(np.shape(this_y))
        if is_training:
            this_reg = LinearRegression(normalize=True).fit(this_X, this_y, sample_weight=this_sample_weight)
            cluster_reg_list.append(this_reg)
        else:
            this_reg = trained_cluster_reg_list[i]
        cluster_reg_predict_list.append(this_reg.predict(this_X))

    if is_training:
        return cate_reg_list, cate_reg_predict_list, \
            cluster_reg_list, cluster_reg_predict_list
    else:
        return cate_reg_predict_list, cluster_reg_predict_list
