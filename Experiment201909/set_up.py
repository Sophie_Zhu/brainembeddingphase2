import numpy as np
from tqdm import tqdm
from B2BNet_v2.read_write_utility import \
     write_mst_n_hubs, \
     write_closeness, \
     write_leave_one_closeness, \
     write_leave_one_mst_n_hubs, \
     load_divergence

from B2BNet_v2.set_latent_groups import \
    write_local_centroids_n_latent_groups, \
    write_leave_one_local_centroids_n_latent_groups, \
    write_global_centroid

from B2BNet_v2.load_group_data import \
    load_data, load_representation, resolve_valid_positions

from B2BNet_v2.hub_alignment import HubAlignment

experiment_path = 'Samsung_T5/Experiment2019/Exp201909_NYU/'

data_dict = load_data()
a1a3_group = data_dict['a1a3_group']
typical_group = data_dict['typical_group']
ta1a3_group = data_dict['ta1a3_group']

# write_closeness('Typical', len(typical_group), experiment_path)
# write_closeness('ADHD', len(a1a3_group), experiment_path)
##############################
# STEP 1: write msts and initial hubs
##############################
# write_mst_n_hubs('Typical', len(typical_group), experiment_path, hub_top_k=5)
# write_mst_n_hubs('ADHD', len(a1a3_group), experiment_path, hub_top_k=5)

# default hub_thres =5
representation_dict = load_representation(data_dict)
input_representations = representation_dict['input_representations']
individuals_relative = representation_dict['individuals_relative']
valid_position_dict = resolve_valid_positions(input_representations, ta1a3_group)
valid_positions = valid_position_dict['valid_positions']
universal_length = len(ta1a3_group)

##############################
# STEP 2: align initial hubs
##############################
# leave_twice_hub_alignment_class = HubAlignment(experiment_path)
# leave_twice_hub_alignment_class.write_aligned_hubs()
# ##############################
# # STEP 3: write local centroids and latent_groups
# ##############################
adhd_b_diver = load_divergence('ADHD', experiment_path)
typical_b_diver = load_divergence('Typical', experiment_path)

# write_local_centroids_n_latent_groups(
#     adhd_b_diver,
#     a1a3_group,
#     -1,
#     'ADHD',
#     experiment_path,
#     individuals_relative,
#     valid_positions
# )
#
# write_local_centroids_n_latent_groups(
#     typical_b_diver,
#     typical_group,
#     -1,
#     'Typical',
#     experiment_path,
#     individuals_relative,
#     valid_positions,
#     hub_top_k=5
# )


for outer_take_one in tqdm(range(universal_length)):
    if outer_take_one < len(a1a3_group):
        write_local_centroids_n_latent_groups(
                adhd_b_diver,
                a1a3_group,
                outer_take_one,
                'ADHD',
                experiment_path,
                individuals_relative,
                valid_positions
        )
        # leave on typical

    else:
        this_out_take_one = outer_take_one - len(a1a3_group)
        # leave on adhd
        write_local_centroids_n_latent_groups(
                typical_b_diver,
                typical_group,
                this_out_take_one,
                'Typical',
                experiment_path,
                individuals_relative,
                valid_positions,
                hub_top_k=5
        )

#
# # write_leave_one_closeness('Typical', len(typical_group), experiment_path)
# # write_leave_one_closeness('ADHD', len(a1a3_group), experiment_path)
#

##############################
# STEP 1: write msts and initial hubs
##############################
# write_leave_one_mst_n_hubs('Typical', len(typical_group), experiment_path, hub_top_k=5)
# write_leave_one_mst_n_hubs('ADHD', len(a1a3_group), experiment_path, hub_top_k=5)

##############################
# STEP 2: align initial hubs
##############################
# hub_alignment_class = HubAlignment(experiment_path)
# hub_alignment_class.hub_alignment()
#
# # ##############################
# # # STEP 3: write local centroids and latent_groups
# # ##############################
# write_leave_one_local_centroids_n_latent_groups(
#         a1a3_group,
#         'ADHD',
#         experiment_path,
#         individuals_relative,
#         valid_positions
# )
# #
# #
# write_leave_one_local_centroids_n_latent_groups(
#         typical_group,
#         'Typical',
#         experiment_path,
#         individuals_relative,
#         valid_positions,
#         hub_top_k=5
# )
############################
# global centroid
############################
# # write_global_centroid(
# #     typical_group,
# #     'Typical',
# #     experiment_path,
# #     individuals_relative,
# #     valid_positions,
# # )
# #
# # write_global_centroid(
# #     a1a3_group,
# #     'ADHD',
# #     experiment_path,
# #     individuals_relative,
# #     valid_positions,
# # )
