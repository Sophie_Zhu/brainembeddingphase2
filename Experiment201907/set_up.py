import numpy as np
from B2BNet.utility import \
     write_mst_n_hubs, \
     write_closeness, \
     write_leave_one_closeness, \
     write_leave_one_mst_n_hubs

from B2BNet.set_latent_groups import \
    write_local_centroids_n_latent_groups, \
    write_leave_one_local_centroids_n_latent_groups, \
    write_global_centroid

from B2BNet.load_group_data import \
    load_data, load_representation, resolve_valid_positions

experiment_path = 'Samsung_T5/Experiment2019/K-fold_NYU/'

data_dict = load_data()
a1a3_group = data_dict['a1a3_group']
typical_group = data_dict['typical_group']
ta1a3_group = data_dict['ta1a3_group']

# write_closeness('Typical', len(typical_group), experiment_path)
# write_closeness('ADHD', len(a1a3_group), experiment_path)

# write_mst_n_hubs('Typical', len(typical_group), experiment_path, hub_thres=6)
# write_mst_n_hubs('ADHD', len(a1a3_group), experiment_path)

# default hub_thres =5
representation_dict = load_representation(data_dict)
input_representations = representation_dict['input_representations']
individuals_relative = representation_dict['individuals_relative']
valid_position_dict = resolve_valid_positions(input_representations, ta1a3_group)
valid_positions = valid_position_dict['valid_positions']


write_local_centroids_n_latent_groups(
        a1a3_group,
        'ADHD',
        experiment_path,
        individuals_relative,
        valid_positions
)
#
#
# write_local_centroids_n_latent_groups(
#         typical_group,
#         'Typical',
#         experiment_path,
#         individuals_relative,
#         valid_positions,
#         hub_thres=6
# )

# write_leave_one_closeness('Typical', len(typical_group), experiment_path)
# write_leave_one_closeness('ADHD', len(a1a3_group), experiment_path)
#
# write_leave_one_mst_n_hubs('Typical', len(typical_group), experiment_path, hub_thres=6)
# write_leave_one_mst_n_hubs('ADHD', len(a1a3_group), experiment_path)

# write_leave_one_local_centroids_n_latent_groups(
#         a1a3_group,
#         'ADHD',
#         experiment_path,
#         individuals_relative,
#         valid_positions
# )


# write_leave_one_local_centroids_n_latent_groups(
#         typical_group,
#         'Typical',
#         experiment_path,
#         individuals_relative,
#         valid_positions,
#         hub_thres=6
# )

# write_global_centroid(
#     typical_group,
#     'Typical',
#     experiment_path,
#     individuals_relative,
#     valid_positions,
# )
#
# write_global_centroid(
#     a1a3_group,
#     'ADHD',
#     experiment_path,
#     individuals_relative,
#     valid_positions,
# )
