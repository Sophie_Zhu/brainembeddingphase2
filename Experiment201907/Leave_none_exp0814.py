import sys
sys.path.append('/home/sophie/Workspace')
import numpy as np
import matplotlib.pylab as plt
from scipy import stats
import pandas as pd
from BrainEmbeddingPhase2.B2BNet.debug_leave_one_utility \
    import LeaveOne
from BrainEmbeddingPhase2.B2BNet.cluster_scoring import *

experiment_path = 'Samsung_T5/Experiment2019/K-fold_NYU/'

# to_centroids_test = LeaveOne(experiment_path)
# # universal_clusters_to_centroids = \
# #     to_centroids_test.clusters_to_centroids_leave_none(is_diag=True)


# leave_none_test = LeaveOne(experiment_path)
# list_x_in_a_tog1_fgl_b, \
# list_x_in_a_tog2_fgl_b, \
# list_x_in_t_tog1_fgl_b, \
# list_x_in_t_tog2_fgl_b, \
# diver_a_g1cent, diver_a_g2cent, \
# diver_t_g1cent, diver_t_g2cent = leave_none_test.leave_none_test(is_log='sum', is_gauss='skewnorm')

prob_matrices_results = get_prob_matrices()

# matrix_for_scoring, sample_weight= generate_prob_matrix_with_cluster_labels(
#         *get_prob_matrices())