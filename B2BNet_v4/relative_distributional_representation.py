
import numpy as np


def sum_representations(distribution_representations, selected_indexes):
    # 所有大脑的联合分布之和
    sum_r = np.sum(distribution_representations[selected_indexes], axis=0)
    valid_positions = np.where(sum_r > 0)
    return sum_r, valid_positions


def relative_representations(distribution_representations, selected_indexes, all_indexes):
    # 大脑b的相对频度
    sum_r, valid_positions = sum_representations(distribution_representations, all_indexes)
    result = np.zeros(np.shape(distribution_representations[selected_indexes]))
    result[:] = np.nan
    individuals = distribution_representations[selected_indexes]
    for i in range(len(selected_indexes)):
        result[i][valid_positions] = individuals[i][valid_positions] / \
                              sum_r[valid_positions]
    return result


def group_relative_representations(distribution_representations, group_indexes, all_indexes):
    # 组C的相对频度
    sum_r_group, valid_positions_group = sum_representations(distribution_representations, group_indexes)
    sum_r_all, valid_positions_all = sum_representations(distribution_representations, all_indexes)
    result = np.zeros(np.shape(sum_r_group))
    result[:] = np.nan
    result[valid_positions_all] = sum_r_group[valid_positions_all] / sum_r_all[valid_positions_all]
    return result

