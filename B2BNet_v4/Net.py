import pickle
from tqdm import tqdm
from .load_group_data import \
    load_data,\
    path_prefix, kl_matrix_dict
from .DivergenceMatrix import load_divergence, get_leave_one_matrix
import networkx as nx


def f_net(kl_matrix_smooth):
    G = nx.Graph()
    group_len = len(kl_matrix_smooth)
    for i in range(group_len):
        for j in range(group_len):
            if i != j:
                G.add_edge(i, j, weight=kl_matrix_smooth[i, j])
    return G


class Net:
    def __init__(self, experiment_path):
        self.experiment_path = experiment_path

    def SaveNet(self, group_name, kl_matrix_smooth):
        net = f_net(kl_matrix_smooth)
        with open(path_prefix + self.experiment_path + kl_matrix_dict[group_name] +
                  "_leave_none_net.txt", "wb") as fp:
            pickle.dump(net, fp)

    def LoadNet(self, group_name):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[group_name] +
                      "_leave_none_net.txt", 'rb')
        group_leave_none_net = pickle.load(infile)
        infile.close()
        return group_leave_none_net

    def SaveEdgeList(self, group_name):
        net = self.LoadNet(group_name)
        nx.write_weighted_edgelist(net,
                                    path_prefix +
                                    self.experiment_path +
                                    kl_matrix_dict[group_name] +
                                    "_leave_none_net.weighted.edgelist")

    def LoadEdgeList(self, group_name):
        nx.read_weighted_edgelist(
            path_prefix +
            self.experiment_path +
            kl_matrix_dict[group_name] +
            "_leave_none_net.weighted.edgelist")




