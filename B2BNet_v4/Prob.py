import pickle
import numpy as np
from tqdm import tqdm
from .PDFParam import load_leave_one_pdf_params_for_training
from .DivergenceVector import \
    load_leave_one_diver_for_training, \
    load_leave_one_diver_for_test, \
    load_leave_one_pick_take_one_diver_for_training
from .load_group_data import \
    path_prefix, load_data
from .params_utility import x_set_to_b2_centroids_by_b_clusters


def write_leave_one_prob_for_training(experiment_path, is_gauss='norm'):
    data_dict = load_data()
    ta1a3_group = data_dict['ta1a3_group']
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    len_universal = len_a1a3_group + len_typical_group

    leave_one_diver = load_leave_one_diver_for_training(experiment_path)

    leave_one_adhd_pdf_params, leave_one_typical_pdf_params = \
        load_leave_one_pdf_params_for_training(experiment_path)

    leave_one_fgl_b_matrix = []
    for leave_one in tqdm(range(len_universal)):
        take_one_fgl_b_matrix = []
        for take_one in tqdm(range(len_universal)):
            this_diver = np.array(leave_one_diver[leave_one][take_one])
            this_adhd_pdf_params = leave_one_adhd_pdf_params[leave_one][take_one]
            this_typical_pdf_params = leave_one_typical_pdf_params[leave_one][take_one]

            fgl_b_matrix_by_adhd_cluster = \
                x_set_to_b2_centroids_by_b_clusters(is_gauss,
                                                    this_adhd_pdf_params,
                                                    this_diver)
            fgl_b_matrix_by_typical_cluster = \
                x_set_to_b2_centroids_by_b_clusters(is_gauss,
                                                    this_typical_pdf_params,
                                                    this_diver)
            take_one_fgl_b_matrix.append(
                np.concatenate([fgl_b_matrix_by_adhd_cluster, fgl_b_matrix_by_typical_cluster], axis=1)
            )

        # leave_one_fgl_b_matrix.append(take_one_fgl_b_matrix)
        with open(path_prefix + experiment_path +
                  f"_train_leave_one_{leave_one}_prob.txt", "wb") as fp:
            pickle.dump(take_one_fgl_b_matrix, fp)

    # with open(path_prefix + experiment_path +
    #           "_train_leave_one_prob.txt", "wb") as fp:
    #     pickle.dump(leave_one_fgl_b_matrix, fp)


def load_leave_one_prob_for_training(experiment_path, leave_one):
    infile = open(path_prefix + experiment_path +
                  f"_train_leave_one_{leave_one}_prob.txt", 'rb')
    train_leave_one_prob = pickle.load(infile)
    infile.close()
    return train_leave_one_prob


# TODO: merge this
def write_leave_one_prob_for_test(experiment_path, is_gauss='norm'):
    data_dict = load_data()
    ta1a3_group = data_dict['ta1a3_group']
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    len_universal = len_a1a3_group + len_typical_group

    leave_one_diver = load_leave_one_diver_for_test(experiment_path)
    leave_one_adhd_pdf_params, leave_one_typical_pdf_params = \
        load_leave_one_pdf_params_for_training(experiment_path)

    leave_one_fgl_b_matrix = []
    for leave_one in tqdm(range(len_universal)):
        take_one_fgl_b_matrix = []
        for take_one in tqdm(range(len_universal)):
            this_diver = np.array(leave_one_diver[leave_one][take_one]).reshape(1, -1)
            # print(np.shape(this_diver))
            this_adhd_pdf_params = leave_one_adhd_pdf_params[leave_one][take_one]
            this_typical_pdf_params = leave_one_typical_pdf_params[leave_one][take_one]

            fgl_b_matrix_by_adhd_cluster = \
                x_set_to_b2_centroids_by_b_clusters(is_gauss,
                                                    this_adhd_pdf_params,
                                                    this_diver)
            fgl_b_matrix_by_typical_cluster = \
                x_set_to_b2_centroids_by_b_clusters(is_gauss,
                                                    this_typical_pdf_params,
                                                    this_diver)
            take_one_fgl_b_matrix.append(
                np.concatenate([fgl_b_matrix_by_adhd_cluster, fgl_b_matrix_by_typical_cluster], axis=1)
            )

        leave_one_fgl_b_matrix.append(take_one_fgl_b_matrix)
        # with open(path_prefix + experiment_path +
        #           f"_train_leave_one_{leave_one}_prob.txt", "wb") as fp:
        #     pickle.dump(take_one_fgl_b_matrix, fp)

    with open(path_prefix + experiment_path +
              "_test_leave_one_prob.txt", "wb") as fp:
        pickle.dump(leave_one_fgl_b_matrix, fp)


def load_leave_one_prob_for_test(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_test_leave_one_prob.txt", 'rb')
    test_take_one_prob = pickle.load(infile)
    infile.close()
    return test_take_one_prob


def write_leave_one_pick_take_one_prob_for_training(experiment_path, is_gauss='norm'):
    data_dict = load_data()
    ta1a3_group = data_dict['ta1a3_group']
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    len_universal = len_a1a3_group + len_typical_group

    leave_one_diver = load_leave_one_pick_take_one_diver_for_training(experiment_path)
    leave_one_adhd_pdf_params, leave_one_typical_pdf_params = \
        load_leave_one_pdf_params_for_training(experiment_path)

    leave_one_fgl_b_matrix = []
    for leave_one in tqdm(range(len_universal)):
        take_one_fgl_b_matrix = []
        for take_one in tqdm(range(len_universal)):
            this_diver = np.array(leave_one_diver[leave_one][take_one]).reshape(1, -1)
            # print(np.shape(this_diver))
            this_adhd_pdf_params = leave_one_adhd_pdf_params[leave_one][take_one]
            this_typical_pdf_params = leave_one_typical_pdf_params[leave_one][take_one]

            fgl_b_matrix_by_adhd_cluster = \
                x_set_to_b2_centroids_by_b_clusters(is_gauss,
                                                    this_adhd_pdf_params,
                                                    this_diver)
            fgl_b_matrix_by_typical_cluster = \
                x_set_to_b2_centroids_by_b_clusters(is_gauss,
                                                    this_typical_pdf_params,
                                                    this_diver)
            take_one_fgl_b_matrix.append(
                np.concatenate([fgl_b_matrix_by_adhd_cluster, fgl_b_matrix_by_typical_cluster], axis=1)
            )

        leave_one_fgl_b_matrix.append(take_one_fgl_b_matrix)
        # with open(path_prefix + experiment_path +
        #           f"_train_leave_one_{leave_one}_prob.txt", "wb") as fp:
        #     pickle.dump(take_one_fgl_b_matrix, fp)

    with open(path_prefix + experiment_path +
              "_train_leave_one_pick_take_one_prob.txt", "wb") as fp:
        pickle.dump(leave_one_fgl_b_matrix, fp)


def load_leave_one_pick_take_one_prob_for_training(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_train_leave_one_pick_take_one_prob.txt", 'rb')
    train_take_one_prob = pickle.load(infile)
    infile.close()
    return train_take_one_prob
