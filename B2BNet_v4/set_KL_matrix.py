from B2BNet_v3.load_group_data import load_data, load_representation, \
    resolve_valid_positions, ubuntu_prefix, kl_matrix_dict
from B2BNet_v3.DivergenceMatrix import f_divergence, load_divergence
import numpy as np
import networkx as nx

data_dict = load_data()
representation_dict = load_representation(data_dict)

# Load some useful group indexes

adhd3_group = data_dict['adhd3_group']
adhd1_group = data_dict['adhd1_group']
ta1a3_group = data_dict['ta1a3_group']
a1a3_group = data_dict['a1a3_group']
typical_group = data_dict['typical_group']
adhd1_end = len(adhd1_group)
adhd3_end = len(adhd1_group) + len(adhd3_group)
typical_end = adhd3_end + len(typical_group)
input_representations = representation_dict['input_representations']
individuals_relative = representation_dict['individuals_relative']

# Load valid coordination in the representation
valid_position_dict = resolve_valid_positions(input_representations, ta1a3_group)
valid_positions = valid_position_dict['valid_positions']

kl_typical = f_divergence(individuals_relative[typical_group], [], valid_positions, is_smooth=True)
kl_adhd = f_divergence(individuals_relative[a1a3_group], [], valid_positions, is_smooth=True)

experiment_path = ubuntu_prefix + 'Samsung_T5/Experiment2019/Exp201911_NYU/'

kl_typical_name = kl_matrix_dict['Typical']
kl_adhd_name = kl_matrix_dict['ADHD']
np.save(experiment_path + kl_typical_name + '_smooth', kl_typical)
np.save(experiment_path + kl_adhd_name + '_smooth', kl_adhd)

kl_universal = f_divergence(individuals_relative[ta1a3_group], [], valid_positions, is_smooth=True)

kl_universal_name = kl_matrix_dict['Universal']
np.save(experiment_path + kl_universal_name + '_smooth', kl_universal)

#
# universal_diver = load_divergence('Universal', experiment_path)
# G = nx.Graph()
# group_len = len(a1a3_group)
# for i in range(group_len):
#     for j in range(group_len):
#         if i < j:
#             G.add_edge(i, j,
#                        weighted=
#                        (universal_diver[i, j]+universal_diver[j, i])/2.0
#                        )












