import numpy as np
from tqdm import tqdm

from B2BNet_v4.DivergenceMatrix import load_smooth_divergence
from B2BNet_v4.MST import MST
from B2BNet_v4.Net import Net
from B2BNet_v4.Hubs import Hubs, write_leave_one_centroids_by_index
from B2BNet_v4.DivergenceVector import write_leave_one_diver_for_training, \
    write_leave_one_diver_for_test, write_leave_one_pick_take_one_diver_for_training
from B2BNet_v4.PDFParam import write_leave_one_pdf_params_for_training
from B2BNet_v4.Prob import write_leave_one_prob_for_training, \
     write_leave_one_prob_for_test, write_leave_one_pick_take_one_prob_for_training
# from B2BNet_v3.Weights import Weights

experiment_path = 'Samsung_T5/Experiment2019/Exp201911_NYU/'
##############################
# STEP 0: load divergence
##############################
# adhd_diver_smooth = load_smooth_divergence('ADHD', experiment_path)
# typical_diver_smooth = load_smooth_divergence('Typical', experiment_path)
# net = Net(experiment_path)
# net.SaveNet('ADHD', adhd_diver_smooth)
# net.SaveNet('Typical', typical_diver_smooth)
# net.SaveEdgeList('ADHD')
# net.SaveEdgeList('Typical')

# universal_diver_smooth = load_smooth_divergence('Universal', experiment_path)
# net = Net(experiment_path)
# net.SaveNet('Universal', universal_diver_smooth)
##############################
# STEP 1: write msts
##############################
# mst = MST(experiment_path)
#
# mst.WriteLeaveNoneMST('ADHD')
# mst.WriteTakeOneMST('ADHD')
# mst.WriteLeaveOneMST('ADHD')
#
# mst.WriteLeaveNoneMST('Typical')
# mst.WriteTakeOneMST('Typical')
# mst.WriteLeaveOneMST('Typical')

##############################
# STEP 2: write hubs
##############################
# hubs = Hubs(experiment_path)

# print('write adhd hubs...')
# hubs.WriteLeaveNoneHubs('ADHD')
# hubs.WriteTakeOneHubs('ADHD')
# hubs.WriteLeaveOneHubs('ADHD')
#
# print('write typical hubs...')
# hubs.WriteLeaveNoneHubs('Typical')
# hubs.WriteTakeOneHubs('Typical')
# hubs.WriteLeaveOneHubs('Typical')

# print('write adhd leave none aligned hubs...')
# hubs.WriteLeaveNoneAlignedHubs('ADHD')
# print('write adhd take one aligned hubs...')
# hubs.WriteTakeOneAlignedHubs('ADHD')
# print('write adhd leave one aligned hubs...')
# hubs.WriteLeaveOneAlignedHubs('ADHD')

# print('write typical leave none aligned hubs...')
# hubs.WriteLeaveNoneAlignedHubs('Typical')
# print('write typical take one aligned hubs...')
# hubs.WriteTakeOneAlignedHubs('Typical')
# print('write typical leave one aligned hubs...')
# hubs.WriteLeaveOneAlignedHubs('Typical')

##############################
# STEP 3: write centroidss
##############################
# write_leave_one_centroids_by_index(experiment_path)


##############################
# STEP 3: write divergence vector
##############################
# write_leave_one_diver_for_training(experiment_path)
# write_leave_one_diver_for_test(experiment_path)
##############################
# STEP 4: write estimated pdf params
##############################
# write_leave_one_pdf_params_for_training(experiment_path)

##############################
# STEP 5: write prob
##############################
write_leave_one_prob_for_training(experiment_path)
write_leave_one_prob_for_test(experiment_path)
##############################
# STEP 6: cnn
##############################
# write_leave_one_pick_take_one_diver_for_training(experiment_path)
# write_leave_one_pick_take_one_prob_for_training(experiment_path)


