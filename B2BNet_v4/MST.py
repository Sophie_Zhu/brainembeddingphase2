import pickle
from tqdm import tqdm
from .load_group_data import \
    load_data,\
    path_prefix, kl_matrix_dict
from .DivergenceMatrix import load_divergence, get_leave_one_matrix
from .Net import Net
import networkx as nx


class MST:
    def __init__(self, experiment_path):
        # self.group_name = group_name
        self.experiment_path = experiment_path
        self.Net = Net(self.experiment_path)

        self.data_dict = load_data()
        # if group_name == 'ADHD':
        #     self.this_group = self.data_dict['a1a3_group']
        #
        # elif group_name == 'Typical':
        #     self.this_group = self.data_dict['typical_group']
        # self.group_len = len(self.this_group)

    def WriteLeaveNoneMST(self, group_name):

        net = self.Net.LoadNet(group_name)

        # print(f"net degree: {net.degree}")
        # print(f"net edge: {net.edges(data=True)}")
        # print(sorted(net.edges(data=True)))
        mst = nx.minimum_spanning_tree(net)
        # print(f"mst degree: {mst.degree}")
        # print(sorted(mst.edges(data=True)))
        print(f"writing mst (leave_none) - {group_name} group ...")
        with open(path_prefix + self.experiment_path + kl_matrix_dict[group_name] +
                  "_leave_none_mst.txt", "wb") as fp:
            pickle.dump(mst, fp)

    def LoadLeaveNoneMST(self, group_name):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[group_name] +
                      "_leave_none_mst.txt", 'rb')
        group_leave_none_mst = pickle.load(infile)
        infile.close()
        return group_leave_none_mst

    def WriteTakeOneMST(self, group_name):
        group_take_one_mst = []

        if group_name == 'ADHD':
            this_group = self.data_dict['a1a3_group']

        elif group_name == 'Typical':
            this_group = self.data_dict['typical_group']

        group_len = len(this_group)

        full_net = self.Net.LoadNet(group_name)

        for i in tqdm(range(group_len)):
            net = full_net.copy()
            net.remove_node(i)
            mst = nx.minimum_spanning_tree(net)
            group_take_one_mst.append(mst)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[group_name] +
                  "_take_one_mst.txt", "wb") as fp:
            pickle.dump(group_take_one_mst, fp)

    def LoadTakeOneMST(self, group_name):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[group_name] +
                      "_take_one_mst.txt", 'rb')
        group_take_one_mst = pickle.load(infile)
        infile.close()
        return group_take_one_mst

    def WriteLeaveOneMST(self, group_name):
        if group_name == 'ADHD':
            this_group = self.data_dict['a1a3_group']

        elif group_name == 'Typical':
            this_group = self.data_dict['typical_group']

        group_len = len(this_group)

        full_net = self.Net.LoadNet(group_name)

        group_leave_twice_mst = []
        for i in tqdm(range(group_len)):
            #
            inner_mst = []
            for j in range(group_len):
                net = full_net.copy()
                net.remove_nodes_from([i, j])
                mst = nx.minimum_spanning_tree(net)

                inner_mst.append(mst)

            group_leave_twice_mst.append(inner_mst)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[group_name] +
                  "_leave_one_mst.txt", "wb") as fp:
            pickle.dump(group_leave_twice_mst, fp)

    def LoadLeaveOneMST(self, group_name):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[group_name] +
                      "_leave_one_mst.txt", 'rb')
        group_leave_twice_mst = pickle.load(infile)
        infile.close()
        return group_leave_twice_mst
