import pickle
import numpy as np
from tqdm import tqdm
from .DivergenceVector import load_leave_one_diver_for_training
from .load_group_data import \
    path_prefix, load_data
from .params_utility import b_set_to_b2_centroids_moments


def f_local_latent_groups(diver_b_to_x_cents, out_indexes):
    diver_size = np.shape(diver_b_to_x_cents)
    length_clusters = diver_size[1]
    x_clusters = [set() for _ in range(length_clusters)]

    for idx_in_b in range(diver_size[0]):
        if idx_in_b not in out_indexes:
            m_index = np.argmin(diver_b_to_x_cents[idx_in_b])
            x_clusters[int(m_index)].add(idx_in_b)
    return x_clusters


def write_leave_one_pdf_params_for_training(experiment_path, is_gauss='norm', hub_top_k=5):
    data_dict = load_data()
    ta1a3_group = data_dict['ta1a3_group']
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    len_universal = len_a1a3_group + len_typical_group

    leave_one_adhd_cluster_pdf_params = []
    leave_one_typical_cluster_pdf_params = []
    # centroids size: n,200,200
    leave_one_diver = np.array(load_leave_one_diver_for_training(experiment_path))
    leave_one_adhd_clusters = []
    leave_one_typical_clusters = []

    for leave_one in tqdm(range(len_universal)):
        take_one_adhd_cluster_pdf_params = []
        take_one_typical_cluster_pdf_params = []

        take_one_adhd_clusters = []
        take_one_typical_clusters = []

        for take_one in tqdm(range(len_universal)):
            this_diver = leave_one_diver[leave_one][take_one]
            this_adhd_diver_cents = this_diver[:len_a1a3_group, :]
            this_typical_diver_cents = this_diver[len_a1a3_group:, :]

            this_adhd_cluster = f_local_latent_groups(this_adhd_diver_cents[:, :hub_top_k],
                                                      [leave_one, take_one])

            this_typical_cluster = f_local_latent_groups(this_typical_diver_cents[:, hub_top_k:],
                                                         [leave_one - len_a1a3_group,
                                                          take_one - len_a1a3_group])

            adhd_estimated_pdf_params = \
                b_set_to_b2_centroids_moments(is_gauss, this_adhd_diver_cents, this_adhd_cluster)

            typical_estimated_pdf_params = \
                b_set_to_b2_centroids_moments(is_gauss, this_typical_diver_cents, this_typical_cluster)

            take_one_adhd_cluster_pdf_params.append(adhd_estimated_pdf_params)
            take_one_typical_cluster_pdf_params.append(typical_estimated_pdf_params)

            take_one_adhd_clusters.append(this_adhd_cluster)
            take_one_typical_clusters.append(this_typical_cluster)

        leave_one_adhd_cluster_pdf_params.append(take_one_adhd_cluster_pdf_params)
        leave_one_typical_cluster_pdf_params.append(take_one_typical_cluster_pdf_params)
        leave_one_adhd_clusters.append(take_one_adhd_clusters)
        leave_one_typical_clusters.append(take_one_typical_clusters)

    with open(path_prefix + experiment_path +
              "_leave_one_adhd_pdf_params.txt", "wb") as fp:
        pickle.dump(leave_one_adhd_cluster_pdf_params, fp)
    with open(path_prefix + experiment_path +
              "_leave_one_typical_pdf_params.txt", "wb") as fp:
        pickle.dump(leave_one_typical_cluster_pdf_params, fp)

    with open(path_prefix + experiment_path +
              "_leave_one_adhd_clusters.txt", "wb") as fp:
        pickle.dump(leave_one_adhd_clusters, fp)
    with open(path_prefix + experiment_path +
              "_leave_one_typical_clusters.txt", "wb") as fp:
        pickle.dump(leave_one_typical_clusters, fp)


def load_leave_one_pdf_params_for_training(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_leave_one_adhd_pdf_params.txt", 'rb')
    leave_one_adhd_pdf_params = pickle.load(infile)
    infile.close()
    infile = open(path_prefix + experiment_path +
                  "_leave_one_typical_pdf_params.txt", 'rb')
    leave_one_typical_pdf_params = pickle.load(infile)
    infile.close()
    return leave_one_adhd_pdf_params, leave_one_typical_pdf_params


def load_leave_one_clusters(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_leave_one_adhd_clusters.txt", 'rb')
    leave_one_adhd_clusters = pickle.load(infile)
    infile.close()
    infile = open(path_prefix + experiment_path +
                  "_leave_one_typical_clusters.txt", 'rb')
    leave_one_typical_clusters = pickle.load(infile)
    infile.close()
    return leave_one_adhd_clusters, leave_one_typical_clusters
