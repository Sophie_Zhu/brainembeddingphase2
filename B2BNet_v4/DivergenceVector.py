import pickle
from tqdm import tqdm
from .DivergenceMatrix import load_smooth_divergence
from .Hubs import load_leave_one_centroids_by_index
from .load_group_data import \
    path_prefix, load_data


def write_leave_one_diver_for_test(experiment_path):
    data_dict = load_data()
    ta1a3_group = data_dict['ta1a3_group']
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    len_universal = len_a1a3_group + len_typical_group
    leave_one_centroids = load_leave_one_centroids_by_index(experiment_path)
    leave_one_centroids_indexes = leave_one_centroids.astype(int)
    kl_matrix = load_smooth_divergence('Universal', experiment_path)

    leave_one_diver_b_to_cents = []
    for leave_one in tqdm(range(len_universal)):
        take_one_diver_b_to_cents = []
        for take_one in tqdm(range(len_universal)):

            this_leave_one_hubs = leave_one_centroids_indexes[leave_one][take_one]

            diver_to_g1g2 = kl_matrix[leave_one, this_leave_one_hubs]

            take_one_diver_b_to_cents.append(diver_to_g1g2)

        leave_one_diver_b_to_cents.append(take_one_diver_b_to_cents)

    with open(path_prefix + experiment_path +
              "_test_leave_one_diver_b_to_cents.txt", "wb") as fp:
        pickle.dump(leave_one_diver_b_to_cents, fp)


def load_leave_one_diver_for_test(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_test_leave_one_diver_b_to_cents.txt", 'rb')
    leave_one_diver_b_to_cents = pickle.load(infile)
    infile.close()
    return leave_one_diver_b_to_cents


def write_leave_one_diver_for_training(experiment_path):
    data_dict = load_data()
    ta1a3_group = data_dict['ta1a3_group']
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    len_universal = len_a1a3_group + len_typical_group
    leave_one_centroids = load_leave_one_centroids_by_index(experiment_path)
    leave_one_centroids_indexes = leave_one_centroids.astype(int)
    kl_matrix = load_smooth_divergence('Universal', experiment_path)

    leave_one_diver_b_to_cents = []

    for leave_one in tqdm(range(len_universal)):
        take_one_diver_b_to_cents = []
        for take_one in tqdm(range(len_universal)):

            diver_to_g1g2 = kl_matrix[:, leave_one_centroids_indexes[leave_one][take_one]]

            take_one_diver_b_to_cents.append(diver_to_g1g2)

        leave_one_diver_b_to_cents.append(take_one_diver_b_to_cents)

    with open(path_prefix + experiment_path +
              "_leave_one_diver_b_to_cents.txt", "wb") as fp:
        pickle.dump(leave_one_diver_b_to_cents, fp)


def load_leave_one_diver_for_training(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_leave_one_diver_b_to_cents.txt", 'rb')
    leave_one_diver_b_to_cents = pickle.load(infile)
    infile.close()
    return leave_one_diver_b_to_cents


# TODO merge
def write_leave_one_pick_take_one_diver_for_training(experiment_path):
    data_dict = load_data()
    ta1a3_group = data_dict['ta1a3_group']
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    len_universal = len_a1a3_group + len_typical_group
    leave_one_centroids = load_leave_one_centroids_by_index(experiment_path)
    leave_one_centroids_indexes = leave_one_centroids.astype(int)
    kl_matrix = load_smooth_divergence('Universal', experiment_path)

    leave_one_diver_b_to_cents = []

    for leave_one in tqdm(range(len_universal)):
        take_one_diver_b_to_cents = []
        for take_one in tqdm(range(len_universal)):
            this_leave_one_hubs = leave_one_centroids_indexes[leave_one][take_one]

            diver_to_g1g2 = kl_matrix[take_one, this_leave_one_hubs]

            # diver_to_g1g2 = this_diver[take_one, leave_one_centroids_indexes[leave_one][take_one]]

            take_one_diver_b_to_cents.append(diver_to_g1g2)

        leave_one_diver_b_to_cents.append(take_one_diver_b_to_cents)

    with open(path_prefix + experiment_path +
              "_leave_one_pick_one_diver_b_to_cents.txt", "wb") as fp:
        pickle.dump(leave_one_diver_b_to_cents, fp)


def load_leave_one_pick_take_one_diver_for_training(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_leave_one_pick_one_diver_b_to_cents.txt", 'rb')
    leave_one_diver_b_to_cents = pickle.load(infile)
    infile.close()
    return leave_one_diver_b_to_cents
