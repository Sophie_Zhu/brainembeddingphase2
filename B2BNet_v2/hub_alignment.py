import copy
from tqdm import tqdm
import pickle
from collections import Counter

import numpy as np
from .load_group_data import \
    path_prefix, kl_matrix_dict, \
    load_data, load_representation, \
    resolve_valid_positions
from .read_write_utility import \
    load_mst_n_hubs, \
    load_leave_one_mst_n_hubs, \
    load_leave_one_local_centroids_n_latent_groups, \
    get_leave_ij_set, \
    load_global_centroid_n_cluster
from .B2BNet_utility import f_divergence

'''
Four scenarios:

outer leave one out : in adhd
inner leave one out : in adhd

outer leave one out : in adhd
inner leave one out : in typical

outer leave one out : in typical
inner leave one out : in adhd


outer leave one out : in typical
inner leave one out : in typical
'''


class HubAlignment:
    def __init__(self, experiment_path, hub_top_k=5):
        # experiment_path = 'Samsung_T5/Experiment2019/K-fold_NYU/'
        self.hub_top_k = hub_top_k
        self.experiment_path = experiment_path
        self.data_dict = load_data()
        self.a1a3_group = self.data_dict['a1a3_group']
        self.typical_group = self.data_dict['typical_group']
        self.ta1a3_group = self.data_dict['ta1a3_group']
        self.adhd3_end = self.data_dict['adhd3_end']
        self.length_universal_set = len(self.ta1a3_group)

        representation_dict = load_representation(self.data_dict)
        input_representations = representation_dict['input_representations']
        self.individuals_relative = representation_dict['individuals_relative']
        valid_position_dict = resolve_valid_positions(input_representations, self.ta1a3_group)
        self.valid_positions = valid_position_dict['valid_positions']

        self.leave_one_dict = {}
        self.take_one_inner_leave_one_dict = []

    def load_leave_one(self):
        leave_one_typical_mst, leave_one_typical_hubs = \
            load_leave_one_mst_n_hubs('Typical', self.experiment_path)

        leave_one_a1a3_mst, leave_one_a1a3_hubs = \
            load_leave_one_mst_n_hubs('ADHD', self.experiment_path)

        self.leave_one_dict = {
            'typical_mst': leave_one_typical_mst,
            'typical_hubs': leave_one_typical_hubs,
            'a1a3_mst': leave_one_a1a3_mst,
            'a1a3_hubs': leave_one_a1a3_hubs
        }

    def load_leave_twice(self):
        self.load_leave_one()
        leave_twice_typical_mst, leave_twice_typical_hubs = \
            load_mst_n_hubs('Typical', self.experiment_path)

        print(f"shape leave_twice_typical_hubs:{ np.shape(np.array(leave_twice_typical_hubs))} ")

        leave_twice_a1a3_mst, leave_twice_a1a3_hubs = \
            load_mst_n_hubs('ADHD', self.experiment_path)

        take_one_inner_leave_one_dict = []
        print("load_leave_twice ...")
        for outer_take_one in tqdm(range(len(self.ta1a3_group))):
            this_inner_leave_one_dict = {
                        'typical_hubs': [],
                        'a1a3_hubs': []
                        #,
                        #'adapted_typical_set': [],
                        #'adapted_a1a3_set': []
                    }
            for inner_take_one in tqdm(range(len(self.ta1a3_group) - 1)):
                ################################
                # scenario 1
                # outer leave one out : in adhd
                # inner leave one out : in adhd
                if outer_take_one < self.adhd3_end and inner_take_one < self.adhd3_end - 1:
                    # adapted_a1a3_set = \
                    #     get_leave_ij_set(self.a1a3_group, outer_take_one, inner_take_one)
                    # adapted_typical_set = self.typical_group.copy()

                    inner_leave_one_dict = {
                        'typical_hubs': self.leave_one_dict['typical_hubs'][-1], # leave none
                        'a1a3_hubs':  leave_twice_a1a3_hubs[outer_take_one][inner_take_one]
                        #,
                        #'adapted_typical_set': adapted_typical_set,
                        #'adapted_a1a3_set': adapted_a1a3_set
                    }
                ################################
                # scenario 2
                # outer leave one out : in adhd
                # inner leave one out : in typical
                if outer_take_one < self.adhd3_end and inner_take_one >= self.adhd3_end - 1:
                    typical_take_one = inner_take_one - (self.adhd3_end - 1)
                    # adapted_a1a3_set = get_leave_ij_set(self.a1a3_group, outer_take_one, -1)
                    # adapted_typical_set = get_leave_ij_set(self.typical_group, typical_take_one, -1)

                    inner_leave_one_dict = {
                        'typical_hubs':
                            self.leave_one_dict['typical_hubs'][typical_take_one],
                        'a1a3_hubs':
                            self.leave_one_dict['a1a3_hubs'][outer_take_one]
                        #,
                        #'adapted_typical_set': adapted_typical_set,
                        #'adapted_a1a3_set': adapted_a1a3_set
                    }
                ################################
                # scenario 3
                # outer leave one out : in typical
                # inner leave one out : in adhd
                if outer_take_one >= self.adhd3_end and inner_take_one < self.adhd3_end:
                    typical_take_one = outer_take_one - self.adhd3_end
                    # adapted_a1a3_set = get_leave_ij_set(self.a1a3_group, inner_take_one, -1)
                    # adapted_typical_set = get_leave_ij_set(self.typical_group, typical_take_one, -1)
                    inner_leave_one_dict = {
                        'typical_hubs':
                            self.leave_one_dict['typical_hubs'][typical_take_one],
                        'a1a3_hubs':
                            self.leave_one_dict['a1a3_hubs'][inner_take_one]
                        #,
                        #'adapted_typical_set': adapted_typical_set,
                        #'adapted_a1a3_set': adapted_a1a3_set
                    }
                ################################
                # scenario 4
                # outer leave one out : in typical
                # inner leave one out : in typical
                if outer_take_one >= self.adhd3_end and inner_take_one >= self.adhd3_end:
                    typical_take_one_outer = outer_take_one - self.adhd3_end
                    typical_take_one_inner = inner_take_one - self.adhd3_end
                    # adapted_typical_set = \
                    #     get_leave_ij_set(self.typical_group,
                    #                      typical_take_one_outer,
                    #                      typical_take_one_inner)
                    #
                    # adapted_a1a3_set = self.a1a3_group.copy()

                    inner_leave_one_dict = {
                        'typical_hubs': leave_twice_typical_hubs[typical_take_one_outer][typical_take_one_inner],
                        'a1a3_hubs': self.leave_one_dict['a1a3_hubs'][-1]
                        #,  # leave none
                        #'adapted_typical_set': adapted_typical_set,
                        #'adapted_a1a3_set': adapted_a1a3_set
                    }

                this_inner_leave_one_dict['a1a3_hubs'].append(inner_leave_one_dict['a1a3_hubs'])
                this_inner_leave_one_dict['typical_hubs'].append(inner_leave_one_dict['typical_hubs'])
                # this_inner_leave_one_dict['adapted_typical_set'].append(inner_leave_one_dict['adapted_typical_set'])
                # this_inner_leave_one_dict['adapted_a1a3_set'].append(inner_leave_one_dict['adapted_a1a3_set'])
            take_one_inner_leave_one_dict.append(this_inner_leave_one_dict)
        self.take_one_inner_leave_one_dict = take_one_inner_leave_one_dict

    #
    def write_aligned_hubs(self):
        self.load_leave_twice()

        for outer_take_one in tqdm(range(len(self.ta1a3_group))):
            print(f"writing outer_take_one #{outer_take_one} ...")
            inner_leave_one_dict = self.take_one_inner_leave_one_dict[outer_take_one]
            adapted_a1a3_group = self.a1a3_group.copy()
            adapted_typical_group = self.typical_group.copy()
            if outer_take_one < self.adhd3_end:
                adapted_a1a3_group = get_leave_ij_set(self.a1a3_group, outer_take_one, -1)
            elif outer_take_one >= self.adhd3_end:
                typical_take_one = outer_take_one - self.adhd3_end
                adapted_typical_group = get_leave_ij_set(self.typical_group, typical_take_one, -1)

            self.inner_hub_alignment(
                outer_take_one,
                inner_leave_one_dict,
                adapted_a1a3_group,
                adapted_typical_group
            )

    def count_inner_hub_freq(self, inner_leave_one_dict, adapted_a1a3_length):
        a1a3_hub_count = []
        typical_hub_count = []
        print("count_inner_hub_freq ...")
        for inner_take_one in tqdm(range(len(self.ta1a3_group) - 1)):
            # print(f"take one:{take_one}")
            if inner_take_one < adapted_a1a3_length:
                hubs = np.array(inner_leave_one_dict['a1a3_hubs'][inner_take_one])
                # a1a3_mst = self.leave_one_dict['a1a3_mst'][take_one]
                # print([a1a3_mst.degree[hub_idx] for hub_idx in hubs])

                adjust_idx = np.where(hubs >= inner_take_one)
                hubs[adjust_idx] = hubs[adjust_idx] + 1
                # print(f"hubs:{hubs}")
                # this_mst = self.leave_one_dict['a1a3_mst'][take_one]
                a1a3_hub_count = np.concatenate([a1a3_hub_count, hubs])

            elif inner_take_one >= adapted_a1a3_length:
                typical_take_one = inner_take_one - self.adhd3_end
                hubs = np.array(inner_leave_one_dict['typical_hubs'][typical_take_one])
                # typical_mst = self.leave_one_dict['typical_mst'][typical_take_one]
                # print([typical_mst.degree[hub_idx] for hub_idx in hubs])
                adjust_idx = np.where(hubs >= typical_take_one)
                hubs[adjust_idx] = hubs[adjust_idx] + 1
                # this_mst = self.leave_one_dict['typical_mst'][typical_take_one]
                typical_hub_count = np.concatenate([typical_hub_count, hubs])

        a1a3_recounted = Counter(list(a1a3_hub_count))
        a1a3_sorted_hubs = np.array([pair[0] for pair in sorted(a1a3_recounted.items(), key=lambda item: item[1])])

        typical_recounted = Counter(list(typical_hub_count))
        typical_sorted_hubs = np.array([pair[0] for pair in sorted(typical_recounted.items(), key=lambda item: item[1])])

        return a1a3_sorted_hubs[-self.hub_top_k:], typical_sorted_hubs[-self.hub_top_k:]

    def inner_template_clusters(self, b_set, hubs):
        length_clusters = self.hub_top_k
        initial_clusters = [set() for _ in range(length_clusters)]
        centroids = self.individuals_relative[np.array(b_set)[hubs]]

        for idx_in_b in range(len(b_set)):
            diver_indexes_in_b_hubs = \
                self.get_diver_x_set_cent_y_smooth(  # smooth, attention!
                    [b_set[idx_in_b]],
                    centroids)

            m_index = np.argmin(diver_indexes_in_b_hubs)
            initial_clusters[int(m_index)].add(idx_in_b)
        return initial_clusters

    def inner_hub_alignment(self,
                            outer_i,
                            inner_leave_one_dict,
                            adapted_a1a3_group, adapted_typical_group
                            ):
        adapted_a1a3_length = len(adapted_a1a3_group)
        a1a3_template_hubs, typical_template_hubs = \
            self.count_inner_hub_freq(inner_leave_one_dict, adapted_a1a3_length)
        a1a3_template_hubs = [int(i) for i in a1a3_template_hubs]
        typical_template_hubs = [int(i) for i in typical_template_hubs]
        a1a3_template_clusters = self.template_clusters(adapted_a1a3_group, a1a3_template_hubs)
        typical_template_clusters = self.template_clusters(adapted_typical_group, typical_template_hubs)
        a1a3_group_leave_one_hubs = []
        typical_group_leave_one_hubs = []
        inner_length = len(self.ta1a3_group) - 1
        for inner_take_one in tqdm(range(inner_length + 2)):
            # TODO:Debug
            # print(f"take one:{inner_take_one}")
            if inner_take_one < adapted_a1a3_length or inner_take_one == inner_length:
                this_take_one = -1 if inner_take_one == inner_length else inner_take_one
                hubs = np.array(inner_leave_one_dict['a1a3_hubs'][this_take_one])
                # a1a3_mst = self.leave_one_dict['a1a3_mst'][inner_take_one]
                # print([a1a3_mst.degree[hub_idx] for hub_idx in hubs])

                adjust_idx = np.where(hubs >= this_take_one)
                # hubs[adjust_idx] = hubs[adjust_idx] + 1
                # print(f"hubs:{hubs}")
                group_name = 'ADHD'
                this_group = adapted_a1a3_group #self.a1a3_group
                template_hubs = a1a3_template_hubs.copy()
                template_clusters = a1a3_template_clusters.copy()

            elif inner_take_one >= adapted_a1a3_length or inner_take_one == inner_length + 1:
                typical_take_one = -1 if inner_take_one == inner_length + 1 else inner_take_one - adapted_a1a3_length
                hubs = np.array(inner_leave_one_dict['typical_hubs'][typical_take_one])
                # if 84 in hubs:
                #     print("hihi 84")
                #     print(hubs)
                # typical_mst = self.leave_one_dict['typical_mst'][typical_take_one]
                # print([typical_mst.degree[hub_idx] for hub_idx in hubs])
                adjust_idx = np.where(hubs >= typical_take_one)
                group_name = 'Typical'
                this_group = adapted_typical_group #self.typical_group
                template_hubs = typical_template_hubs.copy()
                template_clusters = typical_template_clusters.copy()
            #####################################

            #####################################
            hubs_reindex = hubs.copy()
            if inner_take_one < inner_length:
                hubs_reindex[adjust_idx] = hubs[adjust_idx] + 1
            hubs_reindex[adjust_idx] = hubs[adjust_idx] + 1
            hubs_aligned = hubs.copy()[:self.hub_top_k]
            hubs_reindex_aligned = hubs.copy()[:self.hub_top_k]
            #####################################
            template_hubs_left = list(template_hubs.copy())
            to_be_aligned_hubs_left = list(hubs_reindex.copy())
            left_index = []
            aligned_index = []
            this_i = 0
            # TODO:Debug
            # print(f"template_ hubs{template_hubs}")
            for hub_in_template in template_hubs:
                # print( hub_in_template)
                idx = np.where(hubs_reindex == int(hub_in_template))
                if len(idx[0]) > 0:
                    template_hubs_left.remove(hub_in_template)
                    to_be_aligned_hubs_left.remove(hubs_reindex[idx[0]])
                    # print("hihi!")
                    # print(template_hubs_left)
                    # print(this_i, int(idx[0]))
                    hubs_aligned[this_i] = hubs[idx[0]]
                    hubs_reindex_aligned[this_i] = hubs_reindex[idx[0]]
                    aligned_index.append(this_i)

                else:
                    left_index.append(this_i)

                this_i += 1

            #####################################
            template_hubs_left = [int(i) for i in template_hubs_left]
            left_index_with_hub_candidates = []
            # print(f"hubs_to_be_aligned{hubs_reindex}")
            # print(f"template_hubs_left: {template_hubs_left}")
            # print(f"aligned : {np.array(aligned_index)}")

            # print(f"left_index:{left_index}")

            if len(template_hubs_left) > 0:
                for this_left_index in left_index:
                    this_candidates = []
                    for this_hub in hubs_reindex:
                    # if this_hub not in hubs_reindex_aligned[np.array(aligned_index)]:
                        if this_hub in to_be_aligned_hubs_left:
                            #print(f"this_hub:{this_hub}")
                            #print(f"template_cluster {this_left_index}:{template_clusters[this_left_index]}")
                            if this_hub in template_clusters[this_left_index]:
                                #print("in this cluster!")
                                this_candidates.append(this_hub)
                    left_index_with_hub_candidates.append(this_candidates)

            # print(f"left_index_with_hub_candidates{left_index_with_hub_candidates}")
            idx = -1
            for this_left_index in left_index:
                idx += 1
                this_candidates = np.array(left_index_with_hub_candidates)[idx]
                # print(np.shape(this_candidates))
                if len(this_candidates) > 0:
                    if len(this_candidates) == 1:
                        this_candidates = this_candidates[0]
                        reindex_hub = this_candidates
                    else:
                        # print(np.array(this_group)[this_candidates])
                        this_hubs_to_hub_in_template = f_divergence(
                            self.individuals_relative[np.array(this_group)[this_candidates]],
                            [self.individuals_relative[np.array(this_group)[template_hubs[this_left_index]]]],
                            self.valid_positions, is_same=False, is_smooth=True)
                        # print("hihi")
                        # print(this_hubs_to_hub_in_template)
                        min_idx = np.argmin(this_hubs_to_hub_in_template)
                        reindex_hub = this_candidates[min_idx]

                    left_index.remove(this_left_index)
                    # print(f"reindex_hub:{reindex_hub}")
                    to_be_aligned_hubs_left.remove(reindex_hub)
                    hubs_aligned[this_left_index] = hubs[np.where(hubs_reindex == reindex_hub)]
                    hubs_reindex_aligned[this_left_index] = reindex_hub

            ## pick the shortest divergence.
            for this_left_index in left_index:
                this_candidates = np.array(to_be_aligned_hubs_left)
                this_hubs_to_hub_in_template = f_divergence(
                    self.individuals_relative[np.array(this_group)[this_candidates]],
                    [self.individuals_relative[np.array(this_group)[template_hubs[this_left_index]]]],
                    self.valid_positions, is_same=False, is_smooth=True)
                min_idx = np.argmin(this_hubs_to_hub_in_template)
                reindex_hub = this_candidates[min_idx]
                hubs_aligned[this_left_index] = hubs[np.where(hubs_reindex == reindex_hub)]
                hubs_reindex_aligned[this_left_index] = reindex_hub

            # TODO:Debug
            # print(f"aligned_hubs:{hubs_reindex_aligned}")
            # print(f"hubs_before:{hubs}")
            # print(f"hubs_aligned:{hubs_aligned}")

            if group_name == "ADHD":
                a1a3_group_leave_one_hubs.append(np.array(hubs_aligned))
            elif group_name == "Typical":
                typical_group_leave_one_hubs.append(np.array(hubs_aligned))

        with open(path_prefix + self.experiment_path + kl_matrix_dict["ADHD"] +
                  f"outer_{outer_i}_leave_one_hubs_aligned.txt", "wb") as fp:
            pickle.dump(a1a3_group_leave_one_hubs, fp)
        with open(path_prefix + self.experiment_path + kl_matrix_dict["Typical"] +
                  f"outer_{outer_i}_leave_one_hubs_aligned.txt", "wb") as fp:
            pickle.dump(typical_group_leave_one_hubs, fp)


#############################################
    def count_hub_freq(self):
        self.load_leave_one()
        a1a3_hub_count = []
        typical_hub_count = []
        for take_one in tqdm(range(len(self.ta1a3_group))):
            # print(f"take one:{take_one}")
            if take_one < self.adhd3_end:
                hubs = np.array(self.leave_one_dict['a1a3_hubs'][take_one])
                # a1a3_mst = self.leave_one_dict['a1a3_mst'][take_one]
                # print([a1a3_mst.degree[hub_idx] for hub_idx in hubs])

                adjust_idx = np.where(hubs >= take_one)
                hubs[adjust_idx] = hubs[adjust_idx] + 1
                # print(f"hubs:{hubs}")
                this_mst = self.leave_one_dict['a1a3_mst'][take_one]
                a1a3_hub_count = np.concatenate([a1a3_hub_count, hubs])

            elif take_one >= self.adhd3_end:
                typical_take_one = take_one - self.adhd3_end
                hubs = np.array(self.leave_one_dict['typical_hubs'][typical_take_one])
                # typical_mst = self.leave_one_dict['typical_mst'][typical_take_one]
                # print([typical_mst.degree[hub_idx] for hub_idx in hubs])
                adjust_idx = np.where(hubs >= typical_take_one)
                hubs[adjust_idx] = hubs[adjust_idx] + 1
                this_mst = self.leave_one_dict['typical_mst'][typical_take_one]
                typical_hub_count = np.concatenate([typical_hub_count, hubs])

            #####################################
            # print(this_mst)
            # print(f"degrees:{[this_mst.degree[hub_idx] for hub_idx in hubs]}")
            # hubs[adjust_idx] = hubs[adjust_idx] + 1
            # print(f"hubs{hubs}")
            # hub_count = np.concatenate([hub_count, hubs])
        # print(list(hub_count))
        a1a3_recounted = Counter(list(a1a3_hub_count))
        a1a3_sorted_hubs = np.array([pair[0] for pair in sorted(a1a3_recounted.items(), key=lambda item: item[1])])

        typical_recounted = Counter(list(typical_hub_count))
        typical_sorted_hubs = np.array([pair[0] for pair in sorted(typical_recounted.items(), key=lambda item: item[1])])

        return a1a3_sorted_hubs[-self.hub_top_k:], typical_sorted_hubs[-self.hub_top_k:]

    def template_clusters(self, b_set, hubs):
        length_clusters = self.hub_top_k
        initial_clusters = [set() for _ in range(length_clusters)]
        centroids = self.individuals_relative[np.array(b_set)[hubs]]

        for idx_in_b in range(len(b_set)):
            diver_indexes_in_b_hubs = \
                self.get_diver_x_set_cent_y_smooth(  # smooth, attention!
                    [b_set[idx_in_b]],
                    centroids)

            m_index = np.argmin(diver_indexes_in_b_hubs)
            initial_clusters[int(m_index)].add(idx_in_b)
        return initial_clusters

    def hub_alignment(self):
        a1a3_template_hubs, typical_template_hubs = self.count_hub_freq()
        a1a3_template_hubs = [int(i) for i in a1a3_template_hubs]
        typical_template_hubs = [int(i) for i in typical_template_hubs]
        a1a3_template_clusters = self.template_clusters(self.a1a3_group, a1a3_template_hubs)
        typical_template_clusters = self.template_clusters(self.typical_group, typical_template_hubs)
        a1a3_group_leave_one_hubs = []
        typical_group_leave_one_hubs = []
        ##########################
        ## last one: no leave out
        #########################
        for take_one in tqdm(range(len(self.ta1a3_group) + 2)): # leave none for two categories
            # TODO:Debug
            # print(f"take one:{take_one}")
            if take_one < self.adhd3_end or take_one == len(self.ta1a3_group):
                this_take_one = -1 if take_one == len(self.ta1a3_group) else take_one
                hubs = np.array(self.leave_one_dict['a1a3_hubs'][this_take_one])
                # a1a3_mst = self.leave_one_dict['a1a3_mst'][take_one]
                # print([a1a3_mst.degree[hub_idx] for hub_idx in hubs])

                adjust_idx = np.where(hubs >= this_take_one)
                # hubs[adjust_idx] = hubs[adjust_idx] + 1
                # print(f"hubs:{hubs}")
                group_name = 'ADHD'
                this_group = self.a1a3_group
                template_hubs = a1a3_template_hubs.copy()
                template_clusters = a1a3_template_clusters.copy()

            elif take_one >= self.adhd3_end or take_one == len(self.ta1a3_group) + 1:
                typical_take_one = -1 if take_one == len(self.ta1a3_group) + 1 else take_one - self.adhd3_end
                hubs = np.array(self.leave_one_dict['typical_hubs'][typical_take_one])
                # if 84 in hubs:
                #     print("hihi 84")
                #     print(hubs)
                # typical_mst = self.leave_one_dict['typical_mst'][typical_take_one]
                # print([typical_mst.degree[hub_idx] for hub_idx in hubs])
                adjust_idx = np.where(hubs >= typical_take_one)
                group_name = 'Typical'
                this_group = self.typical_group
                template_hubs = typical_template_hubs.copy()
                template_clusters = typical_template_clusters.copy()
            #####################################


            #####################################
            hubs_reindex = hubs.copy()
            if take_one < len(self.ta1a3_group):
                hubs_reindex[adjust_idx] = hubs[adjust_idx] + 1
            hubs_aligned = hubs.copy()[:self.hub_top_k]
            hubs_reindex_aligned = hubs.copy()[:self.hub_top_k]
            #####################################
            template_hubs_left = list(template_hubs.copy())
            to_be_aligned_hubs_left = list(hubs_reindex.copy())
            left_index = []
            aligned_index = []
            this_i = 0
            # TODO:Debug
            # print(f"template_ hubs{template_hubs}")
            for hub_in_template in template_hubs:
                # print( hub_in_template)
                idx = np.where(hubs_reindex == int(hub_in_template))
                if len(idx[0])>0:
                    template_hubs_left.remove(hub_in_template)
                    to_be_aligned_hubs_left.remove(hubs_reindex[idx[0]])
                    # print("hihi!")
                    # print(template_hubs_left)
                    # print(this_i, int(idx[0]))
                    hubs_aligned[this_i] = hubs[idx[0]]
                    hubs_reindex_aligned[this_i] = hubs_reindex[idx[0]]
                    aligned_index.append(this_i)

                else:
                    left_index.append(this_i)

                this_i += 1

            #####################################
            template_hubs_left = [int(i) for i in template_hubs_left]
            left_index_with_hub_candidates = []
            # print(f"hubs_to_be_aligned{hubs_reindex}")
            # print(f"template_hubs_left: {template_hubs_left}")
            # print(f"aligned : {np.array(aligned_index)}")

            # print(f"left_index:{left_index}")

            if len(template_hubs_left) > 0:
                for this_left_index in left_index:
                    this_candidates = []
                    for this_hub in hubs_reindex:
                    # if this_hub not in hubs_reindex_aligned[np.array(aligned_index)]:
                        if this_hub in to_be_aligned_hubs_left:
                            #print(f"this_hub:{this_hub}")
                            #print(f"template_cluster {this_left_index}:{template_clusters[this_left_index]}")
                            if this_hub in template_clusters[this_left_index]:
                                #print("in this cluster!")
                                this_candidates.append(this_hub)
                    left_index_with_hub_candidates.append(this_candidates)

            # print(f"left_index_with_hub_candidates{left_index_with_hub_candidates}")
            idx = -1
            for this_left_index in left_index:
                idx += 1
                this_candidates = np.array(left_index_with_hub_candidates)[idx]
                # print(np.shape(this_candidates))
                if len(this_candidates) > 0:
                    if len(this_candidates) == 1:
                        this_candidates = this_candidates[0]
                        reindex_hub = this_candidates
                    else:
                        # print(np.array(this_group)[this_candidates])
                        this_hubs_to_hub_in_template = f_divergence(
                            self.individuals_relative[np.array(this_group)[this_candidates]],
                            [self.individuals_relative[np.array(this_group)[template_hubs[this_left_index]]]],
                            self.valid_positions, is_same=False, is_smooth=True)
                        # print("hihi")
                        # print(this_hubs_to_hub_in_template)
                        min_idx = np.argmin(this_hubs_to_hub_in_template)
                        reindex_hub = this_candidates[min_idx]

                    left_index.remove(this_left_index)
                    # print(f"reindex_hub:{reindex_hub}")
                    to_be_aligned_hubs_left.remove(reindex_hub)
                    hubs_aligned[this_left_index] = hubs[np.where(hubs_reindex == reindex_hub)]
                    hubs_reindex_aligned[this_left_index] = reindex_hub

            ## pick the shortest divergence.
            for this_left_index in left_index:
                this_candidates = np.array(to_be_aligned_hubs_left)
                this_hubs_to_hub_in_template = f_divergence(
                    self.individuals_relative[np.array(this_group)[this_candidates]],
                    [self.individuals_relative[np.array(this_group)[template_hubs[this_left_index]]]],
                    self.valid_positions, is_same=False, is_smooth=True)
                min_idx = np.argmin(this_hubs_to_hub_in_template)
                reindex_hub = this_candidates[min_idx]
                hubs_aligned[this_left_index] = hubs[np.where(hubs_reindex == reindex_hub)]
                hubs_reindex_aligned[this_left_index] = reindex_hub

            # TODO:Debug
            # print(f"aligned_hubs:{hubs_reindex_aligned}")
            # print(f"hubs_before:{hubs}")
            # print(f"hubs_aligned:{hubs_aligned}")

            if group_name == "ADHD":
                a1a3_group_leave_one_hubs.append(np.array(hubs_aligned))
            elif group_name == "Typical":
                typical_group_leave_one_hubs.append(np.array(hubs_aligned))

        with open(path_prefix + self.experiment_path + kl_matrix_dict["ADHD"] +
                  "_leave_one_hubs_aligned.txt", "wb") as fp:
            pickle.dump(a1a3_group_leave_one_hubs, fp)
        with open(path_prefix + self.experiment_path + kl_matrix_dict["Typical"] +
                  "_leave_one_hubs_aligned.txt", "wb") as fp:
            pickle.dump(typical_group_leave_one_hubs, fp)

    def get_diver_x_set_cent_y_smooth(self, x_set, y_centroids):
        return f_divergence(
            self.individuals_relative[x_set],
            y_centroids,
            self.valid_positions,
            is_same=False
        )







