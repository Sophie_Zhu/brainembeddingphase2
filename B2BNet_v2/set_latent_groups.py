import pickle
import numpy as np
from tqdm import tqdm
from .load_group_data import \
    path_prefix, \
    kl_matrix_dict
from .read_write_utility import load_closeness, load_divergence, load_mst_n_hubs, \
    load_leave_one_closeness, load_leave_one_mst_n_hubs, get_leave_ij_set, \
    get_leave_ij_divergence, \
    load_mst_n_aligned_hubs, load_leave_one_mst_n_aligned_hubs
from .latent_group_by_hubs import LatentGroup


def write_local_centroids_n_latent_groups(
        b_diver,
        b_set,
        outer_take_one,
        group_name,
        experiment_path,
        individuals_relative,
        valid_positions,
        hub_top_k=5
):
    # leave_twice_local_clusters = []

    leave_one_local_centroids = []
    leave_one_local_clusters = []

    inner_b_set = get_leave_ij_set(b_set, outer_take_one, -1)
    inner_b_diver = get_leave_ij_divergence(b_diver, outer_take_one, -1)

    group_mst, group_hubs = load_mst_n_aligned_hubs(group_name, outer_take_one, experiment_path)

    # for outer_i in tqdm(range(len(b_set))):
    #     leave_one_local_centroids = []
    #     leave_one_local_clusters = []
    #     for inner_j in range(len(b_set) - 1):

    # Attention: b_set should be inner b_set
    # print(len(inner_b_set))
    for outer_i in tqdm(range(len(inner_b_set) + 1)):  # last element is without leave one
        converted_outer_i = outer_i
        inner_j = -1
        if outer_i == len(inner_b_set):
            converted_outer_i = -1
        group_latent_group = LatentGroup(
            inner_b_diver,
            group_mst,
            group_hubs,
            group_name,
            inner_b_set,
            converted_outer_i,
            inner_j,
            experiment_path,
            individuals_relative,
            valid_positions,
            hub_top_k
        )
        local_centroids, local_clusters = \
            group_latent_group.f_local_centroids_n_latent_groups_by_div()
        leave_one_local_centroids.append(local_centroids)
        leave_one_local_clusters.append(local_clusters)

    #     leave_twice_local_clusters.append(leave_one_local_clusters)
    #     with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
    #               f"_{outer_i}_local_centroids.txt", "wb") as fp:
    #         pickle.dump(leave_one_local_centroids, fp)
    #
    # with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
    #           f"_latent_groups.txt", "wb") as fp:
    #     pickle.dump(leave_twice_local_clusters, fp)
    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              f"outer_{outer_take_one}_by_div_leave_one_local_centroids.txt", "wb") as fp:
        pickle.dump(leave_one_local_centroids, fp)

    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              f"outer_{outer_take_one}_by_div_leave_one_latent_groups.txt", "wb") as fp:
        pickle.dump(leave_one_local_clusters, fp)


# TODO
def write_leave_one_local_centroids_n_latent_groups(
        b_set,
        group_name,
        experiment_path,
        individuals_relative,
        valid_positions,
        hub_top_k=5
):

    b_diver = load_divergence(group_name, experiment_path)
    group_mst, group_hubs = load_leave_one_mst_n_aligned_hubs(group_name, experiment_path)
    leave_one_local_centroids = []
    leave_one_local_clusters = []
    for outer_i in tqdm(range(len(b_set) + 1)):  # last element is without leave one
        converted_outer_i = outer_i
        inner_j = -1
        if outer_i == len(b_set):
            converted_outer_i = -1
        group_latent_group = LatentGroup(
            b_diver,
            group_mst,
            group_hubs,
            group_name,
            b_set,
            converted_outer_i,
            inner_j,
            experiment_path,
            individuals_relative,
            valid_positions,
            hub_top_k
        )
        local_centroids, local_clusters = \
            group_latent_group.f_local_centroids_n_latent_groups()
        leave_one_local_centroids.append(local_centroids)
        leave_one_local_clusters.append(local_clusters)

    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              f"_leave_one_local_centroids.txt", "wb") as fp:
        pickle.dump(leave_one_local_centroids, fp)

    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              f"_leave_one_latent_groups.txt", "wb") as fp:
        pickle.dump(leave_one_local_clusters, fp)


def write_global_centroid(
        b_set,
        group_name,
        experiment_path,
        individuals_relative,
        valid_positions):

    b_diver = load_divergence(group_name, experiment_path)
    group_mst, group_hubs = load_leave_one_mst_n_hubs(group_name, experiment_path)

    leave_one_local_centroids = []
    leave_one_local_clusters = []
    for outer_i in tqdm(range(len(b_set) + 1)):  # last element is without leave one
        converted_outer_i = outer_i
        inner_j = -1
        if outer_i == len(b_set):
            converted_outer_i = -1
        group_latent_group = LatentGroup(
            b_diver,
            group_mst,
            group_hubs,
            group_name,
            b_set,
            converted_outer_i,
            inner_j,
            experiment_path,
            individuals_relative,
            valid_positions,
        )

        if outer_i == len(b_set):
            cluster_set = np.arange(len(b_set))
            local_clusters = [set(cluster_set)]
        else:
            cluster_set = np.arange(len(b_set) - 1)
            local_clusters = [set(cluster_set)]

        local_centroids = group_latent_group.f_centroid(local_clusters)

        leave_one_local_centroids.append(local_centroids)
        leave_one_local_clusters.append(local_clusters)

    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              f"_global_centroid_leave_one.txt", "wb") as fp:
        pickle.dump(leave_one_local_centroids, fp)

    with open(path_prefix + experiment_path + kl_matrix_dict[group_name] +
              f"_global_cluster.txt", "wb") as fp:
        pickle.dump(leave_one_local_clusters, fp)
