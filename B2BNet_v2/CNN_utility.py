import keras
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten, Dropout
from keras import regularizers
from sklearn.model_selection import train_test_split


# input: probability matrix

def create_model():
    # create model
    model = Sequential()
    # add model layers
    model.add(Conv2D(8 * 8 * 10, kernel_size=(3, 3), strides=(1, 1), activation='relu',
                     kernel_regularizer=regularizers.l2(0.001),
                     input_shape=(10, 10, 1)))
    model.add(Dropout(0.5))
    model.add(Conv2D(6 * 6 * 10, kernel_size=(3, 3), strides=(1, 1), activation='relu',
                     kernel_regularizer=regularizers.l2(0.001)))
    model.add(Dropout(0.5))
    model.add(Conv2D(2 * 2 * 20, kernel_size=(3, 3), strides=(3, 3), activation='relu',
                     kernel_regularizer=regularizers.l2(0.001)))
    #model.add(Dropout(0.5))
    model.add(Flatten())
    model.add(Dense(10, activation='softmax'))
    # keras.optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=1e-8)
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    print(model.summary())
    return model


def cnn_test_flow(X , y, cluster_label_list):
    x_train, x_test, y_train, y_test = \
        train_test_split(X, y, test_size=0.2, stratify=cluster_label_list)
    # reshape data to fit model
    x_train = x_train.reshape(*np.shape(x_train), 1)
    x_test = x_test.reshape(*np.shape(x_test), 1)

    keras.backend.clear_session()
    model_for_training = create_model()
    model_for_training.fit(x_train, y_train, validation_data=(x_test, y_test), epochs=5)
    return model_for_training
