import copy
import numpy as np
import pandas as pd
import keras
from keras.utils import to_categorical
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
from tqdm import tqdm

from .debug_orbitual_param import get_pg_fglb_2dim, get_params_by_fitting
from .cluster_scoring_utility import *

from .load_group_data import \
    load_data, load_representation, \
    resolve_valid_positions, representation2representation
from .latent_group_utility import MultiCentroid
from .read_write_utility import \
    load_leave_one_mst_n_hubs, \
    load_leave_one_local_centroids_n_latent_groups, \
    get_leave_ij_set, \
    load_global_centroid_n_cluster, \
    load_leave_one_mst_n_aligned_hubs


'''
Four scenarios:

outer leave one out : in adhd
inner leave one out : in adhd

outer leave one out : in adhd
inner leave one out : in typical

outer leave one out : in typical
inner leave one out : in adhd


outer leave one out : in typical
inner leave one out : in typical
'''


class LeaveOne:
    def __init__(self, experiment_path):
        # experiment_path = 'Samsung_T5/Experiment2019/K-fold_NYU/'
        self.experiment_path = experiment_path
        self.data_dict = load_data()
        self.a1a3_group = self.data_dict['a1a3_group']
        self.typical_group = self.data_dict['typical_group']
        self.ta1a3_group = self.data_dict['ta1a3_group']
        self.adhd3_end = self.data_dict['adhd3_end']
        self.length_universal_set = len(self.ta1a3_group)

        representation_dict = load_representation(self.data_dict)
        input_representations = representation_dict['input_representations']
        self.individuals_relative = representation_dict['individuals_relative']
        valid_position_dict = resolve_valid_positions(input_representations, self.ta1a3_group)
        self.valid_positions = valid_position_dict['valid_positions']

        self.leave_one_dict = {}
        self.leave_none = {}
        self.leave_one = {}

    def load_leave_one(self):
        leave_one_typical_mst, leave_one_typical_hubs = \
            load_leave_one_mst_n_aligned_hubs('Typical', self.experiment_path)

        leave_one_a1a3_mst, leave_one_a1a3_hubs = \
            load_leave_one_mst_n_aligned_hubs('ADHD', self.experiment_path)

        leave_one_typical_local_centroids, leave_one_typical_latent_groups = \
            load_leave_one_local_centroids_n_latent_groups('Typical', self.experiment_path)

        leave_one_a1a3_local_centroids, leave_one_a1a3_latent_groups = \
            load_leave_one_local_centroids_n_latent_groups('ADHD', self.experiment_path)

        # global_typical_centroid, global_typical_group = load_global_centroid_n_cluster(
        #     'Typical', self.experiment_path)
        # global_a1a3_centroid, global_a1a3_group = load_global_centroid_n_cluster(
        #     'ADHD', self.experiment_path)

        self.leave_one_dict = {
            'typical_mst': leave_one_typical_mst,
            'typical_hubs': leave_one_typical_hubs,
            'a1a3_mst': leave_one_a1a3_mst,
            'a1a3_hubs': leave_one_a1a3_hubs,
            'typical_local_centroids': leave_one_typical_local_centroids,
            'typical_latent_groups': leave_one_typical_latent_groups,
            'a1a3_local_centroids': leave_one_a1a3_local_centroids,
            'a1a3_latent_groups': leave_one_a1a3_latent_groups
            # 'typical_global_centroid': global_typical_centroid,
            # 'a1a3_global_centroid': global_a1a3_centroid,
            # 'typical_global_group': global_typical_group,
            # 'a1a3_global_group': global_a1a3_group
        }

    def set_leave_none(self):
        self.leave_none = {
            'typical_mst':
                self.leave_one_dict['typical_mst'][-1],
            'typical_hubs':
                self.leave_one_dict['typical_hubs'][-1],
            'a1a3_mst':
                self.leave_one_dict['a1a3_mst'][-1],
            'a1a3_hubs':
                self.leave_one_dict['a1a3_hubs'][-1],
            'typical_local_centroids':
                self.leave_one_dict['typical_local_centroids'][-1],
            'typical_latent_groups':
                self.leave_one_dict['typical_latent_groups'][-1],
            'a1a3_local_centroids':
                self.leave_one_dict['a1a3_local_centroids'][-1],
            'a1a3_latent_groups':
                self.leave_one_dict['a1a3_latent_groups'][-1]
            # 'typical_global_centroid':
            #     self.leave_one_dict['typical_global_centroid'][-1],
            # 'a1a3_global_centroid':
            #     self.leave_one_dict['a1a3_global_centroid'][-1],
            # 'typical_global_group':
            #     self.leave_one_dict['typical_global_group'][-1],
            # 'a1a3_global_group':
            #     self.leave_one_dict['a1a3_global_group'][-1]
        }

    def set_fixed_latent_groups(self):
        fixed_a1a3_set = self.a1a3_group
        fixed_a1a3_clusters = self.leave_none['a1a3_latent_groups']
        fixed_a1a3_centroids = self.leave_none['a1a3_local_centroids']

        fixed_typical_set = self.typical_group
        fixed_typical_clusters = self.leave_none['typical_latent_groups']
        fixed_typical_centroids = self.leave_none['typical_local_centroids']

        return fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
               fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids,

    def set_fixed_global_group(self):
        fixed_a1a3_set = self.a1a3_group
        fixed_a1a3_clusters = self.leave_none['a1a3_global_group']
        fixed_a1a3_centroids = self.leave_none['a1a3_global_centroid']

        fixed_typical_set = self.typical_group
        fixed_typical_clusters = self.leave_none['typical_global_group']
        fixed_typical_centroids = self.leave_none['typical_global_centroid']

        return fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
               fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids

    def take_one_test(self, is_global=False,
                       is_log='log', is_gauss='gauss',
                       remove_diag=False, is_testing=False):

        list_X = []
        list_y = []
        list_cluster_label_list = []
        list_X_test = []

        self.load_leave_one()
        self.set_leave_none()

        if is_global:  # TODO
            fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
            fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids = \
                self.set_fixed_global_group()
        else:
            fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
            fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids = \
                self.set_fixed_latent_groups()
        # print(f"fixed_a1a3_hubs:{self.leave_one_dict['a1a3_hubs'][-1]}")
        # print(f"fixed_a1a3_clusters : {fixed_a1a3_clusters}")
        # print(f"fixed_typical_hubs:{self.leave_one_dict['typical_hubs'][-1]}")
        # print(f"fixed_typical_clusters: {fixed_typical_clusters}")

        fixed_a1a3_latent_multi = MultiCentroid(
            fixed_a1a3_set,
            fixed_a1a3_clusters,
            self.individuals_relative, self.valid_positions,
            is_log, is_gauss
        )

        fixed_typical_latent_multi = MultiCentroid(
            fixed_typical_set,
            fixed_typical_clusters,
            self.individuals_relative, self.valid_positions,
            is_log, is_gauss
        )

        for take_one in tqdm(range(len(self.ta1a3_group))):
            # print(f"take_one:{take_one}")
            adapted_typical_latent_multi = copy.deepcopy(fixed_typical_latent_multi)
            # adapted_typical_clusters = copy.deepcopy(fixed_typical_clusters)
            # adapted_typical_centroids = copy.deepcopy(fixed_typical_centroids)
            adapted_typical_set = fixed_typical_set.copy()
            adapted_typical_clusters, adapted_typical_centroids = \
                fixed_typical_clusters.copy(), fixed_typical_centroids.copy()

            adapted_a1a3_latent_multi = copy.deepcopy(fixed_a1a3_latent_multi)
            # adapted_a1a3_clusters = copy.deepcopy(fixed_a1a3_clusters)
            # adapted_a1a3_centroids = copy.deepcopy(fixed_a1a3_centroids)
            adapted_a1a3_set = fixed_a1a3_set.copy()
            adapted_a1a3_clusters, adapted_a1a3_centroids = \
                fixed_a1a3_clusters.copy(), fixed_a1a3_centroids.copy()

            if take_one < self.adhd3_end:
                adapted_a1a3_set = get_leave_ij_set(fixed_a1a3_set, take_one, -1)
                if is_global:
                    adapted_a1a3_clusters = \
                        self.leave_one_dict['a1a3_global_group'][take_one]
                    adapted_a1a3_centroids = \
                        self.leave_one_dict['a1a3_global_centroid'][take_one]
                else:
                    adapted_a1a3_clusters = \
                        self.leave_one_dict['a1a3_latent_groups'][take_one]
                    adapted_a1a3_centroids = \
                        self.leave_one_dict['a1a3_local_centroids'][take_one]
                adapted_a1a3_latent_multi = MultiCentroid(
                    adapted_a1a3_set,
                    adapted_a1a3_clusters,
                    self.individuals_relative, self.valid_positions,
                    is_log, is_gauss
                )

                # hubs = np.array(self.leave_one_dict['a1a3_hubs'][take_one])
                # a1a3_mst = self.leave_one_dict['a1a3_mst'][take_one]
                # print([a1a3_mst.degree[hub_idx] for hub_idx in hubs])
                #
                # adjust_idx = np.where(hubs >= take_one)
                # hubs[adjust_idx] = hubs[adjust_idx] + 1
                # print(f"hubs:{hubs}")
                #
                # a1a3_clusters = []
                # for cluster in adapted_a1a3_clusters:
                #     this_cluster = np.array(list(cluster))
                #     adjust_idx = np.where(this_cluster >= take_one)
                #     this_cluster[adjust_idx] = this_cluster[adjust_idx] + 1
                #     a1a3_clusters.append(this_cluster)
                # print(f"a1a3 clusters:{a1a3_clusters}")

            elif take_one >= self.adhd3_end:
                typical_take_one = take_one - self.adhd3_end
                adapted_typical_set = get_leave_ij_set(fixed_typical_set, typical_take_one, -1)
                if is_global:
                    adapted_typical_clusters = \
                        self.leave_one_dict['typical_global_group'][typical_take_one]
                    adapted_typical_centroids = \
                        self.leave_one_dict['typical_global_centroid'][typical_take_one]
                else:
                    adapted_typical_clusters = \
                        self.leave_one_dict['typical_latent_groups'][typical_take_one]
                    adapted_typical_centroids = \
                        self.leave_one_dict['typical_local_centroids'][typical_take_one]
                adapted_typical_latent_multi = MultiCentroid(
                    adapted_typical_set,
                    adapted_typical_clusters,
                    self.individuals_relative, self.valid_positions,
                    is_log, is_gauss
                )

                # hubs = np.array(self.leave_one_dict['typical_hubs'][typical_take_one])
                # typical_mst = self.leave_one_dict['typical_mst'][typical_take_one]
                # print([typical_mst.degree[hub_idx] for hub_idx in hubs])
                #
                # adjust_idx = np.where(hubs >= typical_take_one)
                # hubs[adjust_idx] = hubs[adjust_idx] + 1
                # print(f"hubs:{hubs}")
                #
                # typical_clusters = []
                # # print(adapted_typical_clusters)
                # for cluster in adapted_typical_clusters:
                #     this_cluster = np.array(list(cluster))
                #     adjust_idx = np.where(this_cluster >= typical_take_one)
                #     this_cluster[adjust_idx] = this_cluster[adjust_idx] + 1
                #     typical_clusters.append(this_cluster)
                # print(f"typical clusters:{typical_clusters}")

            #######################################
            X, y_cluster, cluster_label_list, X_test = self.x_set_framework_for_cnn(
                np.array([self.ta1a3_group[take_one]]),
                adapted_a1a3_set,
                adapted_typical_set,
                adapted_a1a3_centroids,
                adapted_typical_centroids,
                adapted_a1a3_clusters,
                adapted_typical_clusters,
                adapted_a1a3_latent_multi,
                adapted_typical_latent_multi,
                is_log, remove_diag
            )
            list_X.append(X)
            list_y.append(y_cluster)
            list_cluster_label_list.append(cluster_label_list)
            list_X_test.append(X_test)
        ######################################
        list_X = np.array(list_X)
        list_y = np.array(list_y)
        list_cluster_label_list = np.array(list_cluster_label_list)
        list_X_test = np.array(list_X_test)

        shape_list_X = np.shape(list_X)
        shape_list_y = np.shape(list_y)
        shape_list_cluster_label = np.shape(list_cluster_label_list)
        shape_list_X_test = np.shape(list_X_test)

        X = list_X.reshape(shape_list_X[0] * shape_list_X[1], shape_list_X[2], shape_list_X[3])
        y = list_y.reshape(shape_list_y[0] * shape_list_y[1], shape_list_y[2])
        cluster_label = list_cluster_label_list.reshape(shape_list_cluster_label[0] * shape_list_cluster_label[1])
        X_test = list_X_test.reshape(shape_list_X_test[0] * shape_list_X_test[1], shape_list_X_test[2], shape_list_X_test[3])

        return X, y, cluster_label, X_test

    def leave_none_test(self, is_log='log', is_gauss='gauss',
                        remove_diag=False, is_testing=False):
        self.load_leave_one()
        self.set_leave_none()

        fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
        fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids = \
            self.set_fixed_latent_groups()

        fixed_a1a3_latent_multi = MultiCentroid(
            fixed_a1a3_set,
            fixed_a1a3_clusters,
            self.individuals_relative, self.valid_positions,
            is_log, is_gauss
        )

        fixed_typical_latent_multi = MultiCentroid(
            fixed_typical_set,
            fixed_typical_clusters,
            self.individuals_relative, self.valid_positions,
            is_log, is_gauss
        )
        print(f"a1a3_hubs:{self.leave_none['a1a3_hubs']} \n"
              f"typical_hubs:{self.leave_none['typical_hubs']}")


        # print("hihi")
        # print(type(fixed_a1a3_centroids))
        # TODO
        diver_x_set_g1cent, \
        diver_x_set_g2cent, \
        a1a3_g1cent_params, \
        a1a3_g2cent_params, \
        typical_g1cent_params, \
        typical_g2cent_params = \
            get_params_by_fitting(
                fixed_a1a3_latent_multi,
                fixed_typical_latent_multi,
                fixed_a1a3_centroids, fixed_typical_centroids,
                fixed_a1a3_clusters,
                fixed_typical_clusters
            )

        a1a3_to_g1cent_pg_n_fgl_b_matrix, a1a3_to_g2cent_pg_n_fgl_b_matrix, \
        typical_to_g1cent_pg_n_fgl_b_matrix, typical_to_g2cent_pg_n_fgl_b_matrix = \
            get_pg_fglb_2dim(
                diver_x_set_g1cent,
                diver_x_set_g2cent,
                a1a3_g1cent_params,
                a1a3_g2cent_params,
                typical_g1cent_params,
                typical_g2cent_params,
                fixed_a1a3_centroids,
                fixed_typical_centroids,
                fixed_a1a3_clusters,
                fixed_typical_clusters,
                self.length_universal_set,  # TODO
                fixed_a1a3_latent_multi, fixed_typical_latent_multi,
                is_log, remove_diag,
                is_testing,
                x_set=self.ta1a3_group
            )

        return a1a3_to_g1cent_pg_n_fgl_b_matrix, a1a3_to_g2cent_pg_n_fgl_b_matrix, \
               typical_to_g1cent_pg_n_fgl_b_matrix, typical_to_g2cent_pg_n_fgl_b_matrix, \
               fixed_a1a3_latent_multi.clusters, fixed_typical_latent_multi.clusters, \
               a1a3_g1cent_params, a1a3_g2cent_params, typical_g1cent_params, typical_g2cent_params, \
               diver_x_set_g1cent, \
               diver_x_set_g2cent

    ###########################################

    def x_set_framework(self,
                        x_set,
                        adapted_a1a3_set,
                        adapted_typical_set,
                        adapted_a1a3_centroids,
                        adapted_typical_centroids,
                        adapted_a1a3_clusters,
                        adapted_typical_clusters,
                        adapted_a1a3_latent_multi,
                        adapted_typical_latent_multi,
                        is_log, remove_diag
                        ):

        # fit (training) -> params
        # pdf (params, training) -> score reg (training)
        # pdf (prams, testing) -> predict_reg
        len_adapted_a1a3_set = len(adapted_a1a3_set)
        len_adapted_typical_set = len(adapted_typical_set)

        diver_x_set_g1cent, \
        diver_x_set_g2cent, \
        a1a3_g1cent_params, \
        a1a3_g2cent_params, \
        typical_g1cent_params, \
        typical_g2cent_params = get_params_by_fitting(
            adapted_a1a3_latent_multi,
            adapted_typical_latent_multi,
            adapted_a1a3_centroids, adapted_typical_centroids,
            adapted_a1a3_clusters,
            adapted_typical_clusters
        )

        inputs_prob_matrices = (diver_x_set_g1cent,
                                diver_x_set_g2cent,
                                a1a3_g1cent_params,
                                a1a3_g2cent_params,
                                typical_g1cent_params,
                                typical_g2cent_params,
                                adapted_a1a3_centroids,
                                adapted_typical_centroids,
                                adapted_a1a3_clusters,
                                adapted_typical_clusters,
                                self.length_universal_set,
                                adapted_a1a3_latent_multi, adapted_typical_latent_multi,
                                is_log, remove_diag)

        training_pg_n_fgl_b_matrix = \
            get_pg_fglb_2dim(
                *inputs_prob_matrices,
                is_testing=False
            )

        len_adapted_a1a3_clusters = len(adapted_a1a3_clusters)
        len_adapted_typical_clusters = len(adapted_typical_clusters)

        matrix_for_scoring, sample_weight = generate_prob_matrix_with_cluster_labels(
            *training_pg_n_fgl_b_matrix,
            adapted_a1a3_clusters, adapted_typical_clusters,
            len_adapted_a1a3_clusters, len_adapted_typical_clusters,
            len_a1a3_group=len_adapted_a1a3_set,
            is_pg=False
        )

        reg_list, reg_predict_list = cluster_scoring_training(matrix_for_scoring, sample_weight)

        training_as_testing_pg_n_fgl_b_matrix = \
            get_pg_fglb_2dim(
                *inputs_prob_matrices,
                is_testing=True,
                x_set=np.concatenate([adapted_a1a3_set, adapted_typical_set])
            )

        training_as_testing_matrix_for_scoring, _ = generate_prob_matrix_with_cluster_labels(
            *training_as_testing_pg_n_fgl_b_matrix,
            adapted_a1a3_clusters, adapted_typical_clusters,
            len_adapted_a1a3_clusters, len_adapted_typical_clusters,
            len_a1a3_group=len_adapted_a1a3_set,
            is_pg=False
        )

        training_as_testing_reg_predict_list = cluster_scoring_testing(training_as_testing_matrix_for_scoring, reg_list)
        training_as_testing_predicted_score = np.array(training_as_testing_reg_predict_list).T
        training_as_testing_input_score_in_a1a3 = training_as_testing_predicted_score[:, :len_adapted_a1a3_clusters]
        training_as_testing_input_score_in_typical = training_as_testing_predicted_score[:, len_adapted_a1a3_clusters:]

        training_as_testing_result = regression(np.array(training_as_testing_input_score_in_a1a3),
                                     np.array(training_as_testing_input_score_in_typical),
                                     len_a1a3_group=len_adapted_a1a3_set,
                                     len_typical_group=len_adapted_typical_set,
                                     train_type="none")

        testing_pg_n_fgl_b_matrix = get_pg_fglb_2dim(
            *inputs_prob_matrices,
            is_testing=True,
            x_set=x_set
        )

        test_matrix_for_scoring, _ = generate_prob_matrix_with_cluster_labels(
            *testing_pg_n_fgl_b_matrix,
            adapted_a1a3_clusters, adapted_typical_clusters,
            len_adapted_a1a3_clusters, len_adapted_typical_clusters,
            len_a1a3_group=len_adapted_a1a3_set,
            is_pg=False
        )

        test_reg_predict_list = cluster_scoring_testing(test_matrix_for_scoring, reg_list)
        test_predicted_score = np.array(test_reg_predict_list).T

        test_input_score_in_a1a3 = test_predicted_score[:, :len_adapted_a1a3_clusters]
        test_input_score_in_typical = test_predicted_score[:, len_adapted_a1a3_clusters:]

        test_result = regression(np.array(test_input_score_in_a1a3), np.array(test_input_score_in_typical),
                             len_a1a3_group=len_adapted_a1a3_set, len_typical_group=len_adapted_typical_set,
                             train_type="none", is_training=False, trained_clf=training_as_testing_result['trained_clf'])
        #TODO
        return training_as_testing_result, test_result

    def x_set_framework_two_labels(self,
                        x_set,
                        adapted_a1a3_set,
                        adapted_typical_set,
                        adapted_a1a3_centroids,
                        adapted_typical_centroids,
                        adapted_a1a3_clusters,
                        adapted_typical_clusters,
                        adapted_a1a3_latent_multi,
                        adapted_typical_latent_multi,
                        is_log, remove_diag
                        ):

        # fit (training) -> params
        # pdf (params, training) -> score reg (training)
        # pdf (prams, testing->take_one) -> predict_reg
        len_adapted_a1a3_set = len(adapted_a1a3_set)
        len_adapted_typical_set = len(adapted_typical_set)

        diver_x_set_g1cent, \
        diver_x_set_g2cent, \
        a1a3_g1cent_params, \
        a1a3_g2cent_params, \
        typical_g1cent_params, \
        typical_g2cent_params = get_params_by_fitting(
            adapted_a1a3_latent_multi,
            adapted_typical_latent_multi,
            adapted_a1a3_centroids, adapted_typical_centroids,
            adapted_a1a3_clusters,
            adapted_typical_clusters
        )
        # print(f"a1a3_clusters_to_a1a3_groups: \n {a1a3_g1cent_params} \n"
        #       f"a1a3_clusters_to_typical_groups: \n {a1a3_g2cent_params} \n"
        #       f"typical_clusters_to_a1a3_groups: \n {typical_g1cent_params}\n"
        #       f"typical_clusters_to_typical_groups: \n {typical_g2cent_params}")

        inputs_prob_matrices = (diver_x_set_g1cent,
                                diver_x_set_g2cent,
                                a1a3_g1cent_params,
                                a1a3_g2cent_params,
                                typical_g1cent_params,
                                typical_g2cent_params,
                                adapted_a1a3_centroids,
                                adapted_typical_centroids,
                                adapted_a1a3_clusters,
                                adapted_typical_clusters,
                                self.length_universal_set,
                                adapted_a1a3_latent_multi, adapted_typical_latent_multi,
                                is_log, remove_diag)

        training_pg_n_fgl_b_matrix = \
            get_pg_fglb_2dim(
                *inputs_prob_matrices,
                is_testing=False
            )

        len_adapted_a1a3_clusters = len(adapted_a1a3_clusters)
        len_adapted_typical_clusters = len(adapted_typical_clusters)

        matrix_for_scoring, sample_weight = generate_prob_matrix_with_cluster_labels(
            *training_pg_n_fgl_b_matrix,
            adapted_a1a3_clusters, adapted_typical_clusters,
            len_adapted_a1a3_clusters, len_adapted_typical_clusters,
            len_a1a3_group=len_adapted_a1a3_set,
            is_pg=False
        )

        cate_reg_list, cate_reg_predict_list, \
            cluster_reg_list, cluster_reg_predict_list = \
            cluster_scoring_with_two_labels(matrix_for_scoring, sample_weight)

        cate_predicted_score = np.array(cate_reg_predict_list).T
        cluster_predicted_score = np.array(cluster_reg_predict_list).T
        cate_input_score_in_a1a3 = cate_predicted_score[:, :len_adapted_a1a3_clusters]
        cate_input_score_in_typical = cate_predicted_score[:, len_adapted_a1a3_clusters:]
        cluster_input_score_in_a1a3 = cluster_predicted_score[:, :len_adapted_a1a3_clusters]
        cluster_input_score_in_typical = cluster_predicted_score[:, len_adapted_a1a3_clusters:]
        input_score_in_a1a3 = np.concatenate([cate_input_score_in_a1a3, cluster_input_score_in_a1a3], axis=1)
        input_score_in_typical = np.concatenate([cate_input_score_in_typical, cluster_input_score_in_typical], axis=1)

        training_result = regression(np.array(input_score_in_typical), np.array(input_score_in_a1a3),
                            len_a1a3_group=len_adapted_a1a3_set, len_typical_group=len_adapted_typical_set,
                            train_type="none")

        ##############################

        testing_pg_n_fgl_b_matrix = get_pg_fglb_2dim(
            *inputs_prob_matrices,
            is_testing=True,
            x_set=x_set
        )

        test_matrix_for_scoring, _ = generate_prob_matrix_with_cluster_labels(
            *testing_pg_n_fgl_b_matrix,
            adapted_a1a3_clusters, adapted_typical_clusters,
            len_adapted_a1a3_clusters, len_adapted_typical_clusters,
            len_a1a3_group=len_adapted_a1a3_set,
            is_pg=False
        )

        test_cate_reg_predict_list, test_cluster_reg_predict_list = \
            cluster_scoring_with_two_labels(test_matrix_for_scoring, sample_weight,
                                            is_training=False,
                                            trained_cate_reg_list=cate_reg_list,
                                            trained_cluster_reg_list=cluster_reg_list)
        test_cate_predicted_score = np.array(test_cate_reg_predict_list).T
        test_cluster_predicted_score = np.array(test_cluster_reg_predict_list).T

        test_cate_input_score_in_a1a3 = test_cate_predicted_score[:, :len_adapted_a1a3_clusters]
        test_cate_input_score_in_typical = test_cate_predicted_score[:, len_adapted_a1a3_clusters:]
        test_cluster_input_score_in_a1a3 = test_cluster_predicted_score[:, :len_adapted_a1a3_clusters]
        test_cluster_input_score_in_typical = test_cluster_predicted_score[:, len_adapted_a1a3_clusters:]

        test_input_score_in_a1a3 = np.concatenate([test_cate_input_score_in_a1a3, test_cluster_input_score_in_a1a3], axis=1)
        test_input_score_in_typical = np.concatenate([test_cate_input_score_in_typical,
                                                      test_cluster_input_score_in_typical],
                                                     axis=1)

        test_result = regression(np.array(test_input_score_in_a1a3), np.array(test_input_score_in_typical),
                                 len_a1a3_group=len_adapted_a1a3_set, len_typical_group=len_adapted_typical_set,
                                 train_type="none", is_training=False, trained_clf=training_result['trained_clf'])

        return training_result, test_result

    def x_set_framework_for_cnn(self,
                        x_set,
                        adapted_a1a3_set,
                        adapted_typical_set,
                        adapted_a1a3_centroids,
                        adapted_typical_centroids,
                        adapted_a1a3_clusters,
                        adapted_typical_clusters,
                        adapted_a1a3_latent_multi,
                        adapted_typical_latent_multi,
                        is_log, remove_diag
                        ):
        # fit (training) -> params
        # pdf (params, training) ->
        len_adapted_a1a3_set = len(adapted_a1a3_set)
        len_adapted_typical_set = len(adapted_typical_set)

        diver_x_set_g1cent, \
        diver_x_set_g2cent, \
        a1a3_g1cent_params, \
        a1a3_g2cent_params, \
        typical_g1cent_params, \
        typical_g2cent_params = get_params_by_fitting(
            adapted_a1a3_latent_multi,
            adapted_typical_latent_multi,
            adapted_a1a3_centroids, adapted_typical_centroids,
            adapted_a1a3_clusters,
            adapted_typical_clusters
        )
        # print(f"a1a3_clusters_to_a1a3_groups: \n {a1a3_g1cent_params} \n"
        #       f"a1a3_clusters_to_typical_groups: \n {a1a3_g2cent_params} \n"
        #       f"typical_clusters_to_a1a3_groups: \n {typical_g1cent_params}\n"
        #       f"typical_clusters_to_typical_groups: \n {typical_g2cent_params}")

        inputs_prob_matrices = (diver_x_set_g1cent,
                                diver_x_set_g2cent,
                                a1a3_g1cent_params,
                                a1a3_g2cent_params,
                                typical_g1cent_params,
                                typical_g2cent_params,
                                adapted_a1a3_centroids,
                                adapted_typical_centroids,
                                adapted_a1a3_clusters,
                                adapted_typical_clusters,
                                self.length_universal_set,
                                adapted_a1a3_latent_multi, adapted_typical_latent_multi,
                                is_log, remove_diag)

        training_pg_n_fgl_b_matrix = \
            get_pg_fglb_2dim(
                *inputs_prob_matrices,
                is_testing=False
            )
        len_adapted_a1a3_clusters = len(adapted_a1a3_clusters)
        len_adapted_typical_clusters = len(adapted_typical_clusters)

        matrix_for_scoring, sample_weight = generate_prob_matrix_with_cluster_labels(
            *training_pg_n_fgl_b_matrix,
            adapted_a1a3_clusters, adapted_typical_clusters,
            len_adapted_a1a3_clusters, len_adapted_typical_clusters,
            len_a1a3_group=len_adapted_a1a3_set,
            is_pg=False
        )

        cluster_label_list = np.zeros(len_adapted_a1a3_set + len_adapted_typical_set)
        cluster_idx = 0
        for cluster in adapted_a1a3_clusters:
            cluster_label_list[list(cluster)] = cluster_idx
            cluster_idx += 1
        for cluster in adapted_typical_clusters:
            cluster_label_list[np.array(list(cluster)) + len_adapted_a1a3_set] = cluster_idx
            cluster_idx += 1

        X = matrix_for_scoring[:, :, :-2]
        y = np.zeros((len_adapted_a1a3_set+len_adapted_typical_set,
                      len_adapted_a1a3_clusters + len_adapted_typical_clusters))
        # one-hot encode target column
        y_cluster = to_categorical(cluster_label_list)

        ###########################################
        testing_pg_n_fgl_b_matrix = get_pg_fglb_2dim(
            *inputs_prob_matrices,
            is_testing=True,
            x_set=x_set
        )

        test_matrix_for_scoring, _ = generate_prob_matrix_with_cluster_labels(
            *testing_pg_n_fgl_b_matrix,
            adapted_a1a3_clusters, adapted_typical_clusters,
            len_adapted_a1a3_clusters, len_adapted_typical_clusters,
            len_a1a3_group=len_adapted_a1a3_set,
            is_pg=False
        )

        X_test = test_matrix_for_scoring[:, :, :-2]

        return X, y_cluster, cluster_label_list, X_test

