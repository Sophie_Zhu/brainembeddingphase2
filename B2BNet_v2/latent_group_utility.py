import numpy as np
import scipy.stats
from .B2BNet_utility import f_divergence


# sef.b_set, self.clusters
class MultiCentroid:
    def __init__(self, b_set, clusters,
                 individuals_relative, valid_positions,
                 is_log='log', is_gauss='gauss'):
        self.b_set = b_set
        self.clusters = clusters

        self.individuals_relative = individuals_relative
        self.valid_positions = valid_positions
        self.is_log = is_log
        self.is_gauss = is_gauss

    def get_diver_b_cent_b2(self, b2_centroids):
        return f_divergence(
            self.individuals_relative[self.b_set],
            b2_centroids,
            self.valid_positions,
            is_same=False,
            is_smooth=False
        )

    def get_diver_b_cent_b2_smooth(self, b2_centroids):
        return f_divergence(
            self.individuals_relative[self.b_set],
            b2_centroids,
            self.valid_positions,
            is_same=False,
        )

    def get_diver_x_set_cent_y(self, x_set, y_centroids):
        return f_divergence(
            self.individuals_relative[x_set],
            y_centroids,
            self.valid_positions,
            is_same=False,
            is_smooth=False
        )

    def get_diver_x_set_cent_y_smooth(self, x_set, y_centroids):
        return f_divergence(
            self.individuals_relative[x_set],
            y_centroids,
            self.valid_positions,
            is_same=False
        )

    def get_diver_b_to_b2_centroids(self, b2_centroids,
                                    b2_centroids_index_as_cluster=None):

        diver_b_cent_b2 = self.get_diver_b_cent_b2_smooth(b2_centroids) # attention!

        estimated_params = self.b_set_to_b2_centroids_moments(diver_b_cent_b2)
        return diver_b_cent_b2, estimated_params

    def x_set_to_b2_centroids(self, x_set, b2_centroids,
                              map_centroid, estimated_params,
                              diver_x_set_cent_b2=None,
                              remove_diag=False,
                              is_testing=True
                              ):
        if diver_x_set_cent_b2 is not None:
            len_x_set = len(diver_x_set_cent_b2)

        if is_testing:
            diver_x_set_cent_b2 = self.get_diver_x_set_cent_y_smooth(x_set, b2_centroids) #!
            len_x_set = len(x_set)
        elif diver_x_set_cent_b2 is None:
            raise Exception('diver_x_set_cent_b2 must not be None in training mode!')

        sum_log_fgl_b = np.zeros((len_x_set, len(self.clusters)))
        fgl_b_matrix = np.zeros((len_x_set, len(self.clusters), len(b2_centroids)))

        for cluster_index in range(len(self.clusters)):
            # log_p_g = np.log(len(self.clusters[cluster_index]) / full_size)

            for subject_index in range(len_x_set):
                if self.is_log == 'log' or self.is_log == 'sum':
                    cluster_fixed_sum_log_fgl_b = 0
                else:
                    cluster_fixed_sum_log_fgl_b = 1
                for centroid_index in range(len(b2_centroids)):
                    x_line = diver_x_set_cent_b2[subject_index, centroid_index]
                    is_diag = False

                    # if m latent groups in this mst leads to n b2_centroids
                    # map_centroid[centroid_index] = latent group match
                    # no match : map_centroid[centroid_index] = np.nan

                    if cluster_index == map_centroid[centroid_index]:
                        is_diag = True

                    if self.is_gauss == 'norm':
                        fgl_b = scipy.stats.norm.pdf(x_line,
                                                     *estimated_params[cluster_index][centroid_index])

                    elif self.is_gauss == 'skewnorm':
                        fgl_b = scipy.stats.skewnorm.pdf(x_line,
                                                         *estimated_params[cluster_index][centroid_index])
                    elif self.is_gauss == 'expon':
                        fgl_b = scipy.stats.expon.pdf(x_line,
                                                      *estimated_params[cluster_index][centroid_index])
                    elif self.is_gauss == 'gaussian_kde':
                        kernel = estimated_params[cluster_index][centroid_index]
                        fgl_b = kernel.pdf(x_line)

                    if remove_diag:
                        if is_diag:
                            if self.is_log == 'log' or self.is_log == 'sum':
                                fgl_b = 0
                            else:
                                fgl_b = 1

                    fgl_b_matrix[subject_index, cluster_index, centroid_index] = fgl_b

                    if self.is_log == 'log':
                        log_fgl_b = \
                            np.log(fgl_b)
                        cluster_fixed_sum_log_fgl_b += log_fgl_b
                    elif self.is_log == 'sum':
                        cluster_fixed_sum_log_fgl_b += fgl_b
                    elif self.is_log == 'multi':
                        cluster_fixed_sum_log_fgl_b *= fgl_b

                sum_log_fgl_b[subject_index][cluster_index] = \
                    cluster_fixed_sum_log_fgl_b

        return sum_log_fgl_b, fgl_b_matrix

    def b_set_to_b2_centroids_moments(self, diver_b_cent_b2):
        if self.is_gauss == 'skewnorm':
            cluster_to_b2_centroids_fitting_params = []
            for cluster in self.clusters:
                # cluster is a set
                diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
                params_list = [scipy.stats.skewnorm.fit(diver_g_b2_centroids[:, centroid_idx])
                     for centroid_idx in range(np.shape(diver_g_b2_centroids)[1])]
                cluster_to_b2_centroids_fitting_params.append(
                    params_list
                )  # fit for each centroid
            return cluster_to_b2_centroids_fitting_params
        elif self.is_gauss == 'norm':
            cluster_to_b2_centroids_fitting_params = []
            for cluster in self.clusters:
                # cluster is a set
                diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
                cluster_to_b2_centroids_fitting_params.append(
                    [scipy.stats.norm.fit(diver_g_b2_centroids[:, centroid_idx])
                     for centroid_idx in range(np.shape(diver_g_b2_centroids)[1])]
                )  # fit for each centroid
            return cluster_to_b2_centroids_fitting_params
        elif self.is_gauss == 'expon':
            cluster_to_b2_centroids_fitting_params = []
            for cluster in self.clusters:
                # cluster is a set
                diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
                cluster_to_b2_centroids_fitting_params.append(
                    [scipy.stats.expon.fit(diver_g_b2_centroids[:, centroid_idx])
                     for centroid_idx in range(np.shape(diver_g_b2_centroids)[1])]
                )
            return cluster_to_b2_centroids_fitting_params
        elif self.is_gauss == 'gaussian_kde':
            cluster_to_b2_centroids_fitting_params = []
            for cluster in self.clusters:
                # cluster is a set
                diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
                cluster_to_b2_centroids_fitting_params.append(
                    [scipy.stats.gaussian_kde(diver_g_b2_centroids[:, centroid_idx])
                     for centroid_idx in range(np.shape(diver_g_b2_centroids)[1])]
                )
            return cluster_to_b2_centroids_fitting_params

