from .read_write_utility import \
    get_leave_ij_divergence, \
    get_leave_ij_set, \
    get_leave_ij_mst_n_hubs
from .B2BNet_utility import f_divergence
from .load_group_data import *


class LatentGroup:
    def __init__(self,
                 group_diver,
                 group_mst,
                 group_hubs,
                 group_name,
                 group_set,
                 outer_i,
                 inner_j,
                 experiment_path,
                 individuals_relative,
                 valid_positions,
                 hub_top_k=5):

        self.experiment_path = experiment_path
        self.outer_i = outer_i
        self.inner_j = inner_j
        self.group_name = group_name
        self.b_set = get_leave_ij_set(group_set, outer_i, inner_j)
        self.individuals_relative = individuals_relative
        self.valid_positions = valid_positions
        self.hub_top_k = hub_top_k

        # saved variables that should be loaded
        self.mst, self.hubs = get_leave_ij_mst_n_hubs(
            group_mst, group_hubs, outer_i, inner_j)
        self.diver_b_set = get_leave_ij_divergence(group_diver, outer_i, inner_j)
        # empty variables
        self.nearest_b_cent_b = []
        self.centroids = []
        self.clusters = []

    # TODO: this version is not working now
    def f_local_centroids_n_latent_groups_by_div(self):
        length_clusters = self.hub_top_k
        initial_clusters = [set() for _ in range(length_clusters)]
        centroids = self.individuals_relative[np.array(self.b_set)[self.hubs]]

        # print(np.shape(self.diver_b_set))
        for idx_in_b in range(len(self.b_set)):
            # print(np.shape(self.individuals_relative[self.b_set[hubs]]))
            diver_indexes_in_b_hubs = np.zeros(len(self.hubs))
            hub_idx = 0
            for hub in self.hubs:
                diver_indexes_in_b_hubs[hub_idx] = \
                    self.diver_b_set[idx_in_b, hub] + self.diver_b_set[hub, idx_in_b] / 2.0
                hub_idx += 1
                # self.get_diver_x_set_cent_y_smooth(  # smooth, attention!
                #     [self.b_set[idx_in_b]],
                #     centroids)
            # if idx_in_b in hubs:
            #     print(diver_indexes_in_b_hubs)
            m_index = np.argmin(diver_indexes_in_b_hubs)
            initial_clusters[int(m_index)].add(idx_in_b)
        # print(f"hubs:{hubs}")
        # print(f"initial clusters:{initial_clusters}")
        self.centroids = centroids
        self.clusters = initial_clusters
        return self.centroids, self.clusters

    def f_local_centroids_n_latent_groups(self):
        length_clusters = self.hub_top_k
        initial_clusters = [set() for _ in range(length_clusters)]
        centroids = self.individuals_relative[np.array(self.b_set)[self.hubs]]
        # print(self.hubs)
        # print(np.array(self.b_set)[self.hubs])
        # print(np.shape(centroids))
        # print("hihihi this !")
        # print(self.hubs)
        # print(np.shape(self.individuals_relative))
        # print(np.shape(centroids ))
        for idx_in_b in range(len(self.b_set)):
            # print(np.shape(self.individuals_relative[self.b_set[hubs]]))
            diver_indexes_in_b_hubs = \
                self.get_diver_x_set_cent_y_smooth(  # smooth, attention!
                    [self.b_set[idx_in_b]],
                    centroids)
            # if idx_in_b in hubs:
            #     print(diver_indexes_in_b_hubs)
            m_index = np.argmin(diver_indexes_in_b_hubs)
            initial_clusters[int(m_index)].add(idx_in_b)
        # print(f"hubs:{hubs}")
        # print(f"initial clusters:{initial_clusters}")
        self.centroids = centroids
        self.clusters = initial_clusters
        return self.centroids, self.clusters

    def get_diver_x_set_cent_y_smooth(self, x_set, y_centroids):
        return f_divergence(
            self.individuals_relative[x_set],
            y_centroids,
            self.valid_positions,
            is_same=False
        )


