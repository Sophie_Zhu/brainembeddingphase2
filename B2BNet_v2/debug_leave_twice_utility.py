import copy
import numpy as np
import pandas as pd
import keras
from keras.utils import to_categorical
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
from tqdm import tqdm

from .debug_orbitual_param import get_pg_fglb_2dim, get_params_by_fitting
from .cluster_scoring_utility import *

from .load_group_data import \
    load_data, load_representation, \
    resolve_valid_positions, representation2representation
from .latent_group_utility import MultiCentroid
from .read_write_utility import \
    load_aligned_hubs, \
    load_leave_one_mst_n_hubs, \
    load_local_centroids_n_latent_groups_by_div, \
    get_leave_ij_set, \
    load_global_centroid_n_cluster, \
    load_leave_one_mst_n_aligned_hubs, \
    load_leave_one_local_centroids_n_latent_groups


class LeaveOne:
    def __init__(self, outer_take_one, experiment_path):
        # experiment_path = 'Samsung_T5/Experiment2019/K-fold_NYU/'
        self.outer_take_one = outer_take_one
        self.experiment_path = experiment_path
        self.data_dict = load_data()

        self.a1a3_group = get_leave_ij_set(self.data_dict['a1a3_group'], outer_take_one, -1)
        self.typical_group = get_leave_ij_set(self.data_dict['typical_group'], outer_take_one, -1)

        # self.ta1a3_group = self.data_dict['ta1a3_group']
        #
        self.adhd3_end = len(self.a1a3_group)
        self.length_universal_set = len(self.data_dict['ta1a3_group']) - 1

        representation_dict = load_representation(self.data_dict)
        input_representations = representation_dict['input_representations']
        self.individuals_relative = representation_dict['individuals_relative']
        valid_position_dict = resolve_valid_positions(input_representations, self.data_dict['ta1a3_group'])
        self.valid_positions = valid_position_dict['valid_positions']

        self.leave_one_dict = {}
        self.leave_none = {}
        self.leave_one = {}

    def load_leave_one(self):
        leave_one_typical_hubs = \
            load_aligned_hubs('Typical', self.outer_take_one, self.experiment_path)

        leave_one_a1a3_hubs = \
            load_aligned_hubs('ADHD', self.outer_take_one, self.experiment_path)

        if self.outer_take_one < len(self.data_dict['a1a3_group']):
            leave_one_typical_local_centroids, leave_one_typical_latent_groups = \
                load_leave_one_local_centroids_n_latent_groups('Typical', self.experiment_path)

            leave_one_a1a3_local_centroids, leave_one_a1a3_latent_groups = \
                load_local_centroids_n_latent_groups_by_div('ADHD', self.outer_take_one, self.experiment_path)
            self.typical_group = self.data_dict['typical_group']
        else:
            leave_one_a1a3_local_centroids, leave_one_a1a3_latent_groups = \
                load_leave_one_local_centroids_n_latent_groups('ADHD', self.experiment_path)
            this_out_take_one = self.outer_take_one - len(self.data_dict['a1a3_group'])
            leave_one_typical_local_centroids, leave_one_typical_latent_groups = \
                load_local_centroids_n_latent_groups_by_div('Typical', this_out_take_one, self.experiment_path)
            self.a1a3_group = self.data_dict['a1a3_group']
            self.adhd3_end = len(self.a1a3_group)

        self.leave_one_dict = {
            'typical_hubs': leave_one_typical_hubs,
            'a1a3_hubs': leave_one_a1a3_hubs,
            'typical_local_centroids': leave_one_typical_local_centroids,
            'typical_latent_groups': leave_one_typical_latent_groups,
            'a1a3_local_centroids': leave_one_a1a3_local_centroids,
            'a1a3_latent_groups': leave_one_a1a3_latent_groups
        }

    def set_leave_none(self):
        self.leave_none = {
            'typical_hubs':
                self.leave_one_dict['typical_hubs'][-1],
            'a1a3_hubs':
                self.leave_one_dict['a1a3_hubs'][-1],
            'typical_local_centroids':
                self.leave_one_dict['typical_local_centroids'][-1],
            'typical_latent_groups':
                self.leave_one_dict['typical_latent_groups'][-1],
            'a1a3_local_centroids':
                self.leave_one_dict['a1a3_local_centroids'][-1],
            'a1a3_latent_groups':
                self.leave_one_dict['a1a3_latent_groups'][-1]
        }

    def set_fixed_latent_groups(self):
        fixed_a1a3_set = self.a1a3_group
        fixed_a1a3_clusters = self.leave_none['a1a3_latent_groups']
        fixed_a1a3_centroids = self.leave_none['a1a3_local_centroids']

        fixed_typical_set = self.typical_group
        fixed_typical_clusters = self.leave_none['typical_latent_groups']
        fixed_typical_centroids = self.leave_none['typical_local_centroids']

        return fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
               fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids,

    def set_fixed_global_group(self):
        fixed_a1a3_set = self.a1a3_group
        fixed_a1a3_clusters = self.leave_none['a1a3_global_group']
        fixed_a1a3_centroids = self.leave_none['a1a3_global_centroid']

        fixed_typical_set = self.typical_group
        fixed_typical_clusters = self.leave_none['typical_global_group']
        fixed_typical_centroids = self.leave_none['typical_global_centroid']

        return fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
               fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids

    def take_one_test(self, is_global=False,
                       is_log='log', is_gauss='gauss',
                       remove_diag=False, is_testing=False):

        list_X = []
        list_y = []
        list_cluster_label_list = []
        list_X_test = []
        list_inputs_prob_matrices = []

        self.load_leave_one()
        self.set_leave_none()

        if is_global:  # TODO
            fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
            fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids = \
                self.set_fixed_global_group()
        else:
            fixed_a1a3_set, fixed_a1a3_clusters, fixed_a1a3_centroids, \
            fixed_typical_set, fixed_typical_clusters, fixed_typical_centroids = \
                self.set_fixed_latent_groups()
        # print(f"fixed_a1a3_hubs:{self.leave_one_dict['a1a3_hubs'][-1]}")
        # print(f"fixed_a1a3_clusters : {fixed_a1a3_clusters}")
        # print(f"fixed_typical_hubs:{self.leave_one_dict['typical_hubs'][-1]}")
        # print(f"fixed_typical_clusters: {fixed_typical_clusters}")

        fixed_a1a3_latent_multi = MultiCentroid(
            fixed_a1a3_set,
            fixed_a1a3_clusters,
            self.individuals_relative, self.valid_positions,
            is_log, is_gauss
        )

        fixed_typical_latent_multi = MultiCentroid(
            fixed_typical_set,
            fixed_typical_clusters,
            self.individuals_relative, self.valid_positions,
            is_log, is_gauss
        )

        for take_one in tqdm(range(self.length_universal_set)):
            # print(f"take_one:{take_one}")
            adapted_typical_latent_multi = copy.deepcopy(fixed_typical_latent_multi)
            # adapted_typical_clusters = copy.deepcopy(fixed_typical_clusters)
            # adapted_typical_centroids = copy.deepcopy(fixed_typical_centroids)
            adapted_typical_set = fixed_typical_set.copy()
            adapted_typical_clusters, adapted_typical_centroids = \
                fixed_typical_clusters.copy(), fixed_typical_centroids.copy()

            adapted_a1a3_latent_multi = copy.deepcopy(fixed_a1a3_latent_multi)
            # adapted_a1a3_clusters = copy.deepcopy(fixed_a1a3_clusters)
            # adapted_a1a3_centroids = copy.deepcopy(fixed_a1a3_centroids)
            adapted_a1a3_set = fixed_a1a3_set.copy()
            adapted_a1a3_clusters, adapted_a1a3_centroids = \
                fixed_a1a3_clusters.copy(), fixed_a1a3_centroids.copy()

            if take_one < self.adhd3_end:
                adapted_a1a3_set = get_leave_ij_set(fixed_a1a3_set, take_one, -1)
                if is_global:
                    adapted_a1a3_clusters = \
                        self.leave_one_dict['a1a3_global_group'][take_one]
                    adapted_a1a3_centroids = \
                        self.leave_one_dict['a1a3_global_centroid'][take_one]
                else:
                    adapted_a1a3_clusters = \
                        self.leave_one_dict['a1a3_latent_groups'][take_one]
                    adapted_a1a3_centroids = \
                        self.leave_one_dict['a1a3_local_centroids'][take_one]
                adapted_a1a3_latent_multi = MultiCentroid(
                    adapted_a1a3_set,
                    adapted_a1a3_clusters,
                    self.individuals_relative, self.valid_positions,
                    is_log, is_gauss
                )

                # hubs = np.array(self.leave_one_dict['a1a3_hubs'][take_one])
                # a1a3_mst = self.leave_one_dict['a1a3_mst'][take_one]
                # print([a1a3_mst.degree[hub_idx] for hub_idx in hubs])
                #
                # adjust_idx = np.where(hubs >= take_one)
                # hubs[adjust_idx] = hubs[adjust_idx] + 1
                # print(f"hubs:{hubs}")
                #
                # a1a3_clusters = []
                # for cluster in adapted_a1a3_clusters:
                #     this_cluster = np.array(list(cluster))
                #     adjust_idx = np.where(this_cluster >= take_one)
                #     this_cluster[adjust_idx] = this_cluster[adjust_idx] + 1
                #     a1a3_clusters.append(this_cluster)
                # print(f"a1a3 clusters:{a1a3_clusters}")

            elif take_one >= self.adhd3_end:
                typical_take_one = take_one - self.adhd3_end
                adapted_typical_set = get_leave_ij_set(fixed_typical_set, typical_take_one, -1)
                if is_global:
                    adapted_typical_clusters = \
                        self.leave_one_dict['typical_global_group'][typical_take_one]
                    adapted_typical_centroids = \
                        self.leave_one_dict['typical_global_centroid'][typical_take_one]
                else:
                    adapted_typical_clusters = \
                        self.leave_one_dict['typical_latent_groups'][typical_take_one]
                    adapted_typical_centroids = \
                        self.leave_one_dict['typical_local_centroids'][typical_take_one]
                adapted_typical_latent_multi = MultiCentroid(
                    adapted_typical_set,
                    adapted_typical_clusters,
                    self.individuals_relative, self.valid_positions,
                    is_log, is_gauss
                )

                # hubs = np.array(self.leave_one_dict['typical_hubs'][typical_take_one])
                # typical_mst = self.leave_one_dict['typical_mst'][typical_take_one]
                # print([typical_mst.degree[hub_idx] for hub_idx in hubs])
                #
                # adjust_idx = np.where(hubs >= typical_take_one)
                # hubs[adjust_idx] = hubs[adjust_idx] + 1
                # print(f"hubs:{hubs}")
                #
                # typical_clusters = []
                # # print(adapted_typical_clusters)
                # for cluster in adapted_typical_clusters:
                #     this_cluster = np.array(list(cluster))
                #     adjust_idx = np.where(this_cluster >= typical_take_one)
                #     this_cluster[adjust_idx] = this_cluster[adjust_idx] + 1
                #     typical_clusters.append(this_cluster)
                # print(f"typical clusters:{typical_clusters}")

            #######################################
            # print(np.shape(adapted_a1a3_set))
            X, y_cluster, cluster_label_list, X_test = \
                self.get_training_samples_and_pdfs_for_cnn(
                    np.array([self.data_dict['ta1a3_group'][self.outer_take_one]]),
                    adapted_a1a3_set,
                    adapted_typical_set,
                    adapted_a1a3_centroids,
                    adapted_typical_centroids,
                    adapted_a1a3_clusters,
                    adapted_typical_clusters,
                    adapted_a1a3_latent_multi,
                    adapted_typical_latent_multi,
                    is_log, remove_diag
                )
            list_X.append(X)
            list_y.append(y_cluster)
            list_cluster_label_list.append(cluster_label_list)
            # list_inputs_prob_matrices.append(inputs_prob_matrices)
            list_X_test.append(X_test)
        ######################################
        list_X = np.array(list_X)
        list_y = np.array(list_y)
        list_cluster_label_list = np.array(list_cluster_label_list)
        # list_inputs_prob_matrices = np.array(list_inputs_prob_matrices)
        list_X_test = np.array(list_X_test)

        shape_list_X = np.shape(list_X)
        shape_list_y = np.shape(list_y)
        shape_list_cluster_label = np.shape(list_cluster_label_list)
        shape_list_X_test = np.shape(list_X_test)

        print(shape_list_X_test)

        X = list_X.reshape(shape_list_X[0] * shape_list_X[1], shape_list_X[2], shape_list_X[3])
        y = list_y.reshape(shape_list_y[0] * shape_list_y[1], shape_list_y[2])
        cluster_label = list_cluster_label_list.reshape(shape_list_cluster_label[0] * shape_list_cluster_label[1])
        X_test = list_X_test.reshape(shape_list_X_test[0] * shape_list_X_test[1], shape_list_X_test[2], shape_list_X_test[3])

        return X, y, cluster_label, X_test
               # list_inputs_prob_matrices

    def get_training_samples_and_pdfs_for_cnn(self,
                        x_set,
                        adapted_a1a3_set,
                        adapted_typical_set,
                        adapted_a1a3_centroids,
                        adapted_typical_centroids,
                        adapted_a1a3_clusters,
                        adapted_typical_clusters,
                        adapted_a1a3_latent_multi,
                        adapted_typical_latent_multi,
                        is_log, remove_diag
                        ):
        # fit (training) -> params
        # pdf (params, training) ->
        len_adapted_a1a3_set = len(adapted_a1a3_set)
        len_adapted_typical_set = len(adapted_typical_set)

        diver_x_set_g1cent, \
        diver_x_set_g2cent, \
        a1a3_g1cent_params, \
        a1a3_g2cent_params, \
        typical_g1cent_params, \
        typical_g2cent_params = get_params_by_fitting(
            adapted_a1a3_latent_multi,
            adapted_typical_latent_multi,
            adapted_a1a3_centroids, adapted_typical_centroids,
            adapted_a1a3_clusters,
            adapted_typical_clusters
        )
        # print(f"a1a3_clusters_to_a1a3_groups: \n {a1a3_g1cent_params} \n"
        #       f"a1a3_clusters_to_typical_groups: \n {a1a3_g2cent_params} \n"
        #       f"typical_clusters_to_a1a3_groups: \n {typical_g1cent_params}\n"
        #       f"typical_clusters_to_typical_groups: \n {typical_g2cent_params}")

        inputs_prob_matrices = (diver_x_set_g1cent,
                                diver_x_set_g2cent,
                                a1a3_g1cent_params,
                                a1a3_g2cent_params,
                                typical_g1cent_params,
                                typical_g2cent_params,
                                adapted_a1a3_centroids,
                                adapted_typical_centroids,
                                adapted_a1a3_clusters,
                                adapted_typical_clusters,
                                self.length_universal_set,
                                adapted_a1a3_latent_multi, adapted_typical_latent_multi,
                                is_log, remove_diag)

        training_pg_n_fgl_b_matrix = \
            get_pg_fglb_2dim(
                *inputs_prob_matrices,
                is_testing=False
            )
        len_adapted_a1a3_clusters = len(adapted_a1a3_clusters)
        len_adapted_typical_clusters = len(adapted_typical_clusters)

        matrix_for_scoring, sample_weight = generate_prob_matrix_with_cluster_labels(
            *training_pg_n_fgl_b_matrix,
            adapted_a1a3_clusters, adapted_typical_clusters,
            len_adapted_a1a3_clusters, len_adapted_typical_clusters,
            len_a1a3_group=len_adapted_a1a3_set,
            is_pg=False
        )

        cluster_label_list = np.zeros(len_adapted_a1a3_set + len_adapted_typical_set)
        cluster_idx = 0
        for cluster in adapted_a1a3_clusters:
            cluster_label_list[list(cluster)] = cluster_idx
            cluster_idx += 1
        for cluster in adapted_typical_clusters:
            cluster_label_list[np.array(list(cluster)) + len_adapted_a1a3_set] = cluster_idx
            cluster_idx += 1

        X = matrix_for_scoring[:, :, :-2]
        y = np.zeros((len_adapted_a1a3_set+len_adapted_typical_set,
                      len_adapted_a1a3_clusters + len_adapted_typical_clusters))
        # one-hot encode target column
        y_cluster = to_categorical(cluster_label_list)

        ###############
        testing_pg_n_fgl_b_matrix = get_pg_fglb_2dim(
            *inputs_prob_matrices,
            is_testing=True,
            x_set=x_set
        )

        test_matrix_for_scoring, _ = generate_prob_matrix_with_cluster_labels(
            *testing_pg_n_fgl_b_matrix,
            adapted_a1a3_clusters, adapted_typical_clusters,
            len_adapted_a1a3_clusters, len_adapted_typical_clusters,
            len_a1a3_group=len_adapted_a1a3_set,
            is_pg=False
        )

        X_test = test_matrix_for_scoring[:, :, :-2]
        # X_test = testing_sample_by_pdfs(inputs_prob_matrices, len_adapted_a1a3_set, x_set)

        return X, y_cluster, cluster_label_list, X_test
        # inputs_prob_matrices, len_adapted_a1a3_set

    ###########################################

def testing_sample_by_pdfs(inputs_prob_matrices, len_adapted_a1a3_set, x_set):
    testing_pg_n_fgl_b_matrix = get_pg_fglb_2dim(
        *inputs_prob_matrices,
        is_testing=True,
        x_set=x_set
    )
    adapted_a1a3_clusters, adapted_typical_clusters = inputs_prob_matrices[-7:-5]
    len_adapted_a1a3_clusters = len(adapted_a1a3_clusters)
    len_adapted_typical_clusters = len(adapted_typical_clusters)

    test_matrix_for_scoring, _ = generate_prob_matrix_with_cluster_labels(
        *testing_pg_n_fgl_b_matrix,
        adapted_a1a3_clusters, adapted_typical_clusters,
        len_adapted_a1a3_clusters, len_adapted_typical_clusters,
        len_a1a3_group=len_adapted_a1a3_set,
        is_pg=False
    )

    X_test = test_matrix_for_scoring[:, :, :-2]
    return X_test
