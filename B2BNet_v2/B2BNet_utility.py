import numpy as np
from .Kruskal_algorithm import *
from .load_group_data import \
    representation2representation, \
    get_hubs, \
    get_top_hubs, \
    generate_directed_graph, \
    generate_graph_for_Kruskal


def f_divergence(representation_set1, representation_set2, valid_positions,
                 is_same=True, is_smooth=True):

    row_len = len(representation_set1)
    col_len = len(representation_set2)

    if is_same:
        col_len = row_len
        representation_set2 = representation_set1

    diver = np.zeros((row_len, col_len))
    for i in range(row_len):
        for j in range(col_len):
            kl_ij = representation2representation(
                representation_set1[i],
                representation_set2[j],
                valid_positions
            )
            if is_same or not is_smooth:
                diver[i][j] = kl_ij
            elif not is_same:
                kl_ji = representation2representation(
                    representation_set2[j],
                    representation_set1[i],
                    valid_positions
                )
                diver[i][j] = (kl_ij + kl_ji) / 2
    return diver


def f_hub(b_len, mst, hub_thres):
    hubs = get_hubs(b_len, mst, hub_thres)
    # print('hub:', hubs)
    return hubs


def f_hub_top_k(b_len, mst, k):
    hubs = get_top_hubs(b_len, mst, k)
    # print('hub:', hubs)
    return hubs


def f_net(b_len, diver_b_set):
    net = generate_graph_for_Kruskal(b_len, diver_b_set)
    return net


def f_mst(net):
    return net.KruskalMST()


def f_closeness(b_set_len, diver_b_set):
    nx_g = generate_directed_graph(b_set_len, diver_b_set)
    nx_g_closeness = nx.closeness_centrality(nx_g, distance='weighted')
    node_closeness = []
    for v in nx_g.nodes():
        node_closeness.append(nx_g_closeness[v])
    closeness = np.array(node_closeness)
    return closeness




