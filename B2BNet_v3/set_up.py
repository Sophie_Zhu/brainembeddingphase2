import numpy as np
from tqdm import tqdm

from B2BNet_v3.load_group_data import \
    load_data, load_representation, resolve_valid_positions

from B2BNet_v3.MST import MST
from B2BNet_v3.Hubs import Hubs, write_leave_one_centroids_by_index
from B2BNet_v3.DivergenceVector import DivergenceVector, write_leave_one_diver_for_training, \
    write_leave_one_diver_for_test, write_leave_one_pick_take_one_diver_for_training
from B2BNet_v3.PDFParam import PDFParam, write_leave_one_pdf_params_for_training
from B2BNet_v3.Prob import Prob, write_leave_one_prob_for_training, \
    write_leave_one_prob_for_test, write_leave_one_pick_take_one_prob_for_training
from B2BNet_v3.Weights import Weights

experiment_path = 'Samsung_T5/Experiment2019/Exp201910_hub_sort_NYU/'

##############################
# STEP 1: write msts
##############################
# adhd_mst = MST('ADHD', experiment_path)
# typical_mst = MST('Typical', experiment_path)
#
# adhd_mst.WriteLeaveNoneMST()
# adhd_mst.WriteTakeOneMST()
# adhd_mst.WriteLeaveOneMST()
#
# typical_mst.WriteLeaveNoneMST()
# typical_mst.WriteTakeOneMST()
# typical_mst.WriteLeaveOneMST()

##############################
# STEP 2: write hubs
##############################
# adhd_hubs = Hubs('ADHD', experiment_path)
# typical_hubs = Hubs('Typical', experiment_path)
#
# print('write adhd hubs...')
# adhd_hubs.WriteLeaveNoneHubs()
# adhd_hubs.WriteTakeOneHubs()
# adhd_hubs.WriteLeaveOneHubs()
#
# print('write typical hubs...')
# typical_hubs.WriteLeaveNoneHubs()
# typical_hubs.WriteTakeOneHubs()
# typical_hubs.WriteLeaveOneHubs()
#
# print('write adhd leave none aligned hubs...')
# adhd_hubs.WriteLeaveNoneAlignedHubs()
# print('write adhd take one aligned hubs...')
# adhd_hubs.WriteTakeOneAlignedHubs()
# print('write adhd leave one aligned hubs...')
# adhd_hubs.WriteLeaveOneAlignedHubs()

# print('write typical leave none aligned hubs...')
# typical_hubs.WriteLeaveNoneAlignedHubs()
# print('write typical take one aligned hubs...')
# typical_hubs.WriteTakeOneAlignedHubs()
# print('write typical leave one aligned hubs...')
# typical_hubs.WriteLeaveOneAlignedHubs()

##############################
# STEP 3: write centroids
##############################
# write_leave_one_centroids_by_index(experiment_path)


##############################
# STEP 3: write divergence vector
##############################
# adhd_diver = DivergenceVector('ADHD', experiment_path)
# typical_diver = DivergenceVector('Typical', experiment_path)
#
#
# adhd_diver.WriteLeaveNoneForTraining()
# adhd_diver.WriteTakeOneForTraining()
#
# typical_diver.WriteLeaveNoneForTraining()
# typical_diver.WriteTakeOneForTraining()

# write_leave_one_diver_for_training(experiment_path)
# write_leave_one_diver_for_test(experiment_path)
##############################
# STEP 4: write estimated pdf params
##############################
# adhd_pdf = PDFParam('ADHD', experiment_path)
# typical_pdf = PDFParam('Typical', experiment_path)
#
#
# adhd_pdf.WriteLeaveNoneForTraining()
# adhd_pdf.WriteTakeOneForTraining()
#
# typical_pdf.WriteLeaveNoneForTraining()
# typical_pdf.WriteTakeOneForTraining()
#
# write_leave_one_pdf_params_for_training(experiment_path)

##############################
# STEP 5: write prob
##############################
# adhd_prob = Prob('ADHD', experiment_path)
# typical_prob = Prob('Typical', experiment_path)
#
# adhd_prob.WriteLeaveNoneForTraining()
# adhd_prob.WriteTakeOneForTraining()
#
# typical_prob.WriteLeaveNoneForTraining()
# typical_prob.WriteTakeOneForTraining()
#
# write_leave_one_prob_for_training(experiment_path)
# write_leave_one_prob_for_test(experiment_path)
##############################
# STEP 6: cnn
##############################
# write_leave_one_pick_take_one_diver_for_training(experiment_path)
# write_leave_one_pick_take_one_prob_for_training(experiment_path)


