import pickle
import numpy as np
from tqdm import tqdm
from .DivergenceMatrix import f_divergence, get_leave_ij_set
from .DivergenceVector import DivergenceVector, load_leave_one_diver_for_training
from .load_group_data import \
    path_prefix, kl_matrix_dict, load_data, \
    load_representation, \
    resolve_valid_positions
from .params_utility import b_set_to_b2_centroids_moments, x_set_to_b2_centroids_by_b_clusters


def f_local_latent_groups(diver_b_to_x_cents):
    diver_size = np.shape(diver_b_to_x_cents)
    length_clusters = diver_size[1]
    x_clusters = [set() for _ in range(length_clusters)]

    for idx_in_b in range(diver_size[0]):
        m_index = np.argmin(diver_b_to_x_cents[idx_in_b])
        x_clusters[int(m_index)].add(idx_in_b)
    return x_clusters


class PDFParam:
    def __init__(self,
                 group_name,
                 experiment_path,
                 is_gauss='norm',
                 hub_top_k=5
                 ):
        self.experiment_path = experiment_path
        self.group_name = group_name

        self.is_gauss = is_gauss
        self.hub_top_k = hub_top_k

        self.adhdDiver = DivergenceVector('ADHD', self.experiment_path)
        self.typicalDiver = DivergenceVector('Typical', self.experiment_path)

        self.data_dict = load_data()
        if group_name == 'ADHD':
            self.this_group = self.data_dict['a1a3_group']
            self.this_group_diver = self.adhdDiver.LoadLeaveNoneForTraining()

        elif group_name == 'Typical':
            self.this_group = self.data_dict['typical_group']
            self.this_group_diver = self.typicalDiver.LoadLeaveNoneForTraining()

        self.b_set = self.this_group
        self.group_len = len(self.this_group)

        representation_dict = load_representation(self.data_dict)
        input_representations = representation_dict['input_representations']
        self.individuals_relative = representation_dict['individuals_relative']
        self.ta1a3_group = self.data_dict['ta1a3_group']
        valid_position_dict = resolve_valid_positions(input_representations, self.ta1a3_group)
        self.valid_positions = valid_position_dict['valid_positions']

    def WriteLeaveNoneForTraining(self):
        diver_b_cent_g1 = self.this_group_diver[:, :self.hub_top_k]
        diver_b_cent_g2 = self.this_group_diver[:, self.hub_top_k:]
        if self.group_name == 'ADHD':
            b_cluster = f_local_latent_groups(diver_b_cent_g1)
        elif self.group_name == 'Typical':
            b_cluster = f_local_latent_groups(diver_b_cent_g2)

        estimated_pdf_params = \
            b_set_to_b2_centroids_moments(self.is_gauss, self.this_group_diver, b_cluster)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_leave_none_pdf_params.txt", "wb") as fp:
            pickle.dump(estimated_pdf_params, fp)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_leave_none_cluster.txt", "wb") as fp:
            pickle.dump(b_cluster, fp)

    def LoadLeaveNoneForTraining(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_leave_none_pdf_params.txt", 'rb')
        estimated_pdf_params = pickle.load(infile)
        infile.close()
        return estimated_pdf_params

    def LoadLeaveNoneCluster(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_leave_none_cluster.txt", 'rb')
        leave_none_cluster = pickle.load(infile)
        infile.close()
        return leave_none_cluster

    def WriteTakeOneForTraining(self):
        if self.group_name == 'ADHD':
            take_one_diver = self.adhdDiver.LoadTakeOneForTraining()
        elif self.group_name == 'Typical':
            take_one_diver = self.typicalDiver.LoadTakeOneForTraining()

        group_take_one_estimated_pdf_params = []
        group_take_one_b_clusters = []

        # TODO: np.array(diver)
        for this_take_one in tqdm(range(self.group_len)):
            this_diver = take_one_diver[this_take_one]
            this_diver_b_cent_g1 = this_diver[:, :self.hub_top_k]
            this_diver_b_cent_g2 = this_diver[:, self.hub_top_k:]
            if self.group_name == 'ADHD':
                this_b_cluster = f_local_latent_groups(this_diver_b_cent_g1)
            elif self.group_name == 'Typical':
                this_b_cluster = f_local_latent_groups(this_diver_b_cent_g2)
            estimated_pdf_params = \
                b_set_to_b2_centroids_moments(self.is_gauss, this_diver, this_b_cluster)

            group_take_one_estimated_pdf_params.append(estimated_pdf_params)
            group_take_one_b_clusters.append(this_b_cluster)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_take_one_pdf_params.txt", "wb") as fp:
            pickle.dump(group_take_one_estimated_pdf_params, fp)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_take_one_clusters.txt", "wb") as fp:
            pickle.dump(group_take_one_b_clusters, fp)

    def LoadTakeOneForTraining(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_take_one_pdf_params.txt", 'rb')
        take_one_estimated_pdf_params = pickle.load(infile)
        infile.close()
        return take_one_estimated_pdf_params

    def LoadTakeOneCluster(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_take_one_clusters.txt", 'rb')
        take_one_clusters = pickle.load(infile)
        infile.close()
        return take_one_clusters


def write_leave_one_pdf_params_for_training(experiment_path, is_gauss='norm', hub_top_k=5):
    data_dict = load_data()
    ta1a3_group = data_dict['ta1a3_group']
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    len_universal = len_a1a3_group + len_typical_group

    leave_one_adhd_cluster_pdf_params = []
    leave_one_typical_cluster_pdf_params = []
    # centroids size: n,200,200
    leave_one_diver = np.array(load_leave_one_diver_for_training(experiment_path))
    leave_one_adhd_clusters = []
    leave_one_typical_clusters = []

    for leave_one in tqdm(range(len_universal)):
        take_one_adhd_cluster_pdf_params = []
        take_one_typical_cluster_pdf_params = []

        take_one_adhd_clusters = []
        take_one_typical_clusters = []

        for take_one in tqdm(range(len_universal - 1)):
            this_diver = leave_one_diver[leave_one][take_one]

            if leave_one < len_a1a3_group and take_one < len_a1a3_group - 1:
                this_adhd_diver_cents = this_diver[:len_a1a3_group - 2, :]
                this_typical_diver_cents = this_diver[len_a1a3_group - 2:, :]
            elif leave_one < len_a1a3_group and take_one >= len_a1a3_group - 1:
                this_adhd_diver_cents = this_diver[:len_a1a3_group - 1, :]
                this_typical_diver_cents = this_diver[len_a1a3_group - 1:, :]
            elif leave_one >= len_a1a3_group > take_one:
                this_adhd_diver_cents = this_diver[:len_a1a3_group - 1, :]
                this_typical_diver_cents = this_diver[len_a1a3_group - 1:, :]
            elif leave_one >= len_a1a3_group and take_one >= len_a1a3_group:
                this_adhd_diver_cents = this_diver[:len_a1a3_group, :]
                this_typical_diver_cents = this_diver[len_a1a3_group:, :]

            this_adhd_cluster = f_local_latent_groups(this_adhd_diver_cents[:, :hub_top_k])
            this_typical_cluster = f_local_latent_groups(this_typical_diver_cents[:, hub_top_k:])

            adhd_estimated_pdf_params = \
                b_set_to_b2_centroids_moments(is_gauss, this_adhd_diver_cents, this_adhd_cluster)

            typical_estimated_pdf_params = \
                b_set_to_b2_centroids_moments(is_gauss, this_typical_diver_cents, this_typical_cluster)

            take_one_adhd_cluster_pdf_params.append(adhd_estimated_pdf_params)
            take_one_typical_cluster_pdf_params.append(typical_estimated_pdf_params)

            take_one_adhd_clusters.append(this_adhd_cluster)
            take_one_typical_clusters.append(this_typical_cluster)

        leave_one_adhd_cluster_pdf_params.append(take_one_adhd_cluster_pdf_params)
        leave_one_typical_cluster_pdf_params.append(take_one_typical_cluster_pdf_params)
        leave_one_adhd_clusters.append(take_one_adhd_clusters)
        leave_one_typical_clusters.append(take_one_typical_clusters)

    with open(path_prefix + experiment_path +
              "_leave_one_adhd_pdf_params.txt", "wb") as fp:
        pickle.dump(leave_one_adhd_cluster_pdf_params, fp)
    with open(path_prefix + experiment_path +
              "_leave_one_typical_pdf_params.txt", "wb") as fp:
        pickle.dump(leave_one_typical_cluster_pdf_params, fp)

    with open(path_prefix + experiment_path +
              "_leave_one_adhd_clusters.txt", "wb") as fp:
        pickle.dump( leave_one_adhd_clusters, fp)
    with open(path_prefix + experiment_path +
              "_leave_one_typical_clusters.txt", "wb") as fp:
        pickle.dump(leave_one_typical_clusters, fp)


def load_leave_one_pdf_params_for_training(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_leave_one_adhd_pdf_params.txt", 'rb')
    leave_one_adhd_pdf_params = pickle.load(infile)
    infile.close()
    infile = open(path_prefix + experiment_path +
                  "_leave_one_typical_pdf_params.txt", 'rb')
    leave_one_typical_pdf_params = pickle.load(infile)
    infile.close()
    return leave_one_adhd_pdf_params, leave_one_typical_pdf_params


def load_leave_one_clusters(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_leave_one_adhd_clusters.txt", 'rb')
    leave_one_adhd_clusters = pickle.load(infile)
    infile.close()
    infile = open(path_prefix + experiment_path +
                  "_leave_one_typical_clusters.txt", 'rb')
    leave_one_typical_clusters = pickle.load(infile)
    infile.close()
    return leave_one_adhd_clusters, leave_one_typical_clusters
