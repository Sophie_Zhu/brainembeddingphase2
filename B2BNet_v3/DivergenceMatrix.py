import numpy as np
from .load_group_data import \
    representation2representation, \
    path_prefix, kl_matrix_dict


def f_divergence(representation_set1, representation_set2, valid_positions,
                 is_same=True, is_smooth=True):

    row_len = len(representation_set1)
    col_len = len(representation_set2)

    if is_same:
        col_len = row_len
        representation_set2 = representation_set1

    diver = np.zeros((row_len, col_len))
    for i in range(row_len):
        for j in range(col_len):
            kl_ij = representation2representation(
                representation_set1[i],
                representation_set2[j],
                valid_positions
            )
            # This version:
            if not is_smooth:
                diver[i][j] = kl_ij
            elif is_smooth:
                kl_ji = representation2representation(
                    representation_set2[j],
                    representation_set1[i],
                    valid_positions
                )
                diver[i][j] = (kl_ij + kl_ji) / 2
    return diver


def load_divergence(group_name, experiment_path):
    file_path = path_prefix + experiment_path
    kl_matrix = np.load(file_path + kl_matrix_dict[group_name] + '.npy')
    return kl_matrix


def load_smooth_divergence(group_name, experiment_path):
    file_path = path_prefix + experiment_path
    kl_matrix = np.load(file_path + kl_matrix_dict[group_name] + '_smooth' + '.npy')
    return kl_matrix


def get_leave_ij_divergence(kl_matrix, outer_i, inner_j):
    leave_outer_i_matrix = get_leave_one_matrix(outer_i, kl_matrix)
    if outer_i == -1:
        return kl_matrix
    if inner_j == -1:
        return leave_outer_i_matrix
    else:
        leave_ij_matrix = get_leave_one_matrix(inner_j, leave_outer_i_matrix)
        return leave_ij_matrix


def get_leave_one_matrix(leave_one_index, kl_matrix):
    cut_matrix = np.delete(kl_matrix.copy(), leave_one_index, axis=1)
    cut_matrix = np.delete(cut_matrix, leave_one_index, axis=0)
    return cut_matrix


def get_leave_ij_set(b_set, outer_i: int, inner_j: int):
    b_arr = b_set.copy()
    if (outer_i < len(b_set)) and (outer_i > -1):
        b_leave_one_arr = np.delete(b_arr, outer_i)
    else:
        return b_arr

    if inner_j == -1:
        return b_leave_one_arr
    else:
        b_leave_twice_arr = np.delete(b_leave_one_arr, inner_j)
        return b_leave_twice_arr

