import pickle
from tqdm import tqdm
from .load_group_data import \
    load_data,\
    path_prefix, kl_matrix_dict, \
    generate_graph_for_Kruskal
from .DivergenceMatrix import load_divergence, get_leave_one_matrix


def f_net(b_len, diver_b_set):
    net = generate_graph_for_Kruskal(b_len, diver_b_set)
    return net


def f_mst(net):
    return net.KruskalMST()


class MST:
    def __init__(self, group_name, experiment_path):
        self.group_name = group_name
        self.experiment_path = experiment_path

        self.data_dict = load_data()
        if group_name == 'ADHD':
            self.this_group = self.data_dict['a1a3_group']

        elif group_name == 'Typical':
            self.this_group = self.data_dict['typical_group']
        self.group_len = len(self.this_group)

    def WriteLeaveNoneMST(self):
        kl_matrix = load_divergence(self.group_name, self.experiment_path)
        print(f"writing mst (leave_none) - {self.group_name} group ...")
        mst = f_mst(f_net(self.group_len, kl_matrix))
        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_leave_none_mst.txt", "wb") as fp:
            pickle.dump(mst, fp)

    def LoadLeaveNoneMST(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_leave_none_mst.txt", 'rb')
        group_leave_none_mst = pickle.load(infile)
        infile.close()
        return group_leave_none_mst

    def WriteTakeOneMST(self):
        kl_matrix = load_divergence(self.group_name, self.experiment_path)
        group_take_one_mst = []
        for i in tqdm(range(self.group_len)):
            take_one_matrix = get_leave_one_matrix(i, kl_matrix)
            mst = f_mst(f_net(self.group_len-1, take_one_matrix))
            group_take_one_mst.append(mst)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_take_one_mst.txt", "wb") as fp:
            pickle.dump(group_take_one_mst, fp)

    def LoadTakeOneMST(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_take_one_mst.txt", 'rb')
        group_take_one_mst = pickle.load(infile)
        infile.close()
        return group_take_one_mst

    def WriteLeaveOneMST(self):
        kl_matrix = load_divergence(self.group_name, self.experiment_path)
        group_leave_twice_mst = []
        for i in tqdm(range(self.group_len)):
            leave_one_outer_matrix = get_leave_one_matrix(i, kl_matrix)
            #
            inner_mst = []
            for j in range(self.group_len - 1):
                leave_one_inner_matrix = get_leave_one_matrix(j, leave_one_outer_matrix)
                mst = f_mst(f_net(self.group_len - 2, leave_one_inner_matrix))
                inner_mst.append(mst)

            # group_mst = f_mst(f_net(self.group_len - 1, leave_one_outer_matrix))
            # inner_mst.append(group_mst)

            group_leave_twice_mst.append(inner_mst)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_leave_one_mst.txt", "wb") as fp:
            pickle.dump(group_leave_twice_mst, fp)

    def LoadLeaveOneMST(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_leave_one_mst.txt", 'rb')
        group_leave_twice_mst = pickle.load(infile)
        infile.close()
        return group_leave_twice_mst
