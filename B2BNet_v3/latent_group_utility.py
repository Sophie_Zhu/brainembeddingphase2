import numpy as np
import scipy.stats
from .DivergenceMatrix import f_divergence


# sef.b_set, self.clusters
class MultiCentroid:
    def __init__(self, b_set,
                 individuals_relative, valid_positions,
                 is_gauss='gauss'):
        self.b_set = b_set

        self.individuals_relative = individuals_relative
        self.valid_positions = valid_positions
        self.is_gauss = is_gauss

    def get_diver_b_cent_b2(self, b2_centroids):
        return f_divergence(
            self.individuals_relative[self.b_set],
            b2_centroids,
            self.valid_positions,
            is_same=False,
            is_smooth=False
        )

    def get_diver_b_cent_b2_smooth(self, b2_centroids):
        return f_divergence(
            self.individuals_relative[self.b_set],
            b2_centroids,
            self.valid_positions,
            is_same=False,
        )

    def get_diver_x_set_cent_y(self, x_set, y_centroids):
        return f_divergence(
            self.individuals_relative[x_set],
            y_centroids,
            self.valid_positions,
            is_same=False,
            is_smooth=False
        )

    def get_diver_x_set_cent_y_smooth(self, x_set, y_centroids):
        return f_divergence(
            self.individuals_relative[x_set],
            y_centroids,
            self.valid_positions,
            is_same=False
        )

    def get_diver_b_to_b2_centroids(self, b2_centroids):
        diver_b_cent_b2 = self.get_diver_b_cent_b2_smooth(b2_centroids) # attention!
        return diver_b_cent_b2
