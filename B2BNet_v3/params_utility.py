import numpy as np
import scipy.stats
from tqdm import tqdm


def b_set_to_b2_centroids_moments(is_gauss, diver_b_cent_b2, b_clusters):
    if is_gauss == 'skewnorm':
        cluster_to_b2_centroids_fitting_params = []
        for cluster in b_clusters:
            # cluster is a set
            diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
            params_list = [scipy.stats.skewnorm.fit(diver_g_b2_centroids[:, centroid_idx])
                           for centroid_idx in range(np.shape(diver_g_b2_centroids)[1])]
            cluster_to_b2_centroids_fitting_params.append(
                params_list
            )  # fit for each centroid
        return cluster_to_b2_centroids_fitting_params
    elif is_gauss == 'norm':
        cluster_to_b2_centroids_fitting_params = []
        for cluster in b_clusters:
            # cluster is a set
            diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
            cluster_to_b2_centroids_fitting_params.append(
                [scipy.stats.norm.fit(diver_g_b2_centroids[:, centroid_idx])
                 for centroid_idx in range(np.shape(diver_g_b2_centroids)[1])]
            )  # fit for each centroid
        return cluster_to_b2_centroids_fitting_params
    elif is_gauss == 'expon':
        cluster_to_b2_centroids_fitting_params = []
        for cluster in b_clusters:
            # cluster is a set
            diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
            cluster_to_b2_centroids_fitting_params.append(
                [scipy.stats.expon.fit(diver_g_b2_centroids[:, centroid_idx])
                 for centroid_idx in range(np.shape(diver_g_b2_centroids)[1])]
            )
        return cluster_to_b2_centroids_fitting_params
    elif is_gauss == 'gaussian_kde':
        cluster_to_b2_centroids_fitting_params = []
        for cluster in b_clusters:
            # cluster is a set
            diver_g_b2_centroids = diver_b_cent_b2[list(cluster)]
            cluster_to_b2_centroids_fitting_params.append(
                [scipy.stats.gaussian_kde(diver_g_b2_centroids[:, centroid_idx])
                 for centroid_idx in range(np.shape(diver_g_b2_centroids)[1])]
            )
        return cluster_to_b2_centroids_fitting_params

# test:
# diver_x_set_cent_b2 = self.get_diver_x_set_cent_y_smooth(x_set, b2_centroids)


def x_set_to_b2_centroids_by_b_clusters_copy(
        is_gauss,
        estimated_params,
        diver_x_set_cent_b2,
        ):

    size_diver_x_set_cent_b2 = np.shape(diver_x_set_cent_b2)
    len_x_set = size_diver_x_set_cent_b2[0]
    len_b2_centroids = size_diver_x_set_cent_b2[1]
    len_b_clusters = len(estimated_params)

    fgl_b_matrix = np.zeros((len_x_set, len_b_clusters, len_b2_centroids))

    for cluster_index in range(len_b_clusters):

        for subject_index in range(len_x_set):

            for centroid_index in range(len_b2_centroids):
                x_line = diver_x_set_cent_b2[subject_index, centroid_index]

                if is_gauss == 'norm':
                    fgl_b = scipy.stats.norm.pdf(x_line,
                                                 *estimated_params[cluster_index][centroid_index])

                elif is_gauss == 'skewnorm':
                    fgl_b = scipy.stats.skewnorm.pdf(x_line,
                                                     *estimated_params[cluster_index][centroid_index])
                elif is_gauss == 'expon':
                    fgl_b = scipy.stats.expon.pdf(x_line,
                                                  *estimated_params[cluster_index][centroid_index])
                elif is_gauss == 'gaussian_kde':
                    kernel = estimated_params[cluster_index][centroid_index]
                    fgl_b = kernel.pdf(x_line)

                fgl_b_matrix[subject_index, cluster_index, centroid_index] = fgl_b

    return fgl_b_matrix


def x_set_to_b2_centroids_by_b_clusters(
        is_gauss,
        estimated_params,
        diver_x_set_cent_b2,
        ):

    size_diver_x_set_cent_b2 = np.shape(diver_x_set_cent_b2)
    len_x_set = size_diver_x_set_cent_b2[0]
    len_b2_centroids = size_diver_x_set_cent_b2[1]
    len_b_clusters = len(estimated_params)

    fgl_b_matrix = np.zeros((len_x_set, len_b_clusters, len_b2_centroids))

    # [[stats.norm(m, s).pdf(x) for x in range(5,30)] for m,s in zip(means,scales)]
    # for cluster_index in range(len_b_clusters):
    #     res = list(zip(*estimated_params[cluster_index][:]))
    #     means = np.array(res[0])
    #     scales = np.array(res[1])

        # for subject_index in range(len_x_set):
        #    x_line = diver_x_set_cent_b2[subject_index, :]

    res = np.array(list(zip(*estimated_params)))

    means = np.array(res[:, :, 0]).T # (5,10)
    scales = np.array(res[:, :, 1]).T

    x_mat = np.zeros((len_x_set, len_b_clusters, len_b2_centroids))
    for subject_index in range(len_x_set):
        x_mat[subject_index, :, :] = [diver_x_set_cent_b2[subject_index, :], ]*len_b_clusters

    # print('hi')
    # print(np.shape(x_mat)) (170,5,10)
    fgl_b = scipy.stats.norm.pdf(x_mat, means, scales)
    fgl_b_matrix = fgl_b

    return fgl_b_matrix

    # for cluster_index in range(len_b_clusters):
    #
    #     for subject_index in range(len_x_set):
    #
    #         for centroid_index in range(len_b2_centroids):
    #             x_line = diver_x_set_cent_b2[subject_index, centroid_index]
    #
    #             if is_gauss == 'norm':
    #                 fgl_b = scipy.stats.norm.pdf(x_line,
    #                                              *estimated_params[cluster_index][centroid_index])
    #
    #             elif is_gauss == 'skewnorm':
    #                 fgl_b = scipy.stats.skewnorm.pdf(x_line,
    #                                                  *estimated_params[cluster_index][centroid_index])
    #             elif is_gauss == 'expon':
    #                 fgl_b = scipy.stats.expon.pdf(x_line,
    #                                               *estimated_params[cluster_index][centroid_index])
    #             elif is_gauss == 'gaussian_kde':
    #                 kernel = estimated_params[cluster_index][centroid_index]
    #                 fgl_b = kernel.pdf(x_line)
    #
    #             fgl_b_matrix[subject_index, cluster_index, centroid_index] = fgl_b
    #
    # return fgl_b_matrix

