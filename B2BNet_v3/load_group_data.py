from .load_BI_version_data import *
from scipy import stats
from .relative_distributional_representation import *
from .Kruskal_algorithm import *


ubuntu_prefix = '/media/sophie/'
mac_prefix = '/Volumes/'
path_prefix = ubuntu_prefix
kl_matrix_dict = {'Typical': 'kl_typical',  'ADHD': 'kl_adhd', 'Universal': 'kl_universal'}


def load_data():
    DX_label, QC_Rest1 = load_label()
    con2d_path = ubuntu_prefix +'Samsung_T5/Data/MyResults/Experiment_nyu_BI/'
    h_con2d = np.load(con2d_path + 'h_con2d.npz')
    hist2d_path = ubuntu_prefix +'Samsung_T5/Data/MyResults/Experiment_nyu_BI/'

    h_joint2dcnt = np.load(hist2d_path + 'h_joint2dcnt.npz')
    h_joint2d = np.load(hist2d_path + 'h_joint2d.npz')

    ql = np.where(QC_Rest1 == 0)[0]
    al = [0, 34, 47, 96, 72, 83, 65, 6, 126, 109, 118, 190, 122, 141, 131]
    alienate_typical = [i for i in al if DX_label[i] == 0]
    alienate_adhd = [i for i in al if DX_label[i] != 0]
    qc0_rest1_typical = [i for i in ql if DX_label[i] == 0]
    qc0_rest1_adhd = [i for i in ql if DX_label[i] != 0]

    idx_arr = np.arange(len(h_con2d['arr_0']))
    idx_arr = resolve_valid_indexes(idx_arr, QC_Rest1)

    typical_group \
        = [idx_arr[i] for i in range(len(idx_arr)) if DX_label[idx_arr[i]] == 0]
    adhd_group \
        = [idx_arr[i] for i in range(len(idx_arr)) if DX_label[idx_arr[i]] != 0]
    adhd1_group \
        = [idx_arr[i] for i in range(len(idx_arr)) if DX_label[idx_arr[i]] == 1]
    adhd3_group \
        = [idx_arr[i] for i in range(len(idx_arr)) if DX_label[idx_arr[i]] == 3]

    portion = -1

    typical_group \
        = typical_group[:portion] if portion != -1 else typical_group[:]
    adhd1_group \
        = adhd1_group[:portion] if portion != -1 else adhd1_group[:]
    adhd3_group \
        = adhd3_group[:portion] if portion != -1 else adhd3_group[:]

    a1a3_group = np.concatenate([adhd1_group, adhd3_group])
    a1a3_group = a1a3_group.astype(int)
    ta1a3_group = np.concatenate([a1a3_group, typical_group])
    ta1a3_group = ta1a3_group.astype(int)

    ta3_group = np.concatenate([adhd3_group, typical_group])
    ta3_group = ta3_group.astype(int)

    ta1_group = np.concatenate([adhd1_group, typical_group])
    ta1_group = ta1_group.astype(int)

    adhd1_end = len(adhd1_group)
    adhd3_end = len(adhd1_group) + len(adhd3_group)
    typical_end = adhd3_end + len(typical_group)

    return {
        'h_joint2dcnt': h_joint2dcnt,
        'typical_group': typical_group,
        'adhd_group': adhd_group,
        'adhd1_group': adhd1_group,
        'adhd3_group': adhd3_group,
        'ta1a3_group': ta1a3_group,
        'ta3_group': ta3_group,
        'ta1_group': ta1_group,
        'a1a3_group': a1a3_group,
        'adhd1_end': adhd1_end,
        'adhd3_end': adhd3_end,
        'typical_end': typical_end
    }


def load_representation(data_dict):
    h_joint2dcnt = data_dict['h_joint2dcnt']
    adhd1_group = data_dict['adhd1_group']
    adhd3_group = data_dict['adhd3_group']
    typical_group = data_dict['typical_group']
    ta1a3_group = data_dict['ta1a3_group']
    ta3_group = data_dict['ta3_group']
    a1a3_group = data_dict['a1a3_group']
    ta1_group = data_dict['ta1_group']

    input_representations = h_joint2dcnt['arr_0'].copy()
    y_a1 = np.sum(input_representations[adhd1_group], axis=0)
    y_a3 = np.sum(input_representations[adhd3_group], axis=0)
    y_t = np.sum(input_representations[typical_group], axis=0)
    y_ta1a3 = np.sum(input_representations[ta1a3_group], axis=0)
    y_ta3 = np.sum(input_representations[ta3_group], axis=0)
    y_a1a3 = np.sum(input_representations[a1a3_group], axis=0)
    y_ta1 = np.sum(input_representations[ta1_group], axis=0)

    individuals_norm = np.zeros(np.shape(input_representations))

    norm_ta1a3 = y_ta1a3 / np.sum(y_ta1a3)
    individuals_relative = np.zeros(np.shape(individuals_norm))
    for i in range(len(ta1a3_group)):
        individual = input_representations[ta1a3_group[i]]
        individuals_norm[ta1a3_group[i]] = individual / np.sum(individual)
        individuals_relative[ta1a3_group[i]] = individuals_norm[ta1a3_group[i]] / norm_ta1a3

    return {
        'y_a1': y_a1,
        'y_a3': y_a3,
        'y_t': y_t,
        'y_ta1a3': y_ta1a3,
        'y_ta3': y_ta3,
        'y_a1a3': y_a1a3,
        'y_ta1': y_ta1,
        'input_representations': input_representations,
        'individuals_norm': individuals_norm,
        'individuals_relative': individuals_relative,
        'norm_ta1a3': norm_ta1a3
    }


def resolve_valid_positions(input_representations, group_indexes):
    sum_r, valid_positions = sum_representations(input_representations, group_indexes)
    correlation_range = (np.min(valid_positions[0]), np.max(valid_positions[0]))
    distance_range = (np.min(valid_positions[1]), np.max(valid_positions[1]))
    print(correlation_range, distance_range)
    return {
        'valid_positions': valid_positions,
        'correlation_range': correlation_range,
        'distance_range': distance_range
    }


def representation2representation(representation1, representation2, valid_positions):
    return stats.entropy(
                representation1[valid_positions].ravel()
                + np.spacing(1),
                representation2[valid_positions].ravel()
                + np.spacing(1))


def distance_network(group_indexes, individuals_relative, valid_positions):
    relative_KL_matrix = np.zeros((len(group_indexes), len(group_indexes)))
    relative_KL_matrix.fill(np.inf)

    for i in range(len(group_indexes)):
        for j in range(len(group_indexes)):
            if i != j:
                relative_KL_matrix[i][j] = stats.entropy(
                    individuals_relative[group_indexes[i]][valid_positions].ravel()
                    + np.spacing(1),
                    individuals_relative[group_indexes[j]][valid_positions].ravel()
                    + np.spacing(1))

    return relative_KL_matrix


def generate_graph_for_Kruskal(group_len, relative_KL_matrix):
    g = Graph(group_len)
    for i in range(group_len):
        for j in range(group_len):
            if i < j:
                g.addEdge(i, j, (relative_KL_matrix[i, j]+relative_KL_matrix[j, i]) / 2.0)
    return g


def generate_directed_graph(group_len, relative_kl_matrix):
    DG = nx.DiGraph()
    for i in range(group_len):
        for j in range(group_len):
            if i != j:
                DG.add_edge(i, j, weighted=relative_kl_matrix[i, j])
    return DG


def closeness_min_tree(group_indexes, representation_dict, data_dict):
    # data_dict = load_data()
    # representation_dict = load_representation(data_dict)
    input_representations = representation_dict['input_representations']
    ta1a3_group = data_dict['ta1a3_group']
    adhd1_group = data_dict['adhd1_group']
    adhd3_group = data_dict['adhd3_group']
    individuals_relative = representation_dict['individuals_relative']

    valid_position_dict = resolve_valid_positions(input_representations, ta1a3_group)
    valid_positions = valid_position_dict['valid_positions']

    relative_KL_matrix = distance_network(
        group_indexes,
        individuals_relative,
        valid_positions)

    G = generate_graph_for_Kruskal(group_indexes, relative_KL_matrix)
    min_G = G.KruskalMST()

    headers = ["Source", "Target", "Weight"]
    df_min_G = pd.DataFrame(min_G.edges(data=True), index=None, columns=headers)
    weighted = df_min_G['Weight'].values
    for i in range(len(df_min_G['Weight'])):
        df_min_G['Weight'][i] = weighted[i]['weighted']

    #########################
    nxG = generate_directed_graph(group_indexes, relative_KL_matrix)
    nxG_closeness = nx.closeness_centrality(nxG, distance='weighted')
    node_closeness = []
    for v in nxG.nodes():
        if v < len(adhd1_group):
            group_label = 1
        elif v < len(adhd3_group):
            group_label = 3
        else:
            group_label = 0
        node_closeness.append([group_label, nxG_closeness[v]])

    headers = ["Group_label", "Closeness"]
    df_node = pd.DataFrame(node_closeness, index=None, columns=headers)

    return df_min_G, df_node, min_G


def get_hubs(group_len, min_G, hub_thres=5):
    # hub degree>5
    hub_indexes_in_group = []
    for i in range(group_len):
        if min_G.degree[i] > hub_thres:
            hub_indexes_in_group.append(i)
    return hub_indexes_in_group


def get_top_hubs(group_len, min_G, k=5):
    degrees = np.array([min_G.degree[i] for i in range(group_len)])
    argsorted_degrees = np.argsort(degrees)
    last_degree = min_G.degree[argsorted_degrees[-k]]
    hub_indexes_in_group = argsorted_degrees[-k+1:]
    hub_indexes_in_group = np.append(hub_indexes_in_group, np.where(degrees==last_degree))
    hub_set = np.array(list(set(hub_indexes_in_group)))
    # print(hub_set)
    # if 84 in hub_set:
    #     print(group_len, argsorted_degrees[-k+1:])
    return hub_set


def get_hard_wired_centroids():
    return {
        'typical_closeness_centroids_indexes': np.array([81, 24, 29, 61, 15, 28, 73]),
        'adhd1_closeness_centroids_indexes': np.array([32, 46, 38, 5, 47]),
        'adhd3_closeness_centroids_indexes': np.array([1, 15, 5, 3]),
        'a1a3_closeness_centroids_indexes': np.array([47, 61, 71, 32, 46, 57, 85])
    }


def get_shortest_centroid_for_each_individual_by_group(
        group_indexes,
        closeness_centroids_indexes,
        relative_KL_matrix,
        individuals_relative,
        valid_positions,
        group_closeness
):
    # example:
    # get_shortest_centroid_for_each_individual_by_group(
    #         typical_indexes,
    #         typical_closeness_centroids_indexes,
    #         typical_relative_KL_matrix,
    #         individuals_relative,
    #         valid_positions
    #

    shortest_KL_res = []
    for i in range(len(group_indexes)):
        row = i
        group_KL = []
        for j in range(len(closeness_centroids_indexes)):
            col = closeness_centroids_indexes[j]
            if row != col:
                group_KL.append(
                    (relative_KL_matrix[row][col] + relative_KL_matrix[col][row]) / 2
                )
            else:
                group_KL.append(0)
        shortest_KL_res.append(np.argmin(group_KL))

    shortest_KL_res = np.array(shortest_KL_res)
    res = []
    print('Round 1:')
    for i in range(len(closeness_centroids_indexes)):
        print(np.where(shortest_KL_res == i))
        res.append(np.where(shortest_KL_res == i))

    #######################

    # TODO: let 200,200 be variables
    group_centroids = np.zeros((len(closeness_centroids_indexes), 200, 200))

    # weighted sum
    for i in range(len(closeness_centroids_indexes)):
        sum_weight = 0
        for j in res[i][0]:
            sum_weight += group_closeness[j]
        for j in res[i][0]:
            group_centroids[i] += group_closeness[j]/sum_weight * \
                                  individuals_relative[group_indexes[j]]

    shortest_KL_res_round2 = []
    group_KL_round2 = []
    for i in range(len(group_indexes)):
        row = i
        group_KL = []
        for j in range(len(closeness_centroids_indexes)):
            group_KL.append(
                representation2representation(
                    individuals_relative[group_indexes[row]],
                    group_centroids[j], valid_positions)
            )
        group_KL_round2.append(np.min(group_KL))
        shortest_KL_res_round2.append(np.argmin(group_KL))

    shortest_KL_res_round2 = np.array(shortest_KL_res_round2)
    print('Round 2:')
    res_round2 = []
    for i in range(len(closeness_centroids_indexes)):
        print(np.where(shortest_KL_res_round2 == i))
        res_round2.append(np.where(shortest_KL_res_round2 == i))
    return group_centroids, group_KL_round2, res_round2


def get_closeness_groups_by_group_type(
        data_dict,
        representation_dict,
        group_indexes,
        group_closeness_centroids_indexes,
        group_closeness
):

    input_representations = representation_dict['input_representations']
    individuals_relative = representation_dict['individuals_relative']
    ta1a3_group = data_dict['ta1a3_group']

    valid_position_dict = resolve_valid_positions(input_representations, ta1a3_group)
    valid_positions = valid_position_dict['valid_positions']

    group_relative_KL_matrix = distance_network(
        group_indexes,
        individuals_relative,
        valid_positions)

    group_centroids, group_KL_round2, res_round2 = get_shortest_centroid_for_each_individual_by_group(
        group_indexes,
        group_closeness_centroids_indexes,
        group_relative_KL_matrix,
        individuals_relative,
        valid_positions,
        group_closeness
    )
    return group_centroids, group_KL_round2, res_round2
