import pickle
import numpy as np
from tqdm import tqdm
from .DivergenceMatrix import get_leave_ij_set, get_leave_ij_divergence, \
    load_smooth_divergence
from .Hubs import Hubs, load_leave_one_centroids_by_index
from .latent_group_utility import MultiCentroid
from .load_group_data import \
    path_prefix, kl_matrix_dict, load_data, \
    load_representation, \
    resolve_valid_positions


class DivergenceVector:
    def __init__(self,
                 group_name,
                 experiment_path,
                 is_gauss='norm'
                 ):
        self.experiment_path = experiment_path
        self.group_name = group_name

        self.is_gauss = is_gauss

        self.data_dict = load_data()
        if group_name == 'ADHD':
            self.this_group = self.data_dict['a1a3_group']

        elif group_name == 'Typical':
            self.this_group = self.data_dict['typical_group']

        self.b_set = self.this_group
        self.group_len = len(self.this_group)

        representation_dict = load_representation(self.data_dict)
        input_representations = representation_dict['input_representations']
        self.individuals_relative = representation_dict['individuals_relative']
        self.ta1a3_group = self.data_dict['ta1a3_group']
        self.a1a3_group = self.data_dict['a1a3_group']
        self.typical_group = self.data_dict['typical_group']
        valid_position_dict = resolve_valid_positions(input_representations, self.ta1a3_group)
        self.valid_positions = valid_position_dict['valid_positions']

        self.adhdHubs = Hubs('ADHD', self.experiment_path)
        self.typicalHubs = Hubs('Typical', self.experiment_path)

    def WriteLeaveNoneForTraining(self):
        adhd_hubs = self.adhdHubs.LoadLeaveNoneAlignedHubs()
        typical_hubs = self.typicalHubs.LoadLeaveNoneAlignedHubs()
        adhd_centroids = self.individuals_relative[np.array(self.a1a3_group)[adhd_hubs]]
        typical_centroids = self.individuals_relative[np.array(self.typical_group)[typical_hubs]]

        this_group_operation = MultiCentroid(
            self.b_set,
            self.individuals_relative, self.valid_positions,
            self.is_gauss)

        diver_b_g1cent = this_group_operation.get_diver_b_to_b2_centroids(adhd_centroids)
        diver_b_g2cent = this_group_operation.get_diver_b_to_b2_centroids(typical_centroids)

        diver_b_to_cents = np.concatenate([diver_b_g1cent, diver_b_g2cent], axis=1)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_diver_b_to_cents.txt", "wb") as fp:
            pickle.dump(diver_b_to_cents, fp)

    def LoadLeaveNoneForTraining(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_diver_b_to_cents.txt", 'rb')
        group_diver_b_to_cents = pickle.load(infile)
        infile.close()
        return group_diver_b_to_cents

    # TODO
    def WriteTakeOneForTraining(self):
        adhd_hubs = self.adhdHubs.LoadLeaveNoneAlignedHubs()
        typical_hubs = self.typicalHubs.LoadLeaveNoneAlignedHubs()

        adhd_take_one_hubs = \
            self.adhdHubs.LoadTakeOneAlignedHubs()
        typical_take_one_hubs = \
            self.typicalHubs.LoadTakeOneAlignedHubs()

        adhd_centroids = self.individuals_relative[np.array(self.a1a3_group)[adhd_hubs]]
        typical_centroids = self.individuals_relative[np.array(self.typical_group)[typical_hubs]]

        # this_adapted_group = get_leave_ij_set(self.this_group, outer_leave_one, inner_take_one)
        # this_local_centroids = self.individuals_relative[np.array(this_adapted_group)[this_aligned_hubs]]

        if self.group_name == 'ADHD':
            # g1_centroids = adhd_take_one_centroids
            g2_centroids = typical_centroids

        elif self.group_name == 'Typical':
            g1_centroids = adhd_centroids
            # g2_centroids = typical_take_one_centroids

        group_take_one_diver_b_to_cents = []
        for this_take_one in tqdm(range(self.group_len)):
            this_adapted_group = get_leave_ij_set(self.this_group, this_take_one, -1)
            if self.group_name == 'ADHD':
                g1_centroids = \
                    self.individuals_relative[np.array(this_adapted_group)[adhd_take_one_hubs[this_take_one]]]

            elif self.group_name == 'Typical':
                g2_centroids = \
                    self.individuals_relative[np.array(this_adapted_group)[typical_take_one_hubs[this_take_one]]]

            this_group_operation = MultiCentroid(
                this_adapted_group,
                self.individuals_relative, self.valid_positions,
                self.is_gauss)

            diver_b_g1cent = this_group_operation.get_diver_b_to_b2_centroids(g1_centroids)
            diver_b_g2cent = this_group_operation.get_diver_b_to_b2_centroids(g2_centroids)

            diver_b_to_cents = np.concatenate([diver_b_g1cent, diver_b_g2cent], axis=1)
            group_take_one_diver_b_to_cents.append(diver_b_to_cents)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_take_one_diver_b_to_cents.txt", "wb") as fp:
            pickle.dump(group_take_one_diver_b_to_cents, fp)

    def LoadTakeOneForTraining(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_take_one_diver_b_to_cents.txt", 'rb')
        group_diver_b_to_cents = pickle.load(infile)
        infile.close()
        return group_diver_b_to_cents

    def WriteTakeOneForTest(self):
        adhd_hubs = self.adhdHubs.LoadLeaveNoneAlignedHubs()
        typical_hubs = self.typicalHubs.LoadLeaveNoneAlignedHubs()

        adhd_take_one_hubs = \
            self.adhdHubs.LoadTakeOneAlignedHubs()
        typical_take_one_hubs = \
            self.typicalHubs.LoadTakeOneAlignedHubs()

        adhd_centroids = self.individuals_relative[np.array(self.a1a3_group)[adhd_hubs]]
        typical_centroids = self.individuals_relative[np.array(self.typical_group)[typical_hubs]]

        # this_adapted_group = get_leave_ij_set(self.this_group, outer_leave_one, inner_take_one)
        # this_local_centroids = self.individuals_relative[np.array(this_adapted_group)[this_aligned_hubs]]

        if self.group_name == 'ADHD':
            # g1_centroids = adhd_take_one_centroids
            g2_centroids = typical_centroids

        elif self.group_name == 'Typical':
            g1_centroids = adhd_centroids
            # g2_centroids = typical_take_one_centroids

        test_take_one_diver_b_to_cents = []
        for this_take_one in tqdm(range(self.group_len)):

            this_adapted_group = get_leave_ij_set(self.this_group, this_take_one, -1)

            if self.group_name == 'ADHD':
                g1_centroids = \
                    self.individuals_relative[np.array(this_adapted_group)[adhd_take_one_hubs[this_take_one]]]

            elif self.group_name == 'Typical':
                g2_centroids = \
                    self.individuals_relative[np.array(this_adapted_group)[typical_take_one_hubs[this_take_one]]]

            this_take_one_operation = MultiCentroid(
                [self.this_group[this_take_one]],
                self.individuals_relative, self.valid_positions,
                self.is_gauss)

            diver_this_to_g1cent = this_take_one_operation.get_diver_b_to_b2_centroids(g1_centroids)
            diver_this_to_g2cent = this_take_one_operation.get_diver_b_to_b2_centroids(g2_centroids)
            diver_b_to_cents = np.concatenate([diver_this_to_g1cent, diver_this_to_g2cent], axis=1)
            test_take_one_diver_b_to_cents.append(diver_b_to_cents)

            with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_test_take_one_diver_b_to_cents.txt", "wb") as fp:
                pickle.dump(test_take_one_diver_b_to_cents, fp)

    def LoadTakeOneForTest(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_test_take_one_diver_b_to_cents.txt", 'rb')
        group_diver_b_to_cents = pickle.load(infile)
        infile.close()
        return group_diver_b_to_cents


def write_leave_one_diver_for_test(experiment_path, is_gauss='norm'):
    data_dict = load_data()
    ta1a3_group = data_dict['ta1a3_group']
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    representation_dict = load_representation(data_dict)
    input_representations = representation_dict['input_representations']
    individuals_relative = representation_dict['individuals_relative']
    ta1a3_group = data_dict['ta1a3_group']
    valid_position_dict = resolve_valid_positions(input_representations, ta1a3_group)
    valid_positions = valid_position_dict['valid_positions']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    len_universal = len_a1a3_group + len_typical_group
    leave_one_centroids = load_leave_one_centroids_by_index(experiment_path)
    leave_one_centroids_indexes = leave_one_centroids.astype(int)
    kl_matrix = load_smooth_divergence('Universal', experiment_path)

    leave_one_diver_b_to_cents = []
    for leave_one in tqdm(range(len_universal)):
        take_one_diver_b_to_cents = []
        for take_one in tqdm(range(len_universal - 1)):
            # this_adapted_group = get_leave_ij_set(ta1a3_group, leave_one, take_one)
            this_hubs = leave_one_centroids_indexes[leave_one][take_one]
            hubs_reindex = this_hubs.copy()
            adjust_idx = np.where(this_hubs >= take_one)
            hubs_reindex[adjust_idx] = this_hubs[adjust_idx] + 1

            adjust_idx = np.where(hubs_reindex >= leave_one)
            hubs_reindex[adjust_idx] = hubs_reindex[adjust_idx] + 1
            this_leave_one_hubs = hubs_reindex.copy()

            # this_diver = get_leave_ij_divergence(kl_matrix, leave_one, take_one)

            diver_to_g1g2 = kl_matrix[leave_one, this_leave_one_hubs]

            take_one_diver_b_to_cents.append(diver_to_g1g2)

        #     # this_centroids = leave_one_centroids[leave_one][take_one]
        #     if leave_one < len_a1a3_group and take_one < len_a1a3_group - 1:
        #         g1_adapted_group = get_leave_ij_set(a1a3_group, leave_one, take_one)
        #         g2_adapted_group = typical_group.copy()
        #     elif leave_one < len_a1a3_group and take_one >= len_a1a3_group - 1:
        #         typical_take_one = take_one - (len_a1a3_group - 1)
        #         g1_adapted_group = get_leave_ij_set(a1a3_group, leave_one, -1)
        #         g2_adapted_group = get_leave_ij_set(typical_group, typical_take_one, -1)
        #     elif leave_one >= len_a1a3_group > take_one:
        #         typical_take_one = leave_one - len_a1a3_group
        #         g1_adapted_group = get_leave_ij_set(a1a3_group, take_one, -1)
        #         g2_adapted_group = get_leave_ij_set(typical_group, typical_take_one, -1)
        #     elif leave_one >= len_a1a3_group and take_one >= len_a1a3_group:
        #         typical_outer_take_one = leave_one - len_a1a3_group
        #         typical_inner_take_one = take_one - len_a1a3_group
        #         g1_adapted_group = a1a3_group.copy()
        #         g2_adapted_group = get_leave_ij_set(typical_group, typical_outer_take_one, typical_inner_take_one)
        #
        #     leave_one_centroids_indexes = leave_one_centroids[leave_one][take_one]
        #     g1_centroids = individuals_relative[np.array(g1_adapted_group)[leave_one_centroids_indexes[:hub_top_k]]]
        #     g2_centroids = individuals_relative[np.array(g2_adapted_group)[leave_one_centroids_indexes[hub_top_k:]]]
        #     this_centroids = np.concatenate([g1_centroids, g2_centroids])
        #
        #     this_group_operation = MultiCentroid(
        #         [ta1a3_group[leave_one]],
        #         individuals_relative, valid_positions,
        #         is_gauss)
        #
        #     take_one_diver_b_to_cents.append(
        #         this_group_operation.get_diver_b_to_b2_centroids(this_centroids))
        leave_one_diver_b_to_cents.append(take_one_diver_b_to_cents)

    with open(path_prefix + experiment_path +
              "_test_leave_one_diver_b_to_cents.txt", "wb") as fp:
        pickle.dump(leave_one_diver_b_to_cents, fp)


def load_leave_one_diver_for_test(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_test_leave_one_diver_b_to_cents.txt", 'rb')
    leave_one_diver_b_to_cents = pickle.load(infile)
    infile.close()
    return leave_one_diver_b_to_cents


def write_leave_one_diver_for_training(experiment_path, is_gauss='norm', hub_top_k=5):
    data_dict = load_data()
    ta1a3_group = data_dict['ta1a3_group']
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    representation_dict = load_representation(data_dict)
    input_representations = representation_dict['input_representations']
    individuals_relative = representation_dict['individuals_relative']
    ta1a3_group = data_dict['ta1a3_group']
    valid_position_dict = resolve_valid_positions(input_representations, ta1a3_group)
    valid_positions = valid_position_dict['valid_positions']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    len_universal = len_a1a3_group + len_typical_group
    leave_one_centroids = load_leave_one_centroids_by_index(experiment_path)
    leave_one_centroids_indexes = leave_one_centroids.astype(int)
    kl_matrix = load_smooth_divergence('Universal', experiment_path)

    # no_of_cents = np.shape(leave_one_centroids)[0]
    # leave_one_diver_b_to_cents = np.zeros((len_universal, len_universal-1, no_of_cents))
    leave_one_diver_b_to_cents = []
    # centroids size: n,200,200
    for leave_one in tqdm(range(len_universal)):
        take_one_diver_b_to_cents = []
        for take_one in tqdm(range(len_universal - 1)):
            # this_b_set = get_leave_ij_set(ta1a3_group, leave_one, take_one)

            # this_centroids = \
            #     individuals_relative[np.array(this_adapted_group)[leave_one_centroids[leave_one][take_one]]]
            # this_centroids = leave_one_centroids[leave_one][take_one]

            this_diver = get_leave_ij_divergence(kl_matrix, leave_one, take_one)

            diver_to_g1g2 = this_diver[:, leave_one_centroids_indexes[leave_one][take_one]]

            # if leave_one < len_a1a3_group and take_one < len_a1a3_group - 1:
            #    g1_adapted_group = get_leave_ij_set(a1a3_group, leave_one, take_one)
            #    g2_adapted_group = typical_group.copy()
            # elif leave_one < len_a1a3_group and take_one >= len_a1a3_group - 1:
            #     typical_take_one = take_one - (len_a1a3_group - 1)
            #     g1_adapted_group = get_leave_ij_set(a1a3_group, leave_one, -1)
            #     g2_adapted_group = get_leave_ij_set(typical_group, typical_take_one, -1)
            # elif leave_one >= len_a1a3_group > take_one:
            #     typical_take_one = leave_one - len_a1a3_group
            #     g1_adapted_group = get_leave_ij_set(a1a3_group, take_one, -1)
            #     g2_adapted_group = get_leave_ij_set(typical_group, typical_take_one, -1)
            # elif leave_one >= len_a1a3_group and take_one >= len_a1a3_group:
            #     typical_outer_take_one = leave_one - len_a1a3_group
            #     typical_inner_take_one = take_one - len_a1a3_group
            #     g1_adapted_group = a1a3_group.copy()
            #     g2_adapted_group = get_leave_ij_set(typical_group, typical_outer_take_one, typical_inner_take_one)
            #
            # leave_one_centroids_indexes = [int(i) for i in leave_one_centroids[leave_one][take_one]]
            # # print(leave_one_centroids_indexes[:hub_top_k])
            # g1_centroids = individuals_relative[np.array(g1_adapted_group)[leave_one_centroids_indexes[:hub_top_k]]]
            # g2_centroids = individuals_relative[np.array(g2_adapted_group)[leave_one_centroids_indexes[hub_top_k:]]]
            # this_centroids = np.concatenate([g1_centroids, g2_centroids])
            # this_group_operation = MultiCentroid(
            #     this_b_set,
            #     individuals_relative, valid_positions,
            #     is_gauss)
            # take_one_diver_b_to_cents.append(
            #     this_group_operation.get_diver_b_to_b2_centroids(this_centroids))
            take_one_diver_b_to_cents.append(diver_to_g1g2)

        leave_one_diver_b_to_cents.append(take_one_diver_b_to_cents)

    with open(path_prefix + experiment_path +
              "_leave_one_diver_b_to_cents.txt", "wb") as fp:
        pickle.dump(leave_one_diver_b_to_cents, fp)


def load_leave_one_diver_for_training(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_leave_one_diver_b_to_cents.txt", 'rb')
    leave_one_diver_b_to_cents = pickle.load(infile)
    infile.close()
    return leave_one_diver_b_to_cents


def write_leave_one_pick_take_one_diver_for_training(experiment_path, is_gauss='norm', hub_top_k=5):
    data_dict = load_data()
    ta1a3_group = data_dict['ta1a3_group']
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    representation_dict = load_representation(data_dict)
    input_representations = representation_dict['input_representations']
    individuals_relative = representation_dict['individuals_relative']
    ta1a3_group = data_dict['ta1a3_group']
    valid_position_dict = resolve_valid_positions(input_representations, ta1a3_group)
    valid_positions = valid_position_dict['valid_positions']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    len_universal = len_a1a3_group + len_typical_group
    leave_one_centroids = load_leave_one_centroids_by_index(experiment_path)
    leave_one_centroids_indexes = leave_one_centroids.astype(int)
    kl_matrix = load_smooth_divergence('Universal', experiment_path)

    # no_of_cents = np.shape(leave_one_centroids)[0]
    # leave_one_diver_b_to_cents = np.zeros((len_universal, len_universal-1, no_of_cents))
    leave_one_diver_b_to_cents = []
    # centroids size: n,200,200
    for leave_one in tqdm(range(len_universal)):
        take_one_diver_b_to_cents = []
        for take_one in tqdm(range(len_universal - 1)):
            this_hubs = leave_one_centroids_indexes[leave_one][take_one]
            hubs_reindex = this_hubs.copy()
            adjust_idx = np.where(this_hubs >= take_one)
            hubs_reindex[adjust_idx] = this_hubs[adjust_idx] + 1

            # adjust_idx = np.where(hubs_reindex >= leave_one)
            # hubs_reindex[adjust_idx] = hubs_reindex[adjust_idx] + 1
            this_leave_one_hubs = hubs_reindex.copy()

            this_diver = get_leave_ij_divergence(kl_matrix, leave_one, -1)

            diver_to_g1g2 = this_diver[take_one, this_leave_one_hubs]

            # diver_to_g1g2 = this_diver[take_one, leave_one_centroids_indexes[leave_one][take_one]]

            take_one_diver_b_to_cents.append(diver_to_g1g2)

        leave_one_diver_b_to_cents.append(take_one_diver_b_to_cents)

    with open(path_prefix + experiment_path +
              "_leave_one_pick_one_diver_b_to_cents.txt", "wb") as fp:
        pickle.dump(leave_one_diver_b_to_cents, fp)


def load_leave_one_pick_take_one_diver_for_training(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_leave_one_pick_one_diver_b_to_cents.txt", 'rb')
    leave_one_diver_b_to_cents = pickle.load(infile)
    infile.close()
    return leave_one_diver_b_to_cents
