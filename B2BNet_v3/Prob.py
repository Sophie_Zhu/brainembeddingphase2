import pickle
import numpy as np
from tqdm import tqdm
from .PDFParam import PDFParam, load_leave_one_pdf_params_for_training
from .DivergenceVector import DivergenceVector, \
    load_leave_one_diver_for_training, \
    load_leave_one_diver_for_test, \
    load_leave_one_pick_take_one_diver_for_training
from .load_group_data import \
    path_prefix, kl_matrix_dict, load_data, \
    load_representation, \
    resolve_valid_positions
from .params_utility import x_set_to_b2_centroids_by_b_clusters


class Prob:
    def __init__(self,
                 group_name,
                 experiment_path,
                 is_gauss='norm',
                 hub_top_k=5
                 ):
        self.experiment_path = experiment_path
        self.group_name = group_name

        self.is_gauss = is_gauss
        self.hub_top_k = hub_top_k

        self.adhdDiver = DivergenceVector('ADHD', self.experiment_path)
        self.typicalDiver = DivergenceVector('Typical', self.experiment_path)
        self.adhdParam = PDFParam('ADHD', self.experiment_path)
        self.typicalParam = PDFParam('Typical', self.experiment_path)

        self.data_dict = load_data()
        if group_name == 'ADHD':
            self.this_group = self.data_dict['a1a3_group']
            self.this_group_diver = self.adhdDiver.LoadLeaveNoneForTraining()

        elif group_name == 'Typical':
            self.this_group = self.data_dict['typical_group']
            self.this_group_diver = self.typicalDiver.LoadLeaveNoneForTraining()

        self.b_set = self.this_group
        self.group_len = len(self.this_group)

        representation_dict = load_representation(self.data_dict)
        input_representations = representation_dict['input_representations']
        self.individuals_relative = representation_dict['individuals_relative']
        self.ta1a3_group = self.data_dict['ta1a3_group']
        valid_position_dict = resolve_valid_positions(input_representations, self.ta1a3_group)
        self.valid_positions = valid_position_dict['valid_positions']

    def WriteLeaveNoneForTraining(self):
        adhd_leave_none_param = self.adhdParam.LoadLeaveNoneForTraining()
        typical_leave_none_param = self.typicalParam.LoadLeaveNoneForTraining()

        fgl_b_matrix_by_adhd_cluster = \
            x_set_to_b2_centroids_by_b_clusters(self.is_gauss,
                                                adhd_leave_none_param,
                                                self.this_group_diver)
        fgl_b_matrix_by_typical_cluster = \
            x_set_to_b2_centroids_by_b_clusters(self.is_gauss,
                                                typical_leave_none_param,
                                                self.this_group_diver)
        leave_none_fgl_b_matrix = \
            np.concatenate([fgl_b_matrix_by_adhd_cluster, fgl_b_matrix_by_typical_cluster], axis=1)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_train_leave_none_prob.txt", "wb") as fp:
            pickle.dump(leave_none_fgl_b_matrix, fp)

    def LoadLeaveNoneForTraining(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_train_leave_none_prob.txt", 'rb')
        train_leave_none_prob = pickle.load(infile)
        infile.close()
        return train_leave_none_prob

    def TakeOneProb(self, diver_x_to_centroids):
        if self.group_name == 'ADHD':
            group_adhd_param = self.adhdParam.LoadTakeOneForTraining()
            group_typical_param = self.typicalParam.LoadLeaveNoneForTraining()
            this_group_typical_param = group_typical_param.copy()
        elif self.group_name == 'Typical':
            group_typical_param = self.typicalParam.LoadTakeOneForTraining()
            group_adhd_param = self.adhdParam.LoadLeaveNoneForTraining()
            this_group_adhd_param = group_adhd_param.copy()

        take_one_fgl_b_matrix = []
        this_take_one_diver = diver_x_to_centroids.copy()
        for this_take_one in tqdm(range(self.group_len)):
            this_diver = this_take_one_diver[this_take_one]
            if self.group_name == 'ADHD':
                this_group_adhd_param = group_adhd_param[this_take_one]
            elif self.group_name == 'Typical':
                this_group_typical_param = group_typical_param[this_take_one]

            fgl_b_matrix_by_adhd_cluster = \
                x_set_to_b2_centroids_by_b_clusters(self.is_gauss,
                                                    this_group_adhd_param,
                                                    this_diver)
            fgl_b_matrix_by_typical_cluster = \
                x_set_to_b2_centroids_by_b_clusters(self.is_gauss,
                                                    this_group_typical_param,
                                                    this_diver)
            take_one_fgl_b_matrix.append(
                np.concatenate([fgl_b_matrix_by_adhd_cluster, fgl_b_matrix_by_typical_cluster], axis=1)
            )
        return take_one_fgl_b_matrix

    def WriteTakeOneForTraining(self):
        if self.group_name == 'ADHD':
            take_one_diver = self.adhdDiver.LoadTakeOneForTraining()
        elif self.group_name == 'Typical':
            take_one_diver = self.typicalDiver.LoadTakeOneForTraining()

        take_one_fgl_b_matrix = self.TakeOneProb(take_one_diver)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_train_take_one_prob.txt", "wb") as fp:
            pickle.dump(take_one_fgl_b_matrix, fp)

    def LoadTakeOneForTraining(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_train_take_one_prob.txt", 'rb')
        train_take_one_prob = pickle.load(infile)
        infile.close()
        return train_take_one_prob

    def WriteTakeOneForTest(self):
        if self.group_name == 'ADHD':
            take_one_diver = self.adhdDiver.LoadTakeOneForTest()
        elif self.group_name == 'Typical':
            take_one_diver = self.typicalDiver.LoadTakeOneForTest()

        take_one_fgl_b_matrix = self.TakeOneProb(take_one_diver)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_test_take_one_prob.txt", "wb") as fp:
            pickle.dump(take_one_fgl_b_matrix, fp)

    def LoadTakeOneForTest(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_test_take_one_prob.txt", 'rb')
        test_take_one_prob = pickle.load(infile)
        infile.close()
        return test_take_one_prob


def write_leave_one_prob_for_training(experiment_path, is_gauss='norm'):
    data_dict = load_data()
    ta1a3_group = data_dict['ta1a3_group']
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    len_universal = len_a1a3_group + len_typical_group

    leave_one_diver = load_leave_one_diver_for_training(experiment_path)

    leave_one_adhd_pdf_params, leave_one_typical_pdf_params = \
        load_leave_one_pdf_params_for_training(experiment_path)

    leave_one_fgl_b_matrix = []
    for leave_one in tqdm(range(len_universal)):
        take_one_fgl_b_matrix = []
        for take_one in tqdm(range(len_universal - 1)):
            this_diver = np.array(leave_one_diver[leave_one][take_one])
            this_adhd_pdf_params = leave_one_adhd_pdf_params[leave_one][take_one]
            this_typical_pdf_params = leave_one_typical_pdf_params[leave_one][take_one]

            fgl_b_matrix_by_adhd_cluster = \
                x_set_to_b2_centroids_by_b_clusters(is_gauss,
                                                    this_adhd_pdf_params,
                                                    this_diver)
            fgl_b_matrix_by_typical_cluster = \
                x_set_to_b2_centroids_by_b_clusters(is_gauss,
                                                    this_typical_pdf_params,
                                                    this_diver)
            take_one_fgl_b_matrix.append(
                np.concatenate([fgl_b_matrix_by_adhd_cluster, fgl_b_matrix_by_typical_cluster], axis=1)
            )

        # leave_one_fgl_b_matrix.append(take_one_fgl_b_matrix)
        with open(path_prefix + experiment_path +
                  f"_train_leave_one_{leave_one}_prob.txt", "wb") as fp:
            pickle.dump(take_one_fgl_b_matrix, fp)

    # with open(path_prefix + experiment_path +
    #           "_train_leave_one_prob.txt", "wb") as fp:
    #     pickle.dump(leave_one_fgl_b_matrix, fp)


def load_leave_one_prob_for_training(experiment_path, leave_one):
    infile = open(path_prefix + experiment_path +
                  f"_train_leave_one_{leave_one}_prob.txt", 'rb')
    train_leave_one_prob = pickle.load(infile)
    infile.close()
    return train_leave_one_prob


# TODO: merge this
def write_leave_one_prob_for_test(experiment_path, is_gauss='norm'):
    data_dict = load_data()
    ta1a3_group = data_dict['ta1a3_group']
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    len_universal = len_a1a3_group + len_typical_group

    leave_one_diver = load_leave_one_diver_for_test(experiment_path)
    leave_one_adhd_pdf_params, leave_one_typical_pdf_params = \
        load_leave_one_pdf_params_for_training(experiment_path)

    leave_one_fgl_b_matrix = []
    for leave_one in tqdm(range(len_universal)):
        take_one_fgl_b_matrix = []
        for take_one in tqdm(range(len_universal - 1)):
            this_diver = np.array(leave_one_diver[leave_one][take_one]).reshape(1, -1)
            # print(np.shape(this_diver))
            this_adhd_pdf_params = leave_one_adhd_pdf_params[leave_one][take_one]
            this_typical_pdf_params = leave_one_typical_pdf_params[leave_one][take_one]

            fgl_b_matrix_by_adhd_cluster = \
                x_set_to_b2_centroids_by_b_clusters(is_gauss,
                                                    this_adhd_pdf_params,
                                                    this_diver)
            fgl_b_matrix_by_typical_cluster = \
                x_set_to_b2_centroids_by_b_clusters(is_gauss,
                                                    this_typical_pdf_params,
                                                    this_diver)
            take_one_fgl_b_matrix.append(
                np.concatenate([fgl_b_matrix_by_adhd_cluster, fgl_b_matrix_by_typical_cluster], axis=1)
            )

        leave_one_fgl_b_matrix.append(take_one_fgl_b_matrix)
        # with open(path_prefix + experiment_path +
        #           f"_train_leave_one_{leave_one}_prob.txt", "wb") as fp:
        #     pickle.dump(take_one_fgl_b_matrix, fp)

    with open(path_prefix + experiment_path +
              "_test_leave_one_prob.txt", "wb") as fp:
        pickle.dump(leave_one_fgl_b_matrix, fp)


def load_leave_one_prob_for_test(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_test_leave_one_prob.txt", 'rb')
    test_take_one_prob = pickle.load(infile)
    infile.close()
    return test_take_one_prob


def write_leave_one_pick_take_one_prob_for_training(experiment_path, is_gauss='norm'):
    data_dict = load_data()
    ta1a3_group = data_dict['ta1a3_group']
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    len_universal = len_a1a3_group + len_typical_group

    leave_one_diver = load_leave_one_pick_take_one_diver_for_training(experiment_path)
    leave_one_adhd_pdf_params, leave_one_typical_pdf_params = \
        load_leave_one_pdf_params_for_training(experiment_path)

    leave_one_fgl_b_matrix = []
    for leave_one in tqdm(range(len_universal)):
        take_one_fgl_b_matrix = []
        for take_one in tqdm(range(len_universal - 1)):
            this_diver = np.array(leave_one_diver[leave_one][take_one]).reshape(1, -1)
            # print(np.shape(this_diver))
            this_adhd_pdf_params = leave_one_adhd_pdf_params[leave_one][take_one]
            this_typical_pdf_params = leave_one_typical_pdf_params[leave_one][take_one]

            fgl_b_matrix_by_adhd_cluster = \
                x_set_to_b2_centroids_by_b_clusters(is_gauss,
                                                    this_adhd_pdf_params,
                                                    this_diver)
            fgl_b_matrix_by_typical_cluster = \
                x_set_to_b2_centroids_by_b_clusters(is_gauss,
                                                    this_typical_pdf_params,
                                                    this_diver)
            take_one_fgl_b_matrix.append(
                np.concatenate([fgl_b_matrix_by_adhd_cluster, fgl_b_matrix_by_typical_cluster], axis=1)
            )

        leave_one_fgl_b_matrix.append(take_one_fgl_b_matrix)
        # with open(path_prefix + experiment_path +
        #           f"_train_leave_one_{leave_one}_prob.txt", "wb") as fp:
        #     pickle.dump(take_one_fgl_b_matrix, fp)

    with open(path_prefix + experiment_path +
              "_train_leave_one_pick_take_one_prob.txt", "wb") as fp:
        pickle.dump(leave_one_fgl_b_matrix, fp)


def load_leave_one_pick_take_one_prob_for_training(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_train_leave_one_pick_take_one_prob.txt", 'rb')
    train_take_one_prob = pickle.load(infile)
    infile.close()
    return train_take_one_prob
