import pickle
import numpy as np
import keras
from keras.models import model_from_yaml
from keras.utils import to_categorical
from tqdm import tqdm
from .PDFParam import PDFParam, load_leave_one_clusters
from .DivergenceVector import DivergenceVector, load_leave_one_diver_for_training, load_leave_one_diver_for_test
from .load_group_data import \
    path_prefix, kl_matrix_dict, load_data, \
    load_representation, \
    resolve_valid_positions
from .Prob import Prob, load_leave_one_prob_for_test, load_leave_one_prob_for_training
from .CNN_utility import cnn_test_flow


def get_labels(adapted_a1a3_clusters, adapted_typical_clusters,
               len_adapted_a1a3_set, len_adapted_typical_set):

    cluster_label_list = np.zeros(len_adapted_a1a3_set + len_adapted_typical_set)

    cluster_idx = 0
    for cluster in adapted_a1a3_clusters:
        cluster_label_list[list(cluster)] = cluster_idx
        cluster_idx += 1
    for cluster in adapted_typical_clusters:
        cluster_label_list[np.array(list(cluster)) + len_adapted_a1a3_set] = cluster_idx
        cluster_idx += 1

    # one-hot encode target column
    y_cluster = to_categorical(cluster_label_list)

    return y_cluster, cluster_label_list


class Weights:
    def __init__(self,
                 experiment_path
                 ):
        self.experiment_path = experiment_path

        self.data_dict = load_data()
        representation_dict = load_representation(self.data_dict)
        input_representations = representation_dict['input_representations']
        self.individuals_relative = representation_dict['individuals_relative']
        self.ta1a3_group = self.data_dict['ta1a3_group']
        self.a1a3_group = self.data_dict['a1a3_group']
        self.typical_group = self.data_dict['typical_group']
        valid_position_dict = resolve_valid_positions(input_representations, self.ta1a3_group)
        self.valid_positions = valid_position_dict['valid_positions']

        self.length_universal_set = len(self.ta1a3_group)
        self.length_a1a3_set = len(self.a1a3_group)
        self.length_typical_set = len(self.typical_group)


    def WriteLeaveOne(self, date, version):
        leave_one_adhd_clusters, leave_one_typical_clusters = \
            load_leave_one_clusters(self.experiment_path)
        for leave_one in tqdm(range(self.length_universal_set)):
            leave_one_X = load_leave_one_prob_for_training(self.experiment_path, leave_one)
            list_X = []
            list_y = []
            list_cluster_label_list = []
            for take_one in tqdm(range(self.length_universal_set - 1)):
                if leave_one < self.length_a1a3_set and take_one < self.length_a1a3_set - 1:
                    len_adapted_a1a3_set = self.length_a1a3_set - 2
                    len_adapted_typical_set = self.length_typical_set
                elif leave_one < self.length_a1a3_set and take_one >= self.length_a1a3_set - 1:
                    len_adapted_a1a3_set = self.length_a1a3_set - 1
                    len_adapted_typical_set = self.length_typical_set - 1
                elif leave_one >= self.length_a1a3_set > take_one:
                    len_adapted_a1a3_set = self.length_a1a3_set - 1
                    len_adapted_typical_set = self.length_typical_set - 1
                elif leave_one >= self.length_a1a3_set and take_one >= self.length_a1a3_set:
                    len_adapted_a1a3_set = self.length_a1a3_set
                    len_adapted_typical_set = self.length_typical_set - 2

                X = leave_one_X[take_one]
                adapted_a1a3_clusters = leave_one_adhd_clusters[leave_one][take_one]
                adapted_typical_clusters = leave_one_typical_clusters[leave_one][take_one]
                y_cluster, cluster_label_list = \
                    get_labels(adapted_a1a3_clusters, adapted_typical_clusters,
                               len_adapted_a1a3_set, len_adapted_typical_set)

                list_X.append(X)
                list_y.append(y_cluster)
                list_cluster_label_list.append(cluster_label_list)

            ######################################
            list_X = np.array(list_X)
            list_y = np.array(list_y)
            list_cluster_label_list = np.array(list_cluster_label_list)
            # list_inputs_prob_matrices = np.array(list_inputs_prob_matrices)
            # list_X_test = np.array(list_X_test)

            shape_list_X = np.shape(list_X)
            shape_list_y = np.shape(list_y)
            shape_list_cluster_label = np.shape(list_cluster_label_list)
            # shape_list_X_test = np.shape(list_X_test)

            # print(shape_list_X_test)

            X = list_X.reshape(shape_list_X[0] * shape_list_X[1], shape_list_X[2], shape_list_X[3])
            y = list_y.reshape(shape_list_y[0] * shape_list_y[1], shape_list_y[2])
            cluster_label = list_cluster_label_list.reshape(shape_list_cluster_label[0] * shape_list_cluster_label[1])
            # X_test = list_X_test.reshape(shape_list_X_test[0] * shape_list_X_test[1], shape_list_X_test[2], shape_list_X_test[3])

            model_for_training = cnn_test_flow(X, y, cluster_label)
            # serialize model to YAML
            model_yaml = model_for_training.to_yaml()
            with open(path_prefix + self.experiment_path + f"model_{date}_leave_one{leave_one}_{version}.yaml",
                      "w") as yaml_file:
                yaml_file.write(model_yaml)
            # serialize weights to HDF5
            model_for_training.save_weights(
                path_prefix + self.experiment_path + f"model_{date}_leave_one{leave_one}_{version}.h5")
            print("Saved model to disk")
