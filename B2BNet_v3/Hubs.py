import pickle
import numpy as np
from sklearn.externals import joblib
from collections import Counter
from tqdm import tqdm
from .load_group_data import \
    path_prefix, kl_matrix_dict, get_top_hubs, \
    load_data, load_representation, \
    resolve_valid_positions
from .MST import MST
from .DivergenceMatrix import f_divergence, get_leave_ij_set


def f_hub_top_k(b_len, mst, k):
    hubs = get_top_hubs(b_len, mst, k)
    # print('hub:', hubs)
    return hubs


def get_leave_ij_hubs(
        group_leave_hubs,
        outer_i, inner_j):
    if inner_j == -1:
        leave_ij_hubs = group_leave_hubs[outer_i]
    else:
        leave_ij_hubs = group_leave_hubs[outer_i][inner_j]

    return leave_ij_hubs


class Hubs:
    def __init__(self, group_name, experiment_path, hub_top_k=5):
        self.group_name = group_name
        self.experiment_path = experiment_path
        self.hub_top_k = hub_top_k

        self.data_dict = load_data()
        if group_name == 'ADHD':
            self.this_group = self.data_dict['a1a3_group']

        elif group_name == 'Typical':
            self.this_group = self.data_dict['typical_group']
        self.group_len = len(self.this_group)

        self.mst = MST(self.group_name, self.experiment_path)

        representation_dict = load_representation(self.data_dict)
        input_representations = representation_dict['input_representations']
        self.individuals_relative = representation_dict['individuals_relative']
        self.ta1a3_group = self.data_dict['ta1a3_group']
        valid_position_dict = resolve_valid_positions(input_representations, self.ta1a3_group)
        self.valid_positions = valid_position_dict['valid_positions']

    def count_take_one_hub_freq(self, take_one_group_hubs):
        group_hub_count = []
        print("count_hub_freq ...")
        for take_one in tqdm(range(len(take_one_group_hubs))):
            hubs = np.array(take_one_group_hubs[take_one])

            adjust_idx = np.where(hubs >= take_one)
            hubs[adjust_idx] = hubs[adjust_idx] + 1

            group_hub_count = np.concatenate([group_hub_count, hubs])

        group_recounted = Counter(list(group_hub_count))
        group_sorted_hubs = np.array(
            [pair[0] for pair in sorted(group_recounted.items(), key=lambda item: item[1])])

        return group_sorted_hubs[-self.hub_top_k:]

    def template_clusters(self, b_set, hubs):
        length_clusters = self.hub_top_k
        initial_clusters = [set() for _ in range(length_clusters)]
        centroids = self.individuals_relative[np.array(b_set)[hubs]]

        for idx_in_b in range(len(b_set)):
            diver_indexes_in_b_hubs = \
                self.get_diver_x_set_cent_y_smooth(  # smooth, attention!
                    [b_set[idx_in_b]],
                    centroids)

            m_index = np.argmin(diver_indexes_in_b_hubs)
            initial_clusters[int(m_index)].add(idx_in_b)
        return initial_clusters

    def hub_alignment(self, template_hubs, template_clusters, this_group,
                      hubs, this_take_one, this_leave_one=-1):

        hubs_reindex = hubs.copy()
        if this_take_one > -1:
            adjust_idx = np.where(hubs >= this_take_one)
            hubs_reindex[adjust_idx] = hubs[adjust_idx] + 1
        if this_leave_one > -1:
            adjust_idx = np.where(hubs_reindex >= this_leave_one)
            hubs_reindex[adjust_idx] = hubs_reindex[adjust_idx] + 1

        hubs_aligned = hubs.copy()[:self.hub_top_k]
        hubs_reindex_aligned = hubs.copy()[:self.hub_top_k]
        #####################################
        template_hubs_left = list(template_hubs.copy())
        to_be_aligned_hubs_left = list(hubs_reindex.copy())
        left_index = []
        aligned_index = []
        this_i = 0
        # TODO:Debug
        # print(f"template_ hubs{template_hubs}")
        for hub_in_template in template_hubs:
            # print( hub_in_template)
            idx = np.where(hubs_reindex == int(hub_in_template))
            if len(idx[0]) > 0:
                template_hubs_left.remove(hub_in_template)
                to_be_aligned_hubs_left.remove(hubs_reindex[idx[0]])
                # print("hihi!")
                # print(template_hubs_left)
                # print(this_i, int(idx[0]))
                hubs_aligned[this_i] = hubs[idx[0]]
                hubs_reindex_aligned[this_i] = hubs_reindex[idx[0]]
                aligned_index.append(this_i)

            else:
                left_index.append(this_i)

            this_i += 1

        #####################################
        template_hubs_left = [int(i) for i in template_hubs_left]
        left_index_with_hub_candidates = []
        # print(f"hubs_to_be_aligned{hubs_reindex}")
        # print(f"template_hubs_left: {template_hubs_left}")
        # print(f"aligned : {np.array(aligned_index)}")

        # print(f"left_index:{left_index}")

        if len(template_hubs_left) > 0:
            for this_left_index in left_index:
                this_candidates = []
                for this_hub in hubs_reindex:
                    # if this_hub not in hubs_reindex_aligned[np.array(aligned_index)]:
                    if this_hub in to_be_aligned_hubs_left:
                        # print(f"this_hub:{this_hub}")
                        # print(f"template_cluster {this_left_index}:{template_clusters[this_left_index]}")
                        if this_hub in template_clusters[this_left_index]:
                            # print("in this cluster!")
                            this_candidates.append(this_hub)
                left_index_with_hub_candidates.append(this_candidates)

        # print(f"left_index_with_hub_candidates{left_index_with_hub_candidates}")
        idx = -1
        for this_left_index in left_index:
            idx += 1
            this_candidates = np.array(left_index_with_hub_candidates)[idx]
            # print(np.shape(this_candidates))
            if len(this_candidates) > 0:
                if len(this_candidates) == 1:
                    this_candidates = this_candidates[0]
                    reindex_hub = this_candidates
                else:
                    # print(np.array(this_group)[this_candidates])
                    this_hubs_to_hub_in_template = f_divergence(
                        self.individuals_relative[np.array(this_group)[this_candidates]],
                        [self.individuals_relative[np.array(this_group)[template_hubs[this_left_index]]]],
                        self.valid_positions, is_same=False, is_smooth=True)
                    # print("hihi")
                    # print(this_hubs_to_hub_in_template)
                    min_idx = np.argmin(this_hubs_to_hub_in_template)
                    reindex_hub = this_candidates[min_idx]

                left_index.remove(this_left_index)
                # print(f"reindex_hub:{reindex_hub}")
                to_be_aligned_hubs_left.remove(reindex_hub)
                hubs_aligned[this_left_index] = hubs[np.where(hubs_reindex == reindex_hub)]
                hubs_reindex_aligned[this_left_index] = reindex_hub

        # pick the shortest divergence
        for this_left_index in left_index:
            this_candidates = np.array(to_be_aligned_hubs_left)
            this_hubs_to_hub_in_template = f_divergence(
                self.individuals_relative[np.array(this_group)[this_candidates]],
                [self.individuals_relative[np.array(this_group)[template_hubs[this_left_index]]]],
                self.valid_positions, is_same=False, is_smooth=True)
            min_idx = np.argmin(this_hubs_to_hub_in_template)
            reindex_hub = this_candidates[min_idx]
            hubs_aligned[this_left_index] = hubs[np.where(hubs_reindex == reindex_hub)]
            hubs_reindex_aligned[this_left_index] = reindex_hub

        return np.array(hubs_aligned)

    def get_diver_x_set_cent_y_smooth(self, x_set, y_centroids):
        return f_divergence(
            self.individuals_relative[x_set],
            y_centroids,
            self.valid_positions,
            is_same=False
        )

    #############################################

    def WriteLeaveNoneHubs(self):
        leave_none_mst = self.mst.LoadLeaveNoneMST()
        leave_none_hubs = f_hub_top_k(self.group_len, leave_none_mst, self.hub_top_k)
        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_leave_none_hubs.txt", "wb") as fp:
            pickle.dump(leave_none_hubs, fp)

    def LoadLeaveNoneHubs(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_leave_none_hubs.txt", 'rb')
        group_leave_none_hubs = pickle.load(infile)
        infile.close()
        return group_leave_none_hubs

    def WriteTakeOneHubs(self):
        group_take_one_mst = self.mst.LoadTakeOneMST()
        group_take_one_hubs = []

        for i in tqdm(range(self.group_len)):
            this_mst = group_take_one_mst[i]
            this_hubs = f_hub_top_k(self.group_len - 1, this_mst, self.hub_top_k)

            group_take_one_hubs.append(this_hubs)
        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_take_one_hubs.txt", "wb") as fp:
            pickle.dump(group_take_one_hubs, fp)

    def LoadTakeOneHubs(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_take_one_hubs.txt", 'rb')
        group_take_one_hubs = pickle.load(infile)
        infile.close()
        return group_take_one_hubs

    def WriteLeaveOneHubs(self):
        group_leave_one_mst = self.mst.LoadLeaveOneMST()
        group_leave_twice_hubs = []
        for i in tqdm(range(self.group_len)):
            #
            inner_hubs = []
            for j in range(self.group_len - 1):
                this_hubs = f_hub_top_k(self.group_len - 2, group_leave_one_mst[i][j], self.hub_top_k)
                inner_hubs.append(this_hubs)
            # group_hubs = f_hub_top_k(self.group_len-1, group_leave_none_mst[i], self.hub_top_k)
            # inner_hubs.append(group_hubs)
            group_leave_twice_hubs.append(inner_hubs)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_leave_one_hubs.txt", "wb") as fp:
            pickle.dump(group_leave_twice_hubs, fp)

    def LoadLeaveOneHubs(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_leave_one_hubs.txt", 'rb')
        group_leave_twice_hubs = pickle.load(infile)
        infile.close()
        return group_leave_twice_hubs

    #######################################
    def WriteLeaveNoneAlignedHubs(self):
        leave_none_hubs = self.LoadLeaveNoneHubs()
        take_one_hubs = self.LoadTakeOneHubs()

        template_hubs = self.count_take_one_hub_freq(take_one_hubs)
        template_hubs = np.sort([int(i) for i in template_hubs])

        template_clusters = self.template_clusters(self.this_group, template_hubs)
        this_take_one = -1
        leave_none_aligned_hubs = self.hub_alignment(template_hubs,
                                                     template_clusters, self.this_group,
                                                     leave_none_hubs, this_take_one)
        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_leave_none_aligned_hubs.txt", "wb") as fp:
            pickle.dump(leave_none_aligned_hubs, fp)

        # local_centroids = self.individuals_relative[np.array(self.this_group)[leave_none_aligned_hubs]]

        # with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
        #           "_leave_none_local_centroids.txt", "wb") as fp:
        #     pickle.dump(local_centroids, fp)

    def LoadLeaveNoneAlignedHubs(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_leave_none_aligned_hubs.txt", 'rb')
        group_leave_none_hubs = pickle.load(infile)
        infile.close()
        return group_leave_none_hubs

    # def LoadLeaveNoneCentroids(self):
    #     infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
    #                   "_leave_none_local_centroids.txt", 'rb')
    #     group_leave_none_centroids = pickle.load(infile)
    #     infile.close()
    #     return group_leave_none_centroids

    def WriteTakeOneAlignedHubs(self):
        take_one_hubs = self.LoadTakeOneHubs()

        template_hubs = self.count_take_one_hub_freq(take_one_hubs)
        template_hubs = np.sort([int(i) for i in template_hubs])

        template_clusters = self.template_clusters(self.this_group, template_hubs)

        group_take_one_aligned_hubs = []
        # group_take_one_centroids = []

        for this_take_one in tqdm(range(self.group_len)):
            this_aligned_hubs = self.hub_alignment(template_hubs,
                                                   template_clusters, self.this_group,
                                                   take_one_hubs[this_take_one], this_take_one)

            # this_adapted_group = get_leave_ij_set(self.this_group, this_take_one, -1)
            # this_local_centroids = self.individuals_relative[np.array(this_adapted_group)[this_aligned_hubs]]

            # group_take_one_centroids.append(this_local_centroids)
            group_take_one_aligned_hubs.append(this_aligned_hubs)

        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_take_one_aligned_hubs.txt", "wb") as fp:
            pickle.dump(group_take_one_aligned_hubs, fp)

        # with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
        #           "_take_one_local_centroids.txt", "wb") as fp:
        #     pickle.dump(group_take_one_centroids, fp)

    def LoadTakeOneAlignedHubs(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_take_one_aligned_hubs.txt", 'rb')
        group_take_one_aligned_hubs = pickle.load(infile)
        infile.close()
        return group_take_one_aligned_hubs

    # def LoadTakeOneCentroids(self):
    #     infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
    #                   "_take_one_local_centroids.txt", 'rb')
    #     group_take_one_centroids = pickle.load(infile)
    #     infile.close()
    #     return group_take_one_centroids

    def WriteLeaveOneAlignedHubs(self):

        leave_one_hubs = self.LoadLeaveOneHubs()
        group_leave_twice_hubs = []
        # group_leave_twice_centroids = []

        # Bug fix: template_hubs
        take_one_hubs = self.LoadTakeOneHubs()

        template_hubs = self.count_take_one_hub_freq(take_one_hubs)
        this_template_hubs = np.sort([int(i) for i in template_hubs])

        for outer_leave_one in tqdm(range(self.group_len)):
            # this_template_hubs = self.count_take_one_hub_freq(leave_one_hubs[outer_leave_one])
            # this_template_hubs = np.sort([int(i) for i in this_template_hubs])

            # this_template_hubs = template_hubs.copy()
            # adjust_idx = np.where(template_hubs >= outer_leave_one)
            # this_template_hubs[adjust_idx] = template_hubs[adjust_idx] - 1

            this_template_clusters = self.template_clusters(self.this_group, this_template_hubs)
            inner_take_one_aligned_hubs = []

            this_template_group = get_leave_ij_set(self.this_group, outer_leave_one, -1)
            for inner_take_one in range(self.group_len - 1):

                this_aligned_hubs = self.hub_alignment(this_template_hubs,
                                                       this_template_clusters, this_template_group,
                                                       leave_one_hubs[outer_leave_one][inner_take_one],
                                                       inner_take_one, outer_leave_one)
                inner_take_one_aligned_hubs.append(this_aligned_hubs)

                # this_adapted_group = get_leave_ij_set(self.this_group, outer_leave_one, inner_take_one)
                # this_local_centroids = self.individuals_relative[np.array(this_adapted_group)[this_aligned_hubs]]

                # group_leave_twice_centroids.append(this_local_centroids)
            group_leave_twice_hubs.append(inner_take_one_aligned_hubs)

        print(f"write {kl_matrix_dict[self.group_name]}_leave_one_aligned_hubs...")
        with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                  "_leave_one_aligned_hubs.txt", "wb") as fp:
            pickle.dump(group_leave_twice_hubs, fp)

        # print(f"write {kl_matrix_dict[self.group_name]}_leave_one_local_centroids...")
        # with open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
        #           "_leave_one_local_centroids.txt", "wb") as fp:
        #     joblib.dump(group_leave_twice_centroids, fp)
        # print("This WriteLeaveOneAlignedHubs Done")

    def LoadLeaveOneAlignedHubs(self):
        infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
                      "_leave_one_aligned_hubs.txt", 'rb')
        group_leave_twice_hubs = pickle.load(infile)
        infile.close()
        return group_leave_twice_hubs

    # def LoadLeaveOneCentroids(self):
    #     # try:
    #     #     data = unpickler.load()
    #     # except EOFError:
    #     infile = open(path_prefix + self.experiment_path + kl_matrix_dict[self.group_name] +
    #                   f"_leave_one_local_centroids.txt", 'rb')
    #     try:
    #         group_leave_one_centroids = joblib.load(infile)
    #     except EOFError:
    #         print('EOFError')
    #     infile.close()
    #     return group_leave_one_centroids


######################
# outer:adhd inner:adhd
# outer:adhd inner:tdc
# outer:tdc inner:adhd
# outer:tdc inner：tdc
######################
def write_leave_one_centroids_by_index(experiment_path, hub_top_k=5):
    data_dict = load_data()
    a1a3_group = data_dict['a1a3_group']
    typical_group = data_dict['typical_group']

    len_a1a3_group = len(a1a3_group)
    len_typical_group = len(typical_group)
    adhdHubs = Hubs('ADHD', experiment_path)
    typicalHubs = Hubs('Typical', experiment_path)
    adhd_leave_none_centroids = np.array(adhdHubs.LoadLeaveNoneAlignedHubs())
    typical_leave_none_centroids = np.array(typicalHubs.LoadLeaveNoneAlignedHubs())
    adhd_take_one_centroids = adhdHubs.LoadTakeOneAlignedHubs()
    typical_take_one_centroids = typicalHubs.LoadTakeOneAlignedHubs()
    adhd_leave_one_centroids = adhdHubs.LoadLeaveOneAlignedHubs()
    typical_leave_one_centroids = typicalHubs.LoadLeaveOneAlignedHubs()

    len_universal = len_a1a3_group + len_typical_group
    leave_one_centroids = np.zeros((len_universal, len_universal-1, hub_top_k*2))
    # leave_one_group = np.zeros((len_universal, len_universal - 1, hub_top_k * 2))

    # centroids size: n,200,200
    for leave_one in tqdm(range(len_universal)):
        for take_one in tqdm(range(len_universal - 1)):
            if leave_one < len_a1a3_group and take_one < len_a1a3_group - 1:
                leave_one_centroids[leave_one][take_one] = \
                    np.concatenate((adhd_leave_one_centroids[leave_one][take_one],
                                    typical_leave_none_centroids + len_a1a3_group - 2), axis=0)
            elif leave_one < len_a1a3_group and take_one >= len_a1a3_group - 1:
                typical_take_one = take_one - (len_a1a3_group - 1)
                leave_one_centroids[leave_one][take_one] = \
                    np.concatenate((adhd_take_one_centroids[leave_one],
                                    typical_take_one_centroids[typical_take_one] + len_a1a3_group - 1
                                    ), axis=0)
            elif leave_one >= len_a1a3_group > take_one:
                typical_take_one = leave_one - len_a1a3_group
                leave_one_centroids[leave_one][take_one] = \
                    np.concatenate((adhd_take_one_centroids[take_one],
                                    typical_take_one_centroids[typical_take_one] + len_a1a3_group - 1
                                    ), axis=0)
            elif leave_one >= len_a1a3_group and take_one >= len_a1a3_group:
                typical_outer_take_one = leave_one - len_a1a3_group
                typical_inner_take_one = take_one - len_a1a3_group
                leave_one_centroids[leave_one][take_one] = \
                    np.concatenate((adhd_leave_none_centroids,
                                    typical_leave_one_centroids[typical_outer_take_one][typical_inner_take_one] + len_a1a3_group
                                    ), axis=0)
    with open(path_prefix + experiment_path +
              "_leave_one_centroids.txt", "wb") as fp:
        pickle.dump(leave_one_centroids, fp)


def load_leave_one_centroids_by_index(experiment_path):
    infile = open(path_prefix + experiment_path +
                  "_leave_one_centroids.txt", 'rb')
    leave_one_centroids = pickle.load(infile)
    infile.close()
    return leave_one_centroids
